package com.njcb.game.answer.batchtran

import org.apache.cxf.bus.spring.SpringBus
import org.apache.cxf.jaxws.EndpointImpl
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import javax.xml.ws.Endpoint

@Configuration
class BatchTranConfig(
    private val springBus: SpringBus,
    private val service: S10001000001,
) {
    @Bean
    fun s10001000001Endpoint(): Endpoint = EndpointImpl(springBus, service).apply {
        publish("/S10001000001")
    }
}
