package com.njcb.game.answer

import com.njcb.ams.support.autoconfigure.annotation.AmsApplication
import org.springframework.boot.SpringApplication


@AmsApplication(scanBasePackages = ["com.njcb"])
class AnswerApplication {

}

fun main(args: Array<String>) {
    SpringApplication.run(AnswerApplication::class.java, *args)
}
