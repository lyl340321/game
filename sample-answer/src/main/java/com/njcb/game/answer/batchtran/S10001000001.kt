package com.njcb.game.answer.batchtran

import com.dcitsbiz.esb.metadata.ReqSysHeadType
import com.dcitsbiz.esb.metadata.RspSysHeadType
import com.dcitsbiz.esb.services._10001000001.*
import com.dcitsbiz.esb.services._10001000001.wsdl.ESBServerPortType
import com.njcb.ams.store.esbmodule.EsbHeadUtil
import com.njcb.ams.store.esbmodule.annotation.EsbService
import com.njcb.ams.support.exception.AppException
import com.njcb.ams.util.AmsFileUtils
import com.njcb.game.esb.EsbUtils
import com.njcb.game.games.batchtran.TestAccount
import com.njcb.game.games.batchtran.TransAccount
import com.njcb.game.games.batchtran.sumTransactions
import com.njcb.game.util.assertEqual
import com.njcb.game.util.assertTrue
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import kotlin.io.path.readLines


@EsbService
class S10001000001(
    @Value("\${answer.user-token}")
    private val token: String,
    @Value("\${answer.server-addr}")
    private val serverAddr: String,
) : ESBServerPortType {
    override fun batchaccount(req1000100000102: Req1000100000102Type): Rsp1000100000102Type {
        // 下载文件
        val file = kotlin.io.path.createTempFile("BatchAccount", ".csv")
        logger.info("Downloading file from {} to {}", req1000100000102.reqAppBody.fileUrl, file)
        AmsFileUtils.downFile("${req1000100000102.reqAppBody.fileUrl}?token=$token", file.toFile())

        // 读取记账文件
        val txs = file
            .readLines(charset = Charsets.UTF_8)
            .map { line ->
                val data = line.split("|")
                assertEqual(6, data.size, "文件格式不正确")

                TransAccount(
                    payer = TestAccount(data[1], data[2]),
                    payee = TestAccount(data[3], data[4]),
                    amount = data[5].toBigDecimal().also {
                        assertTrue(it.signum() > 0) { "金额不能为负" }
                    },
                )
            }

        // 计算答案
        val summary = txs
            .sumTransactions()
            .shuffled() // 作为测试，我们把顺序打乱，判分程序应该有能力处理不同顺序的交易
            .map {
                // 转换成目标格式
                Req1000100000101Type.ReqAppBody.Details().apply {
                    draweeAcctName = it.payer.name
                    draweeAccount = it.payer.number
                    payeeAcctName = it.payee.name
                    payeeAccount = it.payee.number
                    amount = it.amount.toPlainString()
                }
            }

        // 调用服务器通用记账接口
        val esb = EsbUtils.esbClient<ESBServerPortType>("$serverAddr/services/S10001000001")
        EsbHeadUtil.setIsHeadProxy(false) // 本次禁用自带的ESB错误码解析
        val resp = esb.transaccount(Req1000100000101Type().apply {
            reqSysHead = ReqSysHeadType().apply {
                globalSeq = req1000100000102.reqSysHead.globalSeq
            }
            reqAppHead = req1000100000102.reqAppHead
            reqAppBody = Req1000100000101Type.ReqAppBody().apply {
                applicantName = req1000100000102.reqAppBody.applicantName
                applicantCustNo = req1000100000102.reqAppBody.applicantCustNo
                details.addAll(summary)
            }
        })
        if (resp.rspSysHead.transStatus != "S") {
            throw object : AppException(resp.rspSysHead.retMsg, resp.rspSysHead.retCode) {}
        }

        // 返回成功
        return Rsp1000100000102Type().apply {
            rspSysHead = RspSysHeadType().apply {
                globalSeq = req1000100000102.reqSysHead.globalSeq
            }
            rspAppHead = RspAppHeadType().apply {
                tlrNo = req1000100000102.reqAppHead.tlrNo
                brId = req1000100000102.reqAppHead.brId
            }
            rspAppBody = Rsp1000100000102Type.RspAppBody().apply {
                totalCount = txs.size.toBigInteger()
            }
        }
    }

    override fun transaccount(req1000100000101: Req1000100000101Type): Rsp1000100000101Type {
        TODO("Not yet implemented")
    }

    companion object {
        private val logger = LoggerFactory.getLogger(S10001000001::class.java)
    }
}