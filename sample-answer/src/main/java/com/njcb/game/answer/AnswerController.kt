package com.njcb.game.answer

import com.njcb.game.games.ftp.calculateCustomerSpread
import com.njcb.game.games.networth.NAVManager
import com.njcb.game.games.networth.calculateGain
import com.njcb.game.util.BeanUtil
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import java.math.BigInteger

@Controller
@RequestMapping("question/")
class AnswerController(
    private val navManager: NAVManager,
) {

    @PostMapping("002")
    fun answer002(@RequestBody req: Map<String, String>): ResponseEntity<Map<String, String>> {
        val i = (req["i"] as String).toInt()
        val result = (1..i).map(Int::toBigInteger).reduce(BigInteger::multiply).toString()
        return ResponseEntity.ok(mapOf("answer" to result))
    }

    @PostMapping("004")
    fun answer004(@RequestBody req: Map<String, Any>): ResponseEntity<Map<String, Any>> {
        val input: com.njcb.game.games.ftp.TestCase = BeanUtil.toObject(req)
        val answer = input.calculateCustomerSpread()

        return ResponseEntity.ok(BeanUtil.toJSONMap(mapOf("spreads" to answer)))
    }

    @PostMapping("005")
    fun answer005(@RequestBody req: Map<String, Any>): ResponseEntity<Map<String, Any>> {
        val input: com.njcb.game.games.networth.TestCase = BeanUtil.toObject(req)
        val answer = input.calculateGain(navManager)

        return ResponseEntity.ok(BeanUtil.toJSONMap(mapOf("gain" to answer.toPlainString())))
    }
}
