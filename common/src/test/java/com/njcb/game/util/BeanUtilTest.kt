package com.njcb.game.util

import com.njcb.game.games.ftp.Product
import com.njcb.game.games.ftp.Purchase
import com.njcb.game.games.ftp.TestCase
import kotlin.test.Test
import kotlin.test.assertEquals

class BeanUtilTest {

    private val map = mapOf(
        "ftpRate" to "12.34",
        "depositProducts" to listOf(
            mapOf(
                "code" to "CK001",
                "yearlyRate" to "56.78",
            ),
            mapOf(
                "code" to "CK002",
                "yearlyRate" to "54.32",
            ),
        ),
        "loanProducts" to listOf(
            mapOf(
                "code" to "DK001",
                "yearlyRate" to "23.45",
            ),
            mapOf(
                "code" to "DK002",
                "yearlyRate" to "87.65",
            ),
        ),
        "purchases" to listOf(
            mapOf(
                "customerNo" to "10001",
                "productCode" to "CK001",
                "amount" to "12340",
            ),
            mapOf(
                "customerNo" to "10002",
                "productCode" to "DK002",
                "amount" to "987650",
            ),
        ),
    )

    private val obj = TestCase(
        ftpRate = "12.34".toBigDecimal(),
        depositProducts = listOf(
            Product(
                code = "CK001",
                yearlyRate = "56.78".toBigDecimal(),
            ),
            Product(
                code = "CK002",
                yearlyRate = "54.32".toBigDecimal(),
            ),
        ),
        loanProducts = listOf(
            Product(
                code = "DK001",
                yearlyRate = "23.45".toBigDecimal(),
            ),
            Product(
                code = "DK002",
                yearlyRate = "87.65".toBigDecimal(),
            ),
        ),
        purchases = listOf(
            Purchase(
                customerNo = "10001",
                productCode = "CK001",
                amount = "12340".toBigDecimal(),
            ),
            Purchase(
                customerNo = "10002",
                productCode = "DK002",
                amount = "987650".toBigDecimal(),
            ),
        ),
    )

    @Test
    fun `test toJSONMap`() {
        assertEquals(map, BeanUtil.toJSONMap(obj))
    }

    @Test
    fun `test toObject`() {
        assertEquals(obj, BeanUtil.toObject(map))
    }
}