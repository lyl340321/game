package com.njcb.game.games.networth

import java.time.LocalDate
import java.time.Month
import java.time.temporal.ChronoUnit
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNull

class NAVManagerTest {
    private val manager = NAVManager()

    @Test
    fun `test getNav`() {
        assertEquals(NetAssetValue(
            dailyValue = "1.00280000".toBigDecimal(),
            totalValue = "1.30360000".toBigDecimal(),
        ), manager.getNav("Z10001", "20210201"))

        assertEquals(NetAssetValue(
            dailyValue = "1.04870000".toBigDecimal(),
            totalValue = "1.04870000".toBigDecimal(),
        ), manager.getNav("Z50020", "20210915"))

        assertEquals(NetAssetValue(
            dailyValue = "1.00000000".toBigDecimal(),
            totalValue = "1.00000000".toBigDecimal(),
        ), manager.getNav("S10032", "20211130"))

        assertNull(manager.getNav("FBSH01", "20221131"))
        assertNull(manager.getNav("test", "20210201"))
        assertNull(manager.getNav("test", "test"))
    }

    @Test
    fun `test day calc`() {
        assertEquals(1,
            ChronoUnit.DAYS.between(
                LocalDate.of(2021, Month.MARCH, 3),
                LocalDate.of(2021, Month.MARCH, 4)
            )
        )
    }

    @Test
    fun `test answer`() {
        val result = TestCase(
            purchases = listOf(
                Purchase(
                    productCode = "Y30003",
                    amount = "2270000".toBigDecimal(),
                    buyDate = LocalDate.of(2021, 10, 13),
                    sellDate = LocalDate.of(2021, 11, 15),
                )
            )
        ).calculateGain(navManager = manager)

        assertEquals("118243.83".toBigDecimal(), result)
    }
}
