package com.njcb.game.games.ftp

import kotlin.test.Test
import kotlin.test.assertEquals

class SpreadsTest {
    @Test
    fun `test calculateCustomerSpread`() {
        val input = TestCase(
            ftpRate = "0.039".toBigDecimal(),
            depositProducts = listOf(
                Product(
                    code = "CK001",
                    yearlyRate = "0.023".toBigDecimal(),
                ),
            ),
            loanProducts = listOf(
                Product(
                    code = "DK001",
                    yearlyRate = "0.050".toBigDecimal(),
                ),
            ),
            purchases = listOf(
                Purchase(
                    customerNo = "10001",
                    productCode = "CK001",
                    amount = "979290".toBigDecimal(),
                ),
                Purchase(
                    customerNo = "10002",
                    productCode = "DK001",
                    amount = "240120".toBigDecimal(),
                ),
            ),
        )

        val result = input.calculateCustomerSpread()

        assertEquals(listOf(
            Spread(
                customerNo = "10002",
                amount = "2641.32".toBigDecimal(),
            ),
            Spread(
                customerNo = "10001",
                amount = "15668.64".toBigDecimal(),
            ),
        ), result)
    }
}