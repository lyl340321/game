package com.njcb.game.util

import com.njcb.ams.repository.dao.mapper.BaseMapper
import org.springframework.transaction.PlatformTransactionManager
import org.springframework.transaction.TransactionDefinition
import org.springframework.transaction.support.TransactionTemplate
import java.io.Serializable

/**
 * 确保更新成功
 */
fun <T, E, PK : Serializable> BaseMapper<T, E, PK>.ensureUpdateByPrimaryKey(record: T): T {
    val affected = updateByPrimaryKey(record)
    assertEqual(1, affected) { "更新实体失败，受影响条数$affected: $record" }
    return record
}

/** 在新的独立事务中执行给定操作 */
@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
fun <T> PlatformTransactionManager.executeInNewTransaction(block: () -> T): T {
    return TransactionTemplate(this).apply {
        propagationBehavior = TransactionDefinition.PROPAGATION_REQUIRES_NEW
    }.execute {
        block()
    }
}
