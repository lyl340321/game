package com.njcb.game.util

import com.njcb.ams.support.exception.ErrorCode
import com.njcb.ams.util.AmsAssert
import com.njcb.ams.util.AmsUtils
import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.contract

fun <T> assertNotNull(input: T?, message: String = "传入参数不能为空"): T {
    AmsAssert.notNull(input, message)

    return input!!
}

inline fun <T> assertNotNull(input: T?, messageProvider: () -> String): T {
    if (AmsUtils.isNull(input)) {
        fail(messageProvider())
    }

    return input!!
}

fun fail(message: String, errorCode: ErrorCode? = null): Nothing {
    if (errorCode == null) {
        AmsAssert.fail(message)
    } else {
        AmsAssert.fail(message, errorCode)
    }
    error("Should not reach here")
}

fun <T> assertEqual(expected: T, actual: T, message: String) {
    if (expected != actual) {
        fail(message)
    }
}

inline fun <T> assertEqual(expected: T, actual: T, messageProvider: () -> String) {
    if (expected != actual) {
        fail(messageProvider())
    }
}

fun <T> assertNotEqual(illegal: T, actual: T, message: String) {
    if (illegal == actual) {
        fail(message)
    }
}

inline fun <T> assertNotEqual(illegal: T, actual: T, messageProvider: () -> String) {
    if (illegal == actual) {
        fail(messageProvider())
    }
}

@OptIn(ExperimentalContracts::class)
fun assertTrue(expression: Boolean, message: String) {
    contract {
        returns() implies (expression)
    }
    if (!expression) {
        fail(message)
    }
}

@OptIn(ExperimentalContracts::class)
inline fun assertTrue(expression: Boolean, messageProvider: () -> String) {
    contract {
        returns() implies (expression)
    }
    if (!expression) {
        fail(messageProvider())
    }
}
