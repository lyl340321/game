package com.njcb.game.util

object LevenShtein {
    fun levenshteinDistance(source: String, target: String): Int {
        val m = source.length
        val n = target.length
        val dp = Array(m + 1) { IntArray(n + 1) }

        for (i in 0..m) {
            dp[i][0] = i
        }

        for (j in 0..n) {
            dp[0][j] = j
        }

        for (i in 1..m) {
            for (j in 1..n) {
                val cost = if (source[i - 1] == target[j - 1]) 0 else 1
                dp[i][j] = minOf(
                    dp[i - 1][j] + 1,    // Deletion
                    dp[i][j - 1] + 1,    // Insertion
                    dp[i - 1][j - 1] + cost // Substitution
                )
            }
        }

        return dp[m][n]
    }

    // 回溯算法，用于找出从source到target的编辑路径
    fun levenshteinBacktrace(source: String, target: String): List<String> {
        val m = source.length
        val n = target.length
        val dp = Array(m + 1) { IntArray(n + 1) }
        val path = mutableListOf<String>()

        // 初始化DP表
        for (i in 0..m) {
            dp[i][0] = i
        }
        for (j in 0..n) {
            dp[0][j] = j
        }

        for (i in 1..m) {
            for (j in 1..n) {
                val cost = if (source[i - 1] == target[j - 1]) 0 else 1
                dp[i][j] = minOf(
                    dp[i - 1][j] + 1,    // Deletion
                    dp[i][j - 1] + 1,    // Insertion
                    dp[i - 1][j - 1] + cost // Substitution
                )
            }
        }

        // 回溯
        var i = m
        var j = n
        while (i > 0 && j > 0) {
            val diag = dp[i - 1][j - 1]
            val up = dp[i][j - 1]
            val left = dp[i - 1][j]
            val min = minOf(diag, up, left)

            if (source[i - 1] == target[j - 1] && diag == min) {
                path.add("Keep ${source[i - 1]}")
                i--
                j--
            } else if (left == min) {
                path.add("Delete ${source[i - 1]}")
                i--
            } else if (up == min) {
                path.add("Insert ${target[j - 1]} before ${source.substring(i)}")
                j--
            }
        }

        while (i > 0) {
            path.add("Delete ${source[i - 1]}")
            i--
        }

        while (j > 0) {
            path.add("Insert ${target[j - 1]} at the beginning")
            j--
        }

        path.reverse()
        return path
    }

    fun main() {
        val source = "kitten"
        val target = "sitting"
        println("Levenshtein Distance: ${levenshteinDistance(source, target)}")
        val path = levenshteinBacktrace(source, target)
        println("Edit Path:")
        path.forEach(::println)
    }
}