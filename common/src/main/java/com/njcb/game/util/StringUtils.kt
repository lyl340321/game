package com.njcb.game.util

import java.util.concurrent.ThreadLocalRandom
import kotlin.streams.asSequence

private val ALPHABET = ('A'..'Z') + ('a'..'z') + ('0'..'9')

fun randomString(length: Int): String = ThreadLocalRandom.current()
    .ints(length.toLong(), 0, ALPHABET.size)
    .asSequence()
    .map(ALPHABET::get)
    .joinToString("")