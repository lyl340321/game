package com.njcb.game.util

import com.njcb.ams.support.exception.ExceptionUtil

object ExceptionUtil {
    fun throwAppException(msg: String, errorCode: String? = null): Nothing {
        if (errorCode != null) {
            ExceptionUtil.throwAppException(msg, errorCode)
        } else {
            ExceptionUtil.throwAppException(msg)
        }
        fail("Unexpected")
    }
}