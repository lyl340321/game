package com.njcb.game.util

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.json.JsonWriteFeature
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.json.JsonMapper
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.jacksonTypeRef
import com.fasterxml.jackson.module.kotlin.readValue

object BeanUtil {

    private val mapper = JsonMapper
        .builder()
        .addModule(Jdk8Module())
        .addModule(JavaTimeModule())
        .addModule(KotlinModule())
        .enable(JsonGenerator.Feature.WRITE_BIGDECIMAL_AS_PLAIN)
        .enable(JsonWriteFeature.WRITE_NUMBERS_AS_STRINGS)
        .build()

    fun toJSONMap(pojo: Any): Map<String, Any> {
        val json = mapper.writeValueAsString(pojo)
        return mapper.readValue(json)
    }

    fun <T> toObject(map: Map<String, Any>, toValueTypeRef: TypeReference<T>): T {
        return mapper.convertValue(map, toValueTypeRef)
    }

    inline fun <reified T> toObject(map: Map<String, Any>): T {
        return toObject(map, jacksonTypeRef())
    }
}
