package com.njcb.game.service

import com.fasterxml.jackson.core.json.JsonReadFeature
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.databind.json.JsonMapper
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.jacksonTypeRef
import com.njcb.game.util.fail
import org.springframework.stereotype.Service
import java.nio.file.Path
import kotlin.io.path.exists
import kotlin.io.path.extension
import kotlin.io.path.nameWithoutExtension

@Service
class FileIOService {
    val objectMapper: ObjectMapper = JsonMapper
        .builder()
        .addModule(Jdk8Module())
        .addModule(JavaTimeModule())
        .addModule(KotlinModule())
        // 允许读取 jsonc 文件 （即带注释的json文件）
        .enable(JsonReadFeature.ALLOW_JAVA_COMMENTS)
        .enable(JsonReadFeature.ALLOW_TRAILING_COMMA)
        .enable(SerializationFeature.INDENT_OUTPUT)
        .build()

    fun <T> readJson(path: Path, valueTypeRef: TypeReference<T>): T {
        if (path.exists()) {
            return objectMapper.readValue(path.toFile(), valueTypeRef)
        } else if (path.extension.lowercase() == "json") {
            // Try read the jsonc file
            val jsonc = path.resolveSibling(path.nameWithoutExtension + ".jsonc")
            if (jsonc.exists()) {
                return objectMapper.readValue(jsonc.toFile(), valueTypeRef)
            }
        }

        fail("指定的文件 $path 不存在")
    }

    fun writeJson(path: Path, value: Any) {
        objectMapper.writeValue(path.toFile(), value)
    }
}

inline fun <reified T> FileIOService.readJson(path: Path): T = readJson(path, jacksonTypeRef())
