package com.njcb.game.games.doraemon

import java.util.concurrent.ThreadLocalRandom

data class TreeNode(val value: Double, val left: TreeNode?, val right: TreeNode?)

data class Result(val value: Double)
data class TestCase(val tree: TreeNode, val result: Result) {
    fun visit(node: TreeNode?): DoubleArray {
        if (node == null) return doubleArrayOf(0.0, 0.0)
        val left = visit(node.left)
        val right = visit(node.right)
        val select = node.value + left[1] + right[1]
        val noSelect = left.max() + right.max()
        return doubleArrayOf(select, noSelect)
    }

    fun generate(depth: Int, leftRate: Double = 0.7, rightRate: Double = 0.7): TreeNode? {
        if (depth == 0) return null
        val r = ThreadLocalRandom.current()
        val value = r.nextDouble(1.0, 8.0)
        val left = if (r.nextDouble() < leftRate) generate(depth - 1) else null
        val right = if (r.nextDouble() < rightRate) generate(depth - 1) else null
        return TreeNode(value, left, right)
    }
}

fun TestCase.calculateMaxGold(): Result {
    return Result(visit(this.tree).max())
}