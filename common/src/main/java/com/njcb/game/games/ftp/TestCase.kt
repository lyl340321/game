package com.njcb.game.games.ftp

import java.math.BigDecimal

data class Product(
    val code: String,
    val yearlyRate: BigDecimal,
)

data class Purchase(
    val customerNo: String,
    val productCode: String,
    val amount: BigDecimal,
)

data class TestCase(
    val ftpRate: BigDecimal,
    val depositProducts: List<Product>,
    val loanProducts: List<Product>,
    val purchases: List<Purchase>,
)

data class Spread(
    val customerNo: String,
    val amount: BigDecimal,
)

/** 计算每个客户的利差 */
fun TestCase.calculateCustomerSpread(): List<Spread> {
    // 我们利用如下特性简化计算：
    // 对于存款，产品利率一定小于FTP利率
    // 对于贷款，产品利率一定大于FTP利率
    // 因此我们直接把所有产品当作存款来计算利差，
    // 如果利差是负数，那么该产品就是贷款产品，最终利差只需要简单取反，
    // 即我们可以直接对计算出的利差取绝对值
    val allProducts = (depositProducts + loanProducts)
        .associateBy { it.code }

    return purchases
        .groupBy { it.customerNo }
        .mapValues { (_, ps) ->
            ps.fold("0.00".toBigDecimal()) { acc, p ->
                acc + p.amount * (ftpRate - allProducts[p.productCode]!!.yearlyRate).abs()
            }
        }.map { (cust, amount) ->
            Spread(customerNo = cust, amount = amount.setScale(2))
        }.sortedWith(
            Comparator
                .comparing(Spread::amount) // 先按利差排序
                .thenComparing(Spread::customerNo) // 如果相同，则按客户序号排序
        )
}
