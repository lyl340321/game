package com.njcb.game.games.networth

import com.fasterxml.jackson.annotation.JsonFormat
import java.math.BigDecimal
import java.math.RoundingMode
import java.time.LocalDate
import java.time.temporal.ChronoUnit


data class Purchase(
    val productCode: String,
    val amount: BigDecimal,

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    val buyDate: LocalDate,
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    val sellDate: LocalDate,
)

data class TestCase(
    val purchases: List<Purchase>,
)

fun TestCase.calculateGain(navManager: NAVManager): BigDecimal {
    return purchases.map {
        val navBuy = navManager.getNav(it.productCode, it.buyDate.format(NAVManager.dateFormat))!!
        val navSell = navManager.getNav(it.productCode, it.sellDate.format(NAVManager.dateFormat))!!

        val holdingDays = ChronoUnit.DAYS.between(
            it.buyDate,
            it.sellDate
        )

        ((navSell.totalValue - navBuy.totalValue) * 360.toBigDecimal() * it.amount)
            .divide(holdingDays.toBigDecimal() * navBuy.totalValue, RoundingMode.HALF_UP)
            .setScale(2, RoundingMode.HALF_UP)
    }.fold(BigDecimal.ZERO, BigDecimal::add)
        .setScale(2, RoundingMode.HALF_UP)
}
