package com.njcb.game.games.bpm

import com.njcb.ams.pojo.dto.standard.EntityResponse
import com.njcb.ams.pojo.dto.standard.PageResponse
import java.math.BigDecimal
import java.sql.Time

data class BusinessSubmitRequest(
    val businessNo: String,
    val operator: String,
    val businessAmount: BigDecimal
)

open class CommonResponse(
    val success: Boolean,
    val code: Int,
    val msg: String
)

enum class ApproveResult {
    agree, disagree
}

data class BusinessApproveRequest(
    val businessNo: String,
    val operator: String,
    val approveResult: ApproveResult
)

data class QueryApproveRecord(
    val businessNo: String
)

data class NodeInfo(
    val nodeName: String,
    val operator: String,
    val approveResult: ApproveResult,
    val startTime: Time,
    val endTime: Time
){
    override fun equals(other: Any?): Boolean {
        return super.equals(other)
    }

    override fun hashCode(): Int {
        var result = nodeName.hashCode()
        result = 31 * result + operator.hashCode()
        result = 31 * result + approveResult.hashCode()
        result = 31 * result + startTime.hashCode()
        result = 31 * result + endTime.hashCode()
        return result
    }
}

data class QueryApproveRecordResponse(
    val rows: PageResponse<NodeInfo>
) : CommonResponse(false, 0, "")

class TestCase {
}