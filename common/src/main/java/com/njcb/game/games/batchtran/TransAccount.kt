package com.njcb.game.games.batchtran

import java.math.BigDecimal


data class TestAccount(
    val name: String,
    val number: String,
)

data class TransAccount(
    val payer: TestAccount,
    val payee: TestAccount,
    val amount: BigDecimal,
)

fun List<TransAccount>.sumTransactions(): List<TransAccount> = this.groupBy {
    // 按账号分组， 对于每一个转账记录，分组的 key 为付款人账号和收款人账号按升序排列，
    // 如：付款人账号 1001 收款人账号 1002 则 key 为 (1001, 1002)，
    // 反之如果付款人账号 1002 收款人账号 1001 则 key 也为 (1001, 1002)，
    // 这样我们可以把同一对账号之间的双向转账合并至同一个组中
    if (it.payer.number < it.payee.number) {
        it.payer to it.payee
    } else {
        it.payee to it.payer
    }
}.map { (p, txs) ->
    // 统计每个分组的净值
    val (payer, payee) = p // 我们假定账号小的为付款人，账号大的为收款人
    val totalAmount = txs.sumOf { tx ->
        if (tx.payer == payer) tx.amount
        else tx.amount.negate()// 实际的付款方向和我们上面假定的相反，则金额取负
    }

    if (totalAmount.signum() >= 0) {
        // 总金额为正（在这里为0的话也当作正），则合计的付款方向相同
        TransAccount(payer, payee, totalAmount)
    } else {
        // 总金额为负，则反向
        TransAccount(payee, payer, totalAmount.abs())
    }
}.filter { // 去除金额为0的转账
    it.amount.signum() != 0
}
