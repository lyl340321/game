package com.njcb.game.games.networth

import org.springframework.stereotype.Component
import java.math.BigDecimal
import java.time.format.DateTimeFormatter


data class NetAssetValue(
    val dailyValue: BigDecimal,
    val totalValue: BigDecimal,
)

@Component
class NAVManager {
    private val values: Map<String, Map<String, NetAssetValue>> by lazy {
        mutableMapOf<String, MutableMap<String, NetAssetValue>>().also { result ->
            // 读取csv文件
            javaClass.classLoader
                .getResourceAsStream("static/game_files/net_asset_value.csv")!!
                .bufferedReader(Charsets.UTF_8)
                .lineSequence()
                .drop(1)
                .filter(String::isNotBlank)
                .forEach { line ->
                    val (code, date, navStr, totalNavStr) = line.split(',')

                    val nav = NetAssetValue(
                        dailyValue = navStr.toBigDecimal(),
                        totalValue = totalNavStr.toBigDecimal(),
                    )

                    result.getOrPut(code) { mutableMapOf() }[date] = nav
                }
        }
    }

    fun getNav(code: String, date: String): NetAssetValue? = values[code]?.get(date)

    val allProducts
        get() = values.keys

    fun getAvailableDates(code: String): Set<String> {
        return values[code]!!.keys
    }

    companion object {
        val dateFormat = DateTimeFormatter.ofPattern("yyyyMMdd")!!
    }
}
