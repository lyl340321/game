package com.njcb.game.games.atm

import kotlin.math.pow
import kotlin.math.sqrt

data class PointM(
    val x: Double,
    val y: Double,
)

data class Atm(
    val id: Int,
    val location: PointM,
    val left: Double
)

data class Client(
    val location: PointM,
    val require: Double,
    val k: Double,
)

data class Result(
    val id: Int,
    val satisfaction: String
)

data class TestCase(
    val client: Client,
    val atms: List<Atm>,
    val result: Result
)

fun TestCase.calculateSatisfiedAtm(): Result {
    val map = mutableMapOf<Int, Double>()

    atms.filter { atm -> atm.left >= client.require }
        .forEach { atm ->
            map[atm.id] =
                sqrt((atm.location.x - client.location.x).pow(2) + (atm.location.y - client.location.y).pow(2)) - client.k * client.require
        }
    val entry: Map.Entry<Int, Double>? = map.maxByOrNull { it.value }
    return entry?.let { Result(it.key, "%.2f".format(it.value)) } ?: Result(-1, "")
}