# 环境要求
- java8
- MySQL:
    - 本机安装，端口 3306
    - 启用 InnoDB
    - 表名大小写不敏感: `--lower_case_table_names=1`
    - 账号 ams 密码 ams
    - 有数据库 ams，且 ams 用户有对该库的全部权限

------

# 初始化数据库
- 可以直接用以下命令在 Docker 中启动一个符合需求的 MySQL 实例：
  ```shell
  docker run --name game-mysql -d -p 3306:3306 --restart   unless-stopped -e MYSQL_ROOT_PASSWORD=root -v game_mysql:/var/lib/  mysql mysql:8.0.32 mysqld --lower_case_table_names=1
  ```
  > 安全起见最好改一下root密码

- 用 root 用户创建好 ams 用户以及 ams 数据库
- 用 ams 用户依次执行 sql 文件夹下的数据库初始化脚本

-------
# 准备工作目录
- 复制整个 njcb_games 文件夹到当前用户目录（~）下

------

# 启动应用
执行以下命令启动app
```shell
java -jar game.jar -Dspring.profiles.active=dev
```
注意要用 java 8

------
# 首次运行配置
首次运行时需要在应用管理界面配置定时任务以定时触发评分
- 访问 http://localhost:8888/contest/
- 用管理员账户登录（默认为admin密码88888888)
- 选择 系统管理 > 系统设置 > 定时任务管理 > 新增
- 输入以下信息并保存:
    - 任务名称: 检查验证题目
    - 触发方式：定时
    - 执行函数：com.njcb.game.service.GameCheckTask
    - 表达式：0/30 * * * * ? *
- 重启整个应用即可

-------
# 参数配置
- 访问 http://localhost:8888/contest/ 并用管理员账号登录
- 选择 系统管理 > 系统设置 > 系统参数管理
- 目前系统有以下参数可以在此处修改（参数类型统一为 GAME）：

| 参数名         | 说明                                                                                |
|-------------|-----------------------------------------------------------------------------------|
| context-url | 比赛服务器基准URL。比赛服务器给选手机器发请求时，若请求中需要包含指向比赛判分服务器的URL（比如批量并账题目中的批量并账文件下载地址），会使用该参数作为前缀。 |
