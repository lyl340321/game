package com.njcb.game.bootconfig;

import com.njcb.ams.bootconfig.WebSecurityConfiguration;
import com.njcb.ams.support.annotation.AmsConfig;

import java.util.List;

/**
 * @author srxhx207
 */
@AmsConfig
public class GameSecurityConfiguration extends WebSecurityConfiguration {

	@Override
	public void permitMatchers(List<String> antPatterns) {
		antPatterns.add("/register");
		antPatterns.add("/result/**");
		antPatterns.add("/static/**");
		antPatterns.add("/index.html");
		antPatterns.add("/billboard/**");
		antPatterns.add("/resource/**");
		antPatterns.add("/download/**");
		antPatterns.add("/submit/**");
		antPatterns.add("/game_files/**");
		antPatterns.add("/crawler/**");
	}

}
