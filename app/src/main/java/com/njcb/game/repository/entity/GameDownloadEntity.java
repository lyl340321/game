package com.njcb.game.repository.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * AMS:
 * Copyright : Copyright (c) 2017
 * Company: NJCB
 *
 * @author AMS开发平台
 * @version 1.2.5 2023-03-01 16:14:43
 * @see 2023-03-01 16:14:43
 */
@Entity
@Table(name = "GAME_QUESTION")
public class GameDownloadEntity implements Serializable {
    private static final long serialVersionUID = -58357098209016595L;
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    /**
     * 选手工号, NULL的话所有人都能下载
     */
    private String runnersNo;
    /**
     * 文件名
     */
    private String fileName;
    /**
     * 文件路径
     */
    private String filePath;
    
    private Integer conductUser;
    
    private String conductTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRunnersNo() {
        return runnersNo;
    }

    public void setRunnersNo(String runnersNo) {
        this.runnersNo = runnersNo;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public Integer getConductUser() {
        return conductUser;
    }

    public void setConductUser(Integer conductUser) {
        this.conductUser = conductUser;
    }

    public String getConductTime() {
        return conductTime;
    }

    public void setConductTime(String conductTime) {
        this.conductTime = conductTime;
    }

}

