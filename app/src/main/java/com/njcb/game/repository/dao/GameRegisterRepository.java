/*
 * GameRegister.java
 * Copyright(C) trythis.cn
 * 2022年12月17日 19时46分25秒Created
 */
package com.njcb.game.repository.dao;

import com.njcb.ams.repository.dao.base.BaseJpaRepository;
import com.njcb.game.repository.entity.GameRegisterEntity;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.LockModeType;
import java.util.List;
import java.util.Map;

@Repository
public interface GameRegisterRepository extends BaseJpaRepository<GameRegisterEntity,Integer> {

    GameRegisterEntity findByRunnersNo(String runnersNo);

    @Lock(value = LockModeType.PESSIMISTIC_WRITE)
    GameRegisterEntity findByToken(String token);

    List<GameRegisterEntity> findByTokenNotNull();


    @Query(value = "select r.department as department,  " +
            "       count(1) as total" +
            "  from game_register r" +
            "    group by r.department", nativeQuery = true)
    List<Map<String, String>> selectDepartmentsScore();

}