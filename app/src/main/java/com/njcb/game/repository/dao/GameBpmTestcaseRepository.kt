package com.njcb.game.repository.dao

import com.njcb.ams.repository.dao.base.BaseJpaRepository
import com.njcb.game.repository.entity.GameBpmTestcaseEntity
import org.springframework.data.jpa.domain.Specification
import org.springframework.stereotype.Repository

@Repository
interface GameBpmTestcaseRepository : BaseJpaRepository<GameBpmTestcaseEntity, Long> {
}