package com.njcb.game.repository.dao;

import com.njcb.ams.repository.dao.base.BaseJpaRepository;
import com.njcb.game.repository.entity.GameScoreEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


/**
 * AMS:
 * Copyright : Copyright (c) 2017
 * Company: NJCB
 *
 * @author AMS开发平台
 * @version 1.2.5 2023-02-14 11:42:50
 * @see 2023-02-14 11:42:50
 */
@Repository
public interface GameScoreRepository extends BaseJpaRepository<GameScoreEntity, Integer> {

    List<GameScoreEntity> findByRunnersNo(String runnersNo);
    List<GameScoreEntity> findByQuesNo(String quesNo);
    GameScoreEntity findByRunnersNoAndQuesNo(String runnersNo, String quesNo);

    @Query(value = "select s.runners_no as runners_no, " +
            "       s.runners_name as runners_name, " +
            "       sum(s.score) as score, " +
            "       min(s.score_time) as score_time " +
            "  from GAME_SCORE s " +
            "  where s.score is not null" +
            "   group by s.runners_no, s.runners_name " +
            "   order by sum(s.score) desc, min(s.score_time)  ", nativeQuery = true)
    List<Map<String,Object>> selectScoreSummarise();
}

