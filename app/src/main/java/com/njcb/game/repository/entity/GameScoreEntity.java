package com.njcb.game.repository.entity;

import javax.persistence.*;
import java.util.Date;
import java.io.Serializable;

/**
 * 得分信息表
 * AMS:
 * Copyright : Copyright (c) 2017
 * Company: NJCB
 *
 * @author AMS开发平台
 * @version 1.2.5 2023-02-14 16:39:45
 * @see 2023-02-14 16:39:45
 */
@Entity
@Table(name = "GAME_SCORE")
public class GameScoreEntity implements Serializable {
    private static final long serialVersionUID = 311945813805818978L;
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    /**
     * 选手工号
     */
    private String runnersNo;
    /**
     * 题目编号
     */
    private String quesNo;
    /**
     * 选手名字
     */
    private String runnersName;
    /**
     * 题目标题
     */
    private String quesTitle;
    /**
     * 分数
     */
    private Integer score;
    /**
     * 当前分数的提交编号
     */
    private Integer scoreAttempt;
    
    private Integer conductUser;
    
    private String conductTime;
    /**
     * 评分时间
     */
    private Date scoreTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRunnersNo() {
        return runnersNo;
    }

    public void setRunnersNo(String runnersNo) {
        this.runnersNo = runnersNo;
    }

    public String getQuesNo() {
        return quesNo;
    }

    public void setQuesNo(String quesNo) {
        this.quesNo = quesNo;
    }

    public String getRunnersName() {
        return runnersName;
    }

    public void setRunnersName(String runnersName) {
        this.runnersName = runnersName;
    }

    public String getQuesTitle() {
        return quesTitle;
    }

    public void setQuesTitle(String quesTitle) {
        this.quesTitle = quesTitle;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getScoreAttempt() {
        return scoreAttempt;
    }

    public void setScoreAttempt(Integer scoreAttempt) {
        this.scoreAttempt = scoreAttempt;
    }

    public Integer getConductUser() {
        return conductUser;
    }

    public void setConductUser(Integer conductUser) {
        this.conductUser = conductUser;
    }

    public String getConductTime() {
        return conductTime;
    }

    public void setConductTime(String conductTime) {
        this.conductTime = conductTime;
    }

    public Date getScoreTime() {
        return scoreTime;
    }

    public void setScoreTime(Date scoreTime) {
        this.scoreTime = scoreTime;
    }

}

