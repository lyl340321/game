package com.njcb.game.repository.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 题目信息表
 */
@Entity
@Table(name = "GAME_QUESTION")
public class GameQuestionEntity implements Serializable {
    private static final long serialVersionUID = -95196011378318456L;
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    /**
     * 题目编号
     */
    private String quesNo;
    /**
     * 标题
     */
    private String quesTitle;
    /**
     * 简介
     */
    @Column(length = 4000)
    private String quesContext;
    /**
     * 是否启用
     */
    private Integer enabled;
    /**
     * 评分模式 SQL/CLASS
     */
    private String checkType;
    /**
     * 评分用SQL
     */
    private String checkSql;
    /**
     * 评分用class的FQ名，或者@bean名称
     */
    private String checkClass;
    /**
     * 评分超时秒数
     */
    private Integer checkTimeout;
    /**
     * 分数
     */
    private Integer score;
    
    private Integer conductUser;
    
    private String conductTime;
    /**
     * 允许用户直接提交
     */
    private Integer allowSubmit;

    /**
     * 题目得分率，不持久化
     */
    @Transient
    private Double scoreRate;

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    private String filePath;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getQuesNo() {
        return quesNo;
    }

    public void setQuesNo(String quesNo) {
        this.quesNo = quesNo;
    }

    public String getQuesTitle() {
        return quesTitle;
    }

    public void setQuesTitle(String quesTitle) {
        this.quesTitle = quesTitle;
    }

    public String getQuesContext() {
        return quesContext;
    }

    public void setQuesContext(String quesContext) {
        this.quesContext = quesContext;
    }

    public Integer getEnabled() {
        return enabled;
    }

    public void setEnabled(Integer enabled) {
        this.enabled = enabled;
    }

    public String getCheckType() {
        return checkType;
    }

    public void setCheckType(String checkType) {
        this.checkType = checkType;
    }

    public String getCheckSql() {
        return checkSql;
    }

    public void setCheckSql(String checkSql) {
        this.checkSql = checkSql;
    }

    public String getCheckClass() {
        return checkClass;
    }

    public void setCheckClass(String checkClass) {
        this.checkClass = checkClass;
    }

    public Integer getCheckTimeout() {
        return checkTimeout;
    }

    public void setCheckTimeout(Integer checkTimeout) {
        this.checkTimeout = checkTimeout;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getConductUser() {
        return conductUser;
    }

    public void setConductUser(Integer conductUser) {
        this.conductUser = conductUser;
    }

    public String getConductTime() {
        return conductTime;
    }

    public void setConductTime(String conductTime) {
        this.conductTime = conductTime;
    }

    public Integer getAllowSubmit() {
        return allowSubmit;
    }

    public void setAllowSubmit(Integer allowSubmit) {
        this.allowSubmit = allowSubmit;
    }

    public Double getScoreRate() {
        return scoreRate;
    }

    public void setScoreRate(Double scoreRate) {
        this.scoreRate = scoreRate;
    }
}

