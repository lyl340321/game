package com.njcb.game.repository.entity

import javax.persistence.*

@Entity
@Table(name = "GAME_BPM_TESTCASE")
data class GameBpmTestcaseEntity(
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    val id: Long,
    val quesNo: String?,
    val seqNo: Int,
    val url: String?,
    val exchangeMethod: String?,
    @Column(length = 2048) val exchangeJson: String?,
    val exchangeClass: String?,
    val exchangeHeader: String?,
    @Column(length = 2048) val expectedJson: String?,
    val expectedClass: String?,
    val compareProperties: String?,

    val sleepSeconds: Int,
    val enabled: Boolean,
    val value: Double,

    )