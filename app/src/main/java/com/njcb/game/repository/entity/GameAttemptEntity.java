package com.njcb.game.repository.entity;

import javax.persistence.*;
import java.util.Date;
import java.io.Serializable;

/**
 * AMS:
 * Copyright : Copyright (c) 2017
 * Company: NJCB
 *
 * @author AMS开发平台
 * @version 1.2.5 2023-02-15 11:35:23
 * @see 2023-02-15 11:35:23
 */
@Entity
@Table(name = "GAME_ATTEMPT")
public class GameAttemptEntity implements Serializable {
    private static final long serialVersionUID = -58420195266878143L;
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    /**
     * 提交者工号
     */
    private String runnersNo;
    /**
     * 题目编号
     */
    private String quesNo;
    /**
     * 提交时间戳
     */
    private Date submitTime;
    /**
     * 评分状态
     */
    private String checkStatus;
    /**
     * 评分结果信息
     */
    @Column(length = 2048)
    private String checkResult;
    /**
     * 分数
     */
    private Integer score;

    private Integer conductUser;

    private String conductTime;
    /**
     * 评分时间
     */
    private Date scoreTime;

    public String getCheckFile() {
        return checkFile;
    }

    public void setCheckFile(String checkFile) {
        this.checkFile = checkFile;
    }

    private String checkFile;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRunnersNo() {
        return runnersNo;
    }

    public void setRunnersNo(String runnersNo) {
        this.runnersNo = runnersNo;
    }

    public String getQuesNo() {
        return quesNo;
    }

    public void setQuesNo(String quesNo) {
        this.quesNo = quesNo;
    }

    public Date getSubmitTime() {
        return submitTime;
    }

    public void setSubmitTime(Date submitTime) {
        this.submitTime = submitTime;
    }

    public String getCheckStatus() {
        return checkStatus;
    }

    public void setCheckStatus(String checkStatus) {
        this.checkStatus = checkStatus;
    }

    public String getCheckResult() {
        return checkResult;
    }

    public void setCheckResult(String checkResult) {
        this.checkResult = checkResult;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getConductUser() {
        return conductUser;
    }

    public void setConductUser(Integer conductUser) {
        this.conductUser = conductUser;
    }

    public String getConductTime() {
        return conductTime;
    }

    public void setConductTime(String conductTime) {
        this.conductTime = conductTime;
    }

    public Date getScoreTime() {
        return scoreTime;
    }

    public void setScoreTime(Date scoreTime) {
        this.scoreTime = scoreTime;
    }

}

