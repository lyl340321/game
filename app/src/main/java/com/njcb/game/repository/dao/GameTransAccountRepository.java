package com.njcb.game.repository.dao;

import com.njcb.ams.repository.dao.base.BaseJpaRepository;
import com.njcb.game.repository.entity.GameTransAccountEntity;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.stereotype.Repository;

import javax.persistence.LockModeType;


/**
 * AMS:
 * Copyright : Copyright (c) 2017
 * Company: NJCB
 *
 * @author AMS开发平台
 * @version 1.2.5 2023-02-28 16:50:14
 * @see 2023-02-28 16:50:14
 */
@Repository
public interface GameTransAccountRepository extends BaseJpaRepository<GameTransAccountEntity, Integer> {

	@Lock(value = LockModeType.PESSIMISTIC_WRITE)
    GameTransAccountEntity findByGlobalSeq(String globalSeq);
}

