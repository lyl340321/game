package com.njcb.game.repository.dao;

import com.njcb.ams.repository.dao.base.BaseJpaRepository;
import com.njcb.game.repository.entity.GameCrawlerAttemptEntity;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.LockModeType;
import java.util.List;


/**
 * AMS:
 * Copyright : Copyright (c) 2017
 * Company: NJCB
 *
 * @author AMS开发平台
 * @version 1.2.5 2023-03-20 11:33:55
 * @see 2023-03-20 11:33:55
 */
@Repository
public interface GameCrawlerAttemptRepository extends BaseJpaRepository<GameCrawlerAttemptEntity, Integer> {



	List<GameCrawlerAttemptEntity> findByRunnersNoAndStatusNotIn(String runnersNo, List<String> values);

	List<GameCrawlerAttemptEntity> findByRunnersNo(String runnersNo, Sort sort);

	@Query(value = "select *" +
			"  from game_crawler_attempt t" +
			"  where t.runners_no = :runnersNo" +
			"  order by submit_time desc" +
			"  limit 1" +
			"  for update",nativeQuery = true)
    GameCrawlerAttemptEntity selectLatestAttemptForUpdate(String runnersNo);


	default GameCrawlerAttemptEntity selectLatestAttempt(@NotNull String runnersNo) {
		Sort sort = Sort.by(Sort.Direction.DESC,"SUBMIT_TIME");
		List<GameCrawlerAttemptEntity> attempts = findByRunnersNo(runnersNo,sort);
		if (attempts.isEmpty()) {
			return null;
		} else {
			return attempts.get(0);
		}
	}

	@Lock(value = LockModeType.PESSIMISTIC_WRITE)
    GameCrawlerAttemptEntity findByRunnersNoAndCheckAttempt(String runnersNo, int attemptId);
}
