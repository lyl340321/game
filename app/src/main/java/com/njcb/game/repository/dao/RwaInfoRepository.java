package com.njcb.game.repository.dao;

import com.njcb.ams.repository.dao.base.BaseJpaRepository;
import com.njcb.game.repository.entity.GameUploadEntity;
import com.njcb.game.repository.entity.RwaInfoEntity;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * AMS:
 * Copyright : Copyright (c) 2017
 * Company: NJCB
 *
 * @author AMS开发平台
 * @version 1.2.5 2023-04-21 11:15:01
 * @see 2023-04-21 11:15:01
 */
@Repository
public interface RwaInfoRepository extends BaseJpaRepository<RwaInfoEntity, Integer> {

    List<RwaInfoEntity> findBySerialNo(String serialNo);
}

