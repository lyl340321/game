/*
 * GameResult.java
 * Copyright(C) trythis.cn
 * 2022年12月13日 13时13分55秒Created
 */
package com.njcb.game.repository.dao;

import com.njcb.game.util.SqlProviderUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * @author LOONG
 */
@Repository
public class GameCheckRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @NotNull
    public List<?> executeSql(String sqlStr, Map<String, String> paramsMap) {
        String executeSql = SqlProviderUtils.sqlProvider(sqlStr,paramsMap);
        List<Map<String, Object>> retList = this.jdbcTemplate.queryForList(executeSql);
        return retList;
    }

}