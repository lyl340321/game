package com.njcb.game.repository.dao;

import com.njcb.ams.repository.dao.base.BaseJpaRepository;
import com.njcb.game.repository.entity.GameAttemptEntity;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * AMS:
 * Copyright : Copyright (c) 2017
 * Company: NJCB
 *
 * @author AMS开发平台
 * @version 1.2.5 2023-02-14 16:23:28
 * @see 2023-02-14 16:23:28
 */
@Repository
public interface GameAttemptRepository extends BaseJpaRepository<GameAttemptEntity, Integer> {

	List<GameAttemptEntity> findByRunnersNo(String runnersNo, Sort sort);

	List<GameAttemptEntity> findByRunnersNoAndQuesNo(String runnersNo, String quesNo, Sort sort);

	GameAttemptEntity findByRunnersNoAndQuesNo(String runnersNo, String quesNo);

	default GameAttemptEntity selectLatestAttempt(String runnersNo, String quesNo) {
		Sort sort = Sort.by(Sort.Direction.DESC,"submitTime");
		List<GameAttemptEntity> attempts = findByRunnersNoAndQuesNo(runnersNo,quesNo,sort);
		if (attempts.isEmpty()) {
			return null;
		} else {
			return attempts.get(0);
		}
	}

	List<GameAttemptEntity> findByRunnersNoAndCheckStatus(String runnersNo, String checkStatus);
}

