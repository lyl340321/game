package com.njcb.game.repository.dao;

import com.njcb.ams.repository.dao.base.BaseJpaRepository;
import com.njcb.game.repository.entity.GameQuestionEntity;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * AMS:
 * Copyright : Copyright (c) 2017
 * Company: NJCB
 *
 * @author AMS开发平台
 * @version 1.2.5 2023-02-14 16:23:25
 * @see 2023-02-14 16:23:25
 */
@Repository
public interface GameQuestionRepository extends BaseJpaRepository<GameQuestionEntity,Integer> {

	GameQuestionEntity findByQuesNo(@NotNull String quesNo);

	List<GameQuestionEntity> findByEnabledAndCheckType(Integer enabled, String checkType);

	List<GameQuestionEntity> findByEnabledAndCheckType(Integer enabled, String checkType, Sort sort);
}

