/*
 * GameRegister.java
 * Copyright(C) trythis.cn
 * 2022年12月21日 16时38分57秒Created
 */
package com.njcb.game.repository.entity;

import javax.persistence.*;

/**
 * 注册信息表
 */
@Entity
@Table(name = "GAME_REGISTER")
public class GameRegisterEntity {
    /**
     * 主键
     * 数据表:AMS.GAME_REGISTER.ID
     *
     * @trythis.cn 2022年12月21日 16时38分57秒
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    /**
     * 编码
     * 数据表:AMS.GAME_REGISTER.RUNNERS_NO
     *
     * @trythis.cn 2022年12月21日 16时38分57秒
     */
    private String runnersNo;

    /**
     * 姓名
     * 数据表:AMS.GAME_REGISTER.RUNNERS_NAME
     *
     * @trythis.cn 2022年12月21日 16时38分57秒
     */
    private String runnersName;

    /**
     * 服务地址
     * 数据表:AMS.GAME_REGISTER.SERVER_ADDR
     *
     * @trythis.cn 2022年12月21日 16时38分57秒
     */
    private String serverAddr;

    /**
     * 更新人
     * 数据表:AMS.GAME_REGISTER.CONDUCT_USER
     *
     * @trythis.cn 2022年12月21日 16时38分57秒
     */
    private Integer conductUser;

    /**
     * 更新时间
     * 数据表:AMS.GAME_REGISTER.CONDUCT_TIME
     *
     * @trythis.cn 2022年12月21日 16时38分57秒
     */
    private String conductTime;

    /**
     * 凭证
     * 数据表:AMS.GAME_REGISTER.TOKEN
     *
     * @trythis.cn 2022年12月21日 16时38分57秒
     */
    private String token;

    /**
     * 部门
     * 数据表:AMS.GAME_REGISTER.DEPARTMENT
     *
     * @trythis.cn 2022年12月21日 16时38分57秒
     */
    private String department;

    /**
     * 注册时间
     * 数据表:AMS.GAME_REGISTER.REGISTER_TIME
     *
     * @trythis.cn 2022年12月21日 16时38分57秒
     */
    private String registerTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRunnersNo() {
        return runnersNo;
    }

    public void setRunnersNo(String runnersNo) {
        this.runnersNo = runnersNo;
    }

    public String getRunnersName() {
        return runnersName;
    }

    public void setRunnersName(String runnersName) {
        this.runnersName = runnersName;
    }

    public String getServerAddr() {
        return serverAddr;
    }

    public void setServerAddr(String serverAddr) {
        this.serverAddr = serverAddr;
    }

    public Integer getConductUser() {
        return conductUser;
    }

    public void setConductUser(Integer conductUser) {
        this.conductUser = conductUser;
    }

    public String getConductTime() {
        return conductTime;
    }

    public void setConductTime(String conductTime) {
        this.conductTime = conductTime;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getRegisterTime() {
        return registerTime;
    }

    public void setRegisterTime(String registerTime) {
        this.registerTime = registerTime;
    }
}