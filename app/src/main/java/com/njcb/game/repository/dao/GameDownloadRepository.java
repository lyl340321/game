package com.njcb.game.repository.dao;

import com.njcb.ams.repository.dao.base.BaseJpaRepository;
import com.njcb.game.repository.entity.GameDownloadEntity;
import org.springframework.stereotype.Repository;


/**
 * AMS:
 * Copyright : Copyright (c) 2017
 * Company: NJCB
 *
 * @author AMS开发平台
 * @version 1.2.5 2023-03-01 16:07:12
 * @see 2023-03-01 16:07:12
 */
@Repository
public interface GameDownloadRepository extends BaseJpaRepository<GameDownloadEntity, Integer> {


}

