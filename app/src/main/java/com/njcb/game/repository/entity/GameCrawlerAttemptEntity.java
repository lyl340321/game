package com.njcb.game.repository.entity;

import javax.persistence.*;
import java.util.Date;
import java.io.Serializable;

/**
 * AMS:
 * Copyright : Copyright (c) 2017
 * Company: NJCB
 *
 * @author AMS开发平台
 * @version 1.2.5 2023-03-20 11:33:55
 * @see 2023-03-20 11:33:55
 */
@Entity
@Table(name = "GAME_CRAWLER_ATTEMPT")
public class GameCrawlerAttemptEntity implements Serializable {
    private static final long serialVersionUID = 857668161534802061L;
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    /**
     * 选手工号
     */
    private String runnersNo;
    /**
     * 提交时间戳
     */
    private Date submitTime;
    /**
     * 提交状态
     */
    private String status;
    /**
     * 评分编号
     */
    private Integer checkAttempt;
    
    private Integer conductUser;
    
    private String conductTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRunnersNo() {
        return runnersNo;
    }

    public void setRunnersNo(String runnersNo) {
        this.runnersNo = runnersNo;
    }

    public Date getSubmitTime() {
        return submitTime;
    }

    public void setSubmitTime(Date submitTime) {
        this.submitTime = submitTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getCheckAttempt() {
        return checkAttempt;
    }

    public void setCheckAttempt(Integer checkAttempt) {
        this.checkAttempt = checkAttempt;
    }

    public Integer getConductUser() {
        return conductUser;
    }

    public void setConductUser(Integer conductUser) {
        this.conductUser = conductUser;
    }

    public String getConductTime() {
        return conductTime;
    }

    public void setConductTime(String conductTime) {
        this.conductTime = conductTime;
    }

}

