package com.njcb.game.repository.entity;

import javax.persistence.*;
import java.util.Date;
import java.io.Serializable;

/**
 * AMS:
 * Copyright : Copyright (c) 2017
 * Company: NJCB
 *
 * @author AMS开发平台
 * @version 1.2.5 2023-04-23 17:01:20
 * @see 2023-04-23 17:01:20
 */
@Entity
@Table(name = "GAME_UPLOAD")
public class GameUploadEntity implements Serializable {
    private static final long serialVersionUID = -81536978701720476L;
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    /**
     * 评分编号
     */
    private Integer checkAttempt;
    /**
     * 上传状态
     */
    private String status;
    /**
     * 上传时间戳
     */
    private Date uploadTime;
    
    private Integer conductUser;
    
    private String conductTime;
    /**
     * 测试样例编号
     */
    private Integer caseNo;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCheckAttempt() {
        return checkAttempt;
    }

    public void setCheckAttempt(Integer checkAttempt) {
        this.checkAttempt = checkAttempt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getUploadTime() {
        return uploadTime;
    }

    public void setUploadTime(Date uploadTime) {
        this.uploadTime = uploadTime;
    }

    public Integer getConductUser() {
        return conductUser;
    }

    public void setConductUser(Integer conductUser) {
        this.conductUser = conductUser;
    }

    public String getConductTime() {
        return conductTime;
    }

    public void setConductTime(String conductTime) {
        this.conductTime = conductTime;
    }

    public Integer getCaseNo() {
        return caseNo;
    }

    public void setCaseNo(Integer caseNo) {
        this.caseNo = caseNo;
    }

}

