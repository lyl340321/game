package com.njcb.game.repository.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * AMS:
 * Copyright : Copyright (c) 2017
 * Company: NJCB
 *
 * @author AMS开发平台
 * @version 1.2.5 2023-03-01 11:05:15
 * @see 2023-03-01 11:05:15
 */
@Entity
@Table(name = "GAME_TRANS_ACCOUNT")
public class GameTransAccountEntity implements Serializable {
    private static final long serialVersionUID = -90929062423725554L;
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    /**
     * 提交编号
     */
    private Integer attempt;
    /**
     * 测试样例编号
     */
    private Integer caseNo;
    /**
     * 全局流水号
     */
    private String globalSeq;
    /**
     * 接口调用次数
     */
    private Integer invokeCount;
    
    private Integer conductUser;
    
    private String conductTime;
    /**
     * 选手工号
     */
    private String runnersNo;
    /**
     * 题目编号
     */
    private String quesNo;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAttempt() {
        return attempt;
    }

    public void setAttempt(Integer attempt) {
        this.attempt = attempt;
    }

    public Integer getCaseNo() {
        return caseNo;
    }

    public void setCaseNo(Integer caseNo) {
        this.caseNo = caseNo;
    }

    public String getGlobalSeq() {
        return globalSeq;
    }

    public void setGlobalSeq(String globalSeq) {
        this.globalSeq = globalSeq;
    }

    public Integer getInvokeCount() {
        return invokeCount;
    }

    public void setInvokeCount(Integer invokeCount) {
        this.invokeCount = invokeCount;
    }

    public Integer getConductUser() {
        return conductUser;
    }

    public void setConductUser(Integer conductUser) {
        this.conductUser = conductUser;
    }

    public String getConductTime() {
        return conductTime;
    }

    public void setConductTime(String conductTime) {
        this.conductTime = conductTime;
    }

    public String getRunnersNo() {
        return runnersNo;
    }

    public void setRunnersNo(String runnersNo) {
        this.runnersNo = runnersNo;
    }

    public String getQuesNo() {
        return quesNo;
    }

    public void setQuesNo(String quesNo) {
        this.quesNo = quesNo;
    }

}

