package com.njcb.game.repository.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * AMS:
 * Copyright : Copyright (c) 2017
 * Company: NJCB
 *
 * @author AMS开发平台
 * @version 1.2.5 2023-04-23 17:01:20
 * @see 2023-04-23 17:01:20
 */
@Entity
@Table(name = "RWA_INFO")
public class RwaInfoEntity implements Serializable {
    private static final long serialVersionUID = -81536978701720476L;
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonIgnore
    private Integer id;

    /**
     * 序列【表情】
     */
    @JsonIgnore
    private String serialNo;

    /**
     * 客户号
     */
    private String custNo;
    /**
     * 借据号
     */
    private String voucherNo;
    /**
     * 资产类型名称
     */
    private String assetTypeName;

    /**
     * 资产类型代码
     */
    private String assetTypeNo;

    /**
     * 资产期限
     */
    private String assetLimit;

    /**
     * 信用风险敞口余额
     */
    private BigDecimal riskBalance;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCustNo() {
        return custNo;
    }

    public void setCustNo(String custNo) {
        this.custNo = custNo;
    }

    public String getVoucherNo() {
        return voucherNo;
    }

    public void setVoucherNo(String voucherNo) {
        this.voucherNo = voucherNo;
    }

    public String getAssetTypeName() {
        return assetTypeName;
    }

    public void setAssetTypeName(String assetTypeName) {
        this.assetTypeName = assetTypeName;
    }

    public String getAssetTypeNo() {
        return assetTypeNo;
    }

    public void setAssetTypeNo(String assetTypeNo) {
        this.assetTypeNo = assetTypeNo;
    }

    public String getAssetLimit() {
        return assetLimit;
    }

    public void setAssetLimit(String assetLimit) {
        this.assetLimit = assetLimit;
    }

    public BigDecimal getRiskBalance() {
        return riskBalance;
    }

    public void setRiskBalance(BigDecimal riskBalance) {
        this.riskBalance = riskBalance;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }
}

