package com.njcb.game.application

import com.njcb.ams.pojo.dto.standard.PageResponse
import com.njcb.ams.support.annotation.Interaction
import com.njcb.ams.support.annotation.Trader
import com.njcb.ams.support.exception.ExceptionUtil
import com.njcb.ams.util.AmsAssert
import com.njcb.game.pojo.GameResultDetail
import com.njcb.game.pojo.RegisterInput
import com.njcb.game.repository.dao.GameRegisterRepository
import com.njcb.game.repository.dao.GameScoreRepository
import com.njcb.game.repository.entity.GameRegisterEntity
import com.njcb.game.repository.entity.GameScoreEntity
import com.njcb.game.service.QuestionsCheckService
import com.njcb.game.service.RegisterService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Service
import java.util.function.Consumer

/**
 * 系统基础功能。
 *
 * @author liuyanlong
 */
@Service
@Lazy(false)
@Interaction(groupName = "GAME")
class BusinessGameManager(
    private val registerService: RegisterService,
    private val questionsCheckService: QuestionsCheckService,
    @Autowired
    private var registerDAO: GameRegisterRepository,
    @Autowired
    private val gameScoreRepository: GameScoreRepository,
) {
    @Trader(tradeCode = "GM1001", tradeName = "参赛人信息注册")
    fun register(input: RegisterInput): String {
        return registerService.register(input)
    }

    @Trader(tradeCode = "GM1002", tradeName = "参赛人提交判分请求")
    fun submit(token: String, questNo: String) = questionsCheckService.submitAttempt(token, questNo, true)

    @Trader(tradeCode = "GM1003", tradeName = "题目验证结果查询")
    fun result(token: String): MutableList<GameResultDetail> {
        val gameRegisterEntity: GameRegisterEntity = registerDAO.findByToken(token)
        if (null == gameRegisterEntity) {
            ExceptionUtil.throwAppException("token[$token]不存在")
        }
        val results: List<GameScoreEntity> = gameScoreRepository.findByRunnersNo(gameRegisterEntity.runnersNo)
        val resultDetails: MutableList<GameResultDetail> = ArrayList()
        for (result in results) {
            val resultDetail = GameResultDetail()
            resultDetail.runnersNo = result.runnersNo
            resultDetail.runnersName = result.runnersName
            resultDetail.quesNo = result.quesNo
            resultDetail.quesTitle = result.quesTitle
            resultDetail.score = result.score
            resultDetail.checkResult = "验证通过"
            resultDetail.failReason = "未知"
            resultDetails.add(resultDetail)
        }
        return resultDetails
    }

    companion object {
        private val logger = LoggerFactory.getLogger(BusinessGameManager::class.java)
    }
}