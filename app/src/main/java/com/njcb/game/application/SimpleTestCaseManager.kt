package com.njcb.game.application

import com.njcb.game.pojo.SimpleTestCase

interface SimpleTestCaseManager {
    fun pickTestCases(questNo: String): List<SimpleTestCase>
}
