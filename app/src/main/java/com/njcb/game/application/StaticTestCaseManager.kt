package com.njcb.game.application

import com.njcb.game.pojo.SimpleTestCase
import com.njcb.game.pojo.StaticTestMetadata
import com.njcb.game.pojo.StaticTestSet
import com.njcb.game.service.FileIOService
import com.njcb.game.service.readJson
import com.njcb.game.util.assertEqual
import com.njcb.game.util.assertTrue
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import java.nio.file.Path
import java.nio.file.Paths
import kotlin.io.path.div
import kotlin.io.path.listDirectoryEntries

@Component
class StaticTestCaseManager(
    @Value("\${game.working-dir}")
    baseWorkingDir: String,
    private val fileIO: FileIOService,
): SimpleTestCaseManager {

    private val testCaseBaseDir = Paths.get("./njcb_games", "cases")

    override fun pickTestCases(questNo: String): List<SimpleTestCase> {
        // TODO: 缓存读取过的题目？

        val caseBaseDir = testCaseBaseDir / questNo
        // 读取metadata
        val metadata: StaticTestMetadata = fileIO.readJson(caseBaseDir / "metadata.json")
        // 校验metadata
        validateMetadata(metadata)

        // 随机挑选测试样例集合
        return metadata.testSets.flatMap { pickCases(caseBaseDir, it, fileIO) }
    }

    companion object {
        private fun validateMetadata(metadata: StaticTestMetadata) {
            // 所有题目分值加起来必须=100
            val totalScore = metadata.testSets.sumOf { it.score * it.count }
            assertEqual(100, totalScore) { "题目配置错误，总分值应该为 100，实际为 $totalScore" }
        }

        private fun pickCases(baseDir: Path, testSet: StaticTestSet, fileIO: FileIOService): List<SimpleTestCase> {
            val testSetDir = baseDir / testSet.dir

            val allCases = testSetDir
                // 列出目录下所有文件
                .listDirectoryEntries("*.{json,jsonc}")

            assertTrue(allCases.size >= testSet.count) {
                "测试样例不足，需要 ${testSet.count}，找到 ${allCases.size}"
            }

            // 随机选取 testSet.count 个测试用例
            return allCases
                .shuffled()
                .take(testSet.count)
                .map { fileIO.readJson<SimpleTestCase>(it).copy(score = testSet.score) }
        }
    }
}