package com.njcb.game.web.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * 用户操作类
 *
 * @author LOONG
 */
@RestController
@RequestMapping("/resource")
public class TemplateController {

    @ApiOperation(value = "报表信息", notes = "报表信息")
    @RequestMapping(value = "/report")
    @ResponseBody
    public ModelAndView report() {
        ModelAndView modelAndView = new ModelAndView("report.ftl");
        modelAndView.addObject("loginName", "loginName");
        modelAndView.addObject("userName", "loginName");
        modelAndView.addObject("empNo", "loginName");
        modelAndView.addObject("mobileNo", "loginName");
        modelAndView.addObject("roleCode", "loginName");
        return modelAndView;
    }

}
