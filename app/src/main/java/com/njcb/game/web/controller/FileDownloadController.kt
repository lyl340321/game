package com.njcb.game.web.controller

import com.njcb.ams.pojo.dto.standard.EntityResponse
import com.njcb.ams.pojo.dto.standard.Response
import com.njcb.ams.util.AmsDateUtils
import com.njcb.ams.util.AmsFileUtils
import com.njcb.ams.web.controller.common.FileUpDownloadController
import com.njcb.game.repository.dao.GameAttemptRepository
import com.njcb.game.repository.dao.GameRegisterRepository
import com.njcb.game.repository.entity.GameAttemptEntity
import com.njcb.game.repository.entity.GameDownloadEntity
import com.njcb.game.service.FileDownloadService
import com.njcb.game.util.ExceptionUtil
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import java.io.File
import java.nio.file.Paths
import java.util.concurrent.ThreadLocalRandom
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import kotlin.io.path.*

/**
 * @author LOONG
 */
@RestController
@Transactional //违反AMS规范：Controller里面不允许数据库操作
class FileDownloadController(
    private val downloadService: FileDownloadService,
    private val gameAttemptRepository: GameAttemptRepository,
    private val gameRegisterRepository: GameRegisterRepository,

    ) {

    /**
     * 文件下载接口
     */
    @RequestMapping(value = ["download/{id}"], method = [RequestMethod.GET])
    @ResponseBody
    fun downloadFile(
        request: HttpServletRequest,
        response: HttpServletResponse,
        @PathVariable id: Int,
        @RequestParam token: String, @Value("\${game.working-dir}") baseWorkingDir: String,
    ) {
        //特殊编码：005题, 未来可以再增加一个标志位. Excel题目默认如果不提供token的话是给提供Sample样例文件
        //  如果提供token则在Game_Attempt表中注册一个(随机)文件, 并在upload中检查与之对应的文件
        //  正常情况下应该是在FileDownloadService里面写但是这里空间太小了
        val f: GameDownloadEntity
        if (5 != id) {
            f = downloadService.downloadFile(id, token) ?: ExceptionUtil.throwAppException("文件不存在")
        } else {
            if ("" == token) { //Sample Excel File
                f = downloadService.downloadFile(5) ?: ExceptionUtil.throwAppException("文件${id}不存在")
                f.filePath = Paths.get(baseWorkingDir, f.filePath).pathString
                logger.info("下载样例文件：{}", f.filePath)
            } else { //Register Game Attempt
                val user = gameRegisterRepository.findByToken(token)
                if (user != null) {
                    val attempt = gameAttemptRepository.findByRunnersNoAndQuesNo(user.runnersNo, "005")
                    f = GameDownloadEntity()
                    if (attempt == null) {
                        val rand = ThreadLocalRandom.current()
                        val caseFiles = Paths.get(baseWorkingDir, "cases", "005", "source")
                        val cases = caseFiles.listDirectoryEntries("2024*.xlsx")
                        val caseCount = cases.count()
                        if (caseCount == 0) ExceptionUtil.throwAppException("005题未准备的样题")
                        val file = cases[rand.nextInt(0, caseCount)]
                        GameAttemptEntity().apply {
                            runnersNo = user.runnersNo
                            quesNo = "005" //Hardcode
                            submitTime = AmsDateUtils.getCurrentDate()
                            checkFile = file.relativeTo(Path(baseWorkingDir)).pathString //借用, 文件位置
                        }.also(gameAttemptRepository::save)
                        f.filePath = file.pathString
                        f.fileName = file.name
                    } else {
                        f.filePath = Paths.get(baseWorkingDir, attempt.checkFile).pathString
                        f.fileName = Paths.get(f.filePath).name
                    }
                } else {
                    ExceptionUtil.throwAppException("token is invalid")
                }
            }
        }
        logger.info("用户[{}]在客户端[{}]下载了文件[{}]", token, request.remoteAddr, f.id)

        AmsFileUtils.downFileForResponse(response, File(f.filePath), f.fileName)
    }

    /**
     * 文件上传接口
     */
    @RequestMapping(value = ["upload/{id}"], method = [RequestMethod.PUT])
    @ResponseBody
    fun uploadFile(
        @RequestParam("file") file: MultipartFile,
        @PathVariable id: Int,
        @RequestParam token: String, @Value("\${game.working-dir}") baseWorkingDir: String,
    ): Response {
        //文件上传的时候只能放在一个选手的目录下
        //配置005题上传的文件不能大于100k, 不然POI会爆掉
        val response = Response()
        if (id != 5) {
            response.message = "该题不能上传文件"
        } else {
            val user = gameRegisterRepository.findByToken(token)
            if (user != null) {
                val attempt = gameAttemptRepository.findByRunnersNoAndQuesNo(user.runnersNo, "005")
                if (attempt != null) {
                    val fileName = Path(attempt.checkFile).name
                    try {
                        file.transferTo(Paths.get(baseWorkingDir, "cases", "005", "target", fileName))
                        response.isSuccess = true
                    } catch (ex: Exception) {
                        response.message = ex.message
                    }
                } else {
                    response.message = "请先下载您需要处理的数据文件"
                }
            } else {
                response.message = "您上传的token:${token}无效，请核实"
            }
        }

        return response
    }

    companion object {
        private val logger = LoggerFactory.getLogger(FileUpDownloadController::class.java)
    }
}