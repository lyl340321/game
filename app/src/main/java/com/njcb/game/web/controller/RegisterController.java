package com.njcb.game.web.controller;

import com.njcb.ams.pojo.dto.standard.EntityResponse;
import com.njcb.ams.pojo.dto.standard.PageResponse;
import com.njcb.ams.util.AmsAssert;
import com.njcb.game.application.BusinessGameManager;
import com.njcb.game.pojo.GameResultDetail;
import com.njcb.game.pojo.RegisterInput;
import com.njcb.game.pojo.RegisterOutput;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 用户操作类
 *
 * @author LOONG
 */
@RestController
@RequestMapping(value = "")
@Api(value = "register", tags = "register")
public class RegisterController {

    private static final Logger logger = LoggerFactory.getLogger(RegisterController.class);

    @Autowired
    private BusinessGameManager businessManager;

    @ApiOperation(value = "注册", notes = "注册参赛人信息")
    @RequestMapping(value = "register", method = RequestMethod.POST)
    @ResponseBody
    public EntityResponse<RegisterOutput> register(@RequestBody RegisterInput input) {
        AmsAssert.notNull(input, "注册信息不能为空");
        String token = businessManager.register(input);
        RegisterOutput registerOutput = new RegisterOutput();
        registerOutput.setToken(token);
        logger.info("[{}]注册成功", input.getRunnersNo());
        return EntityResponse.build(registerOutput);
    }

    @ApiOperation(value = "题目验证结果查询", notes = "题目验证结果查询")
    @RequestMapping(value = "result/{token}")
    @ResponseBody
    public PageResponse<GameResultDetail> result(@PathVariable("token") String token) {
        AmsAssert.notNull(token, "[token]不能为空");
        List<GameResultDetail> resultDetails = businessManager.result(token);
        PageResponse<GameResultDetail> pageResponse = PageResponse.build(resultDetails, resultDetails.size());
        pageResponse.putExtField("token", token);
        return pageResponse;
    }

}
