package com.njcb.game.web.controller;

import com.google.common.collect.Maps;
import com.njcb.ams.pojo.dto.standard.EntityResponse;
import com.njcb.ams.util.AmsDateUtils;
import com.njcb.game.pojo.DepartmentNameCode;
import com.njcb.game.repository.dao.GameQuestionRepository;
import com.njcb.game.repository.dao.GameRegisterRepository;
import com.njcb.game.repository.dao.GameScoreRepository;
import com.njcb.game.repository.entity.GameQuestionEntity;
import com.njcb.game.repository.entity.GameScoreEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.text.DecimalFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 比赛大屏
 *
 * @author LOONG
 */
@RestController
@RequestMapping
@Api(value = "billboard", tags = "billboard")
public class GameBillboardController {

    @Value("${game.end-time}")
    private String endTime;
    @Value("${game.team-number}")
    private Integer teamNumber;
    @Value("${game.judge}")
    private String judge;

    @Resource
    private GameScoreRepository gameScoreRepository;
    @Resource
    private GameQuestionRepository questionRepository;
    @Resource
    private GameRegisterRepository registerRepository;

    @ApiOperation(value = "面板数字", notes = "面板数字")
    @RequestMapping(value = "billboard/value")
    @ResponseBody
    public EntityResponse<Map<String, String>> billboardValue() {
        Map<String, String> rtMap = new HashMap<>();
        //前三名信息
        genTop(rtMap);
        //报到信息
        rtMap.put("teamNumber", "报名" + teamNumber + "人,报到" + registerRepository.findByTokenNotNull().size() + "人");
        //比赛剩余时间
        Integer minutes = AmsDateUtils.getMinutesBetween(AmsDateUtils.getCurrentTime14(), endTime);
        rtMap.put("remainingTime", (minutes / 60) + "小时" + (minutes % 60) + "分钟");
        //已完成比例 时间进度
        rtMap.put("completionRate", String.valueOf((((24 * 60) - minutes) * 100) / (24 * 60)));
        //评委列表
        List<String> judgeList = Arrays.asList(judge.trim().split(","));
        rtMap.put("judgeList", getJudgeList(judgeList));
        return EntityResponse.build(rtMap);
    }

    @ApiOperation(value = "图表", notes = "图表")
    @RequestMapping(value = "billboard/charts")
    @ResponseBody
    public EntityResponse<Map<String, Map<String, List>>> charts() {
        Map<String, Map<String, List>> rtMap = new HashMap<>();
        rtMap.put("runnerDistribution", getRunnerDistribution());
        rtMap.put("realRankingList", getRealRankingList());
        rtMap.put("questionScoreRate", getQuestionScoreRate());
        return EntityResponse.build(rtMap);
    }

    /**
     * 题目得分率
     *
     * @return
     */
    private Map<String, List> getQuestionScoreRate() {
        List<String> questionScoreRateTitle = new ArrayList<>();
        List<String> questionScoreRateData = new ArrayList<>();
        Integer registerCount = registerRepository.findByTokenNotNull().size();
        List<GameQuestionEntity> questionEntityList = questionRepository.findAll();
        //计算得分率
        for (GameQuestionEntity gameQuestionEntity : questionEntityList) {
            List<GameScoreEntity> questionScoreList = gameScoreRepository.findByQuesNo(gameQuestionEntity.getQuesNo());
            Double sumScore = questionScoreList.stream().mapToDouble(GameScoreEntity::getScore).sum();
            Double scoreRate = (sumScore * 100) / (gameQuestionEntity.getScore() * registerCount);
            gameQuestionEntity.setScoreRate(scoreRate);
        }
        //排序
        questionEntityList.sort((o1, o2) -> o2.getScoreRate().compareTo(o1.getScoreRate()));
        for (GameQuestionEntity gameQuestionEntity : questionEntityList) {
            questionScoreRateTitle.add(gameQuestionEntity.getQuesTitle());
            String scoreRate = new DecimalFormat("#.00").format(gameQuestionEntity.getScoreRate());
            questionScoreRateData.add(scoreRate);
        }
        Map<String, List> scoreRateMap = new HashMap<>();
        scoreRateMap.put("title", questionScoreRateTitle);
        scoreRateMap.put("data", questionScoreRateData);
        return scoreRateMap;
    }

    /**
     * 参赛人员分布
     *
     * @return
     */
    private Map<String, List> getRunnerDistribution() {
        List<String> distributionTitle = new ArrayList<>();
        DepartmentNameCode[] departments = DepartmentNameCode.values();
        List<Map<String, String>> distributionData = new ArrayList<>();
        List<Map<String, String>> departmentsScore = registerRepository.selectDepartmentsScore();
        //数据库结果集转Map
        Map<String, String> departmentsScoreMap = departmentsScore.stream().collect(Collectors.toMap(s -> s.get("department"), s -> String.valueOf(s.get("total"))));
        for (DepartmentNameCode department : departments) {
            distributionTitle.add(department.getDesc());
            HashMap<String, String> distributionValue = Maps.newHashMap();
            distributionValue.put("name", department.getDesc());
            if (departmentsScoreMap.containsKey(department.getDesc())) {
                distributionValue.put("value", departmentsScoreMap.get(department.getDesc()));
            } else {
                distributionValue.put("value", "0");
            }
            distributionData.add(distributionValue);
        }
        Map<String, List> distributionMap = new HashMap<>();
        distributionMap.put("title", distributionTitle);
        distributionMap.put("data", distributionData);
        return distributionMap;
    }

    /**
     * 中间实时榜单
     *
     * @return
     */
    private Map<String, List> getRealRankingList() {
        List<Map<String, Object>> results = gameScoreRepository.selectScoreSummarise();
        List<String> nameList = new ArrayList<>();
        List<String> valueList = new ArrayList<>();
        int count = 1;
        for (Map<String, Object> result : results) {
            String runnerName = String.valueOf(result.get("runners_name"));
            if (runnerName.length() < 3) {
                runnerName = runnerName.substring(0, 1) + "　" + runnerName.substring(1);
            }
            nameList.add(runnerName);
            valueList.add(result.get("score").toString());
            if (++count > 20) {
                break;
            }
        }
        Map<String, List> rtMap = new HashMap<>();
        rtMap.put("name", nameList);
        rtMap.put("value", valueList);
        return rtMap;
    }

    private String getJudgeList(List<String> judgeList) {
        StringBuilder judgeString = new StringBuilder();
        judgeString.append("<thead><tr><th>姓名</th><th>角色</th><th>状态</th></tr></thead><tbody id='tList'>");
        for (String judge : judgeList) {
            if (judge.length() < 3) {
                judge = judge.substring(0, 1) + "&emsp;" + judge.substring(1);
            }
            judgeString.append("<tr><td>" + judge + "</td><td>评委</td><td>在岗</td></tr>");
        }
        judgeString.append("</tbody>");
        return judgeString.toString();
    }


    private void genTop(Map<String, String> rtMap) {
        List<Map<String, Object>> results = gameScoreRepository.selectScoreSummarise();
        int count = 1;
        //默认显示未产生
        rtMap.put("one", "第一名：尚未产生");
        rtMap.put("two", "第二名：尚未产生");
        rtMap.put("three", "第三名：尚未产生");
        for (Map<String, Object> result : results) {
            String runnerName = String.valueOf(result.get("runners_name"));
            if (runnerName.length() < 3) {
                runnerName = runnerName.substring(0, 1) + "&emsp;" + runnerName.substring(1);
            }
            String runnerScore = String.valueOf(result.get("score"));
            if (count == 1) {
                rtMap.put("one", "第一名：" + runnerName + "&emsp;" + runnerScore + "分");
            } else if (count == 2) {
                rtMap.put("two", "第二名：" + runnerName + "&emsp;" + runnerScore + "分");
            } else if (count == 3) {
                rtMap.put("three", "第三名：" + runnerName + "&emsp;" + runnerScore + "分");
            }
            if (++count > 3) {
                break;
            }
        }
    }

}
