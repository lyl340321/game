package com.njcb.game.web.controller

import com.njcb.ams.pojo.dto.standard.EntityResponse
import com.njcb.game.application.BusinessGameManager
import com.njcb.game.repository.entity.GameQuestionEntity
import com.njcb.game.repository.entity.GameRegisterEntity
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.web.bind.annotation.*
import java.math.BigDecimal

@RestController
@RequestMapping()
@Api(value = "submit", tags = ["submit"])
class AnswerSubmissionController(
    private val businessGameManager: BusinessGameManager
) {
 val a : Int = 0
    @ApiOperation(value = "提交", notes = "提交评分请求")
    @RequestMapping(value = ["submit/{questNo}/{token}"], method = [RequestMethod.POST])
    @ResponseBody
    fun submit(@PathVariable token: String, @PathVariable questNo: String): EntityResponse<Any?> {
        businessGameManager.submit(token, questNo)
        return EntityResponse.buildSucc()
    }
}