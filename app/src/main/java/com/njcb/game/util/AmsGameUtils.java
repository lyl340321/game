package com.njcb.game.util;

import com.njcb.ams.pojo.dto.standard.EntityResponse;
import com.njcb.ams.pojo.dto.standard.PageResponse;
import com.njcb.ams.pojo.dto.standard.Response;
import com.njcb.ams.pojo.vo.DataList;

import java.util.ArrayList;
import java.util.Collection;

public class AmsGameUtils {

    public static boolean equalsResponse(Response r1, Response r2) {
        if (!r1.getClass().equals(r2.getClass())) {
            return Boolean.FALSE;
        }
        if (!(r1.isSuccess() == r2.isSuccess() && r1.getCode().equals(r2.getCode()))) {
            return Boolean.FALSE;
        }
        if(r1 instanceof PageResponse){
            Collection r1Rows = ((PageResponse<?>) r1).getRows();
            Collection r2Rows = ((PageResponse<?>) r2).getRows();
            return r1Rows.containsAll(r2Rows) && r2Rows.containsAll(r1Rows);
        }else if(r1 instanceof EntityResponse){
            Object r1Entity = ((EntityResponse<?>) r1).getEntity();
            Object r2Entity = ((EntityResponse<?>) r2).getEntity();
            return r1Entity.equals(r2Entity);
        }else if(r1 instanceof DataList){
            ArrayList r1DataList = ((DataList<?>) r1).getDataList();
            ArrayList r2DataList = ((DataList<?>) r2).getDataList();
            return r1DataList.containsAll(r2DataList) && r2DataList.containsAll(r1DataList);
        }else {
            return r1.equals(r2);
        }
    }


}
