package com.njcb.game.util


/** 从 [0, bound) 中随机挑选两个不相同的数 */
fun java.util.Random.randomlyPickTwo(bound: Int): Pair<Int, Int> {
    val i: Int = nextInt(bound)
    val j: Int = (nextInt(bound - 1) + i + 1) % bound

    return i to j
}
