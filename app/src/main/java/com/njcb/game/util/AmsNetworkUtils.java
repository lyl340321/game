package com.njcb.game.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.*;

public class AmsNetworkUtils {

    private static final Logger logger = LoggerFactory.getLogger(AmsNetworkUtils.class);

    /**
     * 检测IP地址是否能ping通
     *
     * @param ip IP地址
     * @return 是否ping通
     */
    public static boolean ping(String ip) {
        return ping(ip, 100);
    }

    /**
     * 检测IP地址是否能ping通
     *
     * @param ip      IP地址
     * @param timeout 检测超时（毫秒）
     * @return 是否ping通
     */
    public static boolean ping(String ip, int timeout) {
        try {
            return InetAddress.getByName(ip).isReachable(timeout); // 当返回值是true时，说明host是可用的，false则不可。
        } catch (Exception ex) {
            return false;
        }
    }


    /**
     * 测试ip及端口连通性
     *
     * @param host
     * @param port
     * @return boolean
     */
    public static boolean testIpAndPort(String host, int port) {
        return testIpAndPort(host, port, 100);
    }

    /**
     * 测试ip及端口连通性
     *
     * @param host
     * @param port
     * @param timeout
     * @return boolean
     */
    public static boolean testIpAndPort(String host, int port, int timeout) {
        Socket socket = new Socket();
        boolean status = false;
        try {
            socket.connect(new InetSocketAddress(host, port), timeout);
            status = true;
        } catch (IOException e) {
            logger.warn("IP[{}]的端口[{}]无法访问", host, port);
        } finally {
            try {
                socket.close();
            } catch (IOException ex) {
                logger.error(ex.getMessage(), ex);
            }
        }
        return status;
    }

    /**
     * 检测API是否能连通
     *
     * @param urlStr 探测地址
     * @return
     */
    public static boolean testUrlIsExist(String urlStr) {
        return testUrlIsExist(urlStr, 100);
    }

    /**
     * 检测API是否能连通
     *
     * @param urlStr  探测地址
     * @param timeout 超时时间
     * @return
     */
    public static boolean testUrlIsExist(String urlStr, int timeout) {
        HttpURLConnection connection = null;
        try {
            URL url = new java.net.URL(urlStr);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("User-Agent", "Mozilla/4.0");
            connection.setRequestMethod("GET");
            // 设置单次请求是否支持重定向，默认为 setFollowRedirects 方法设置的值
            connection.setInstanceFollowRedirects(false);
            connection.setConnectTimeout(timeout);
            connection.connect();
            if (connection.getResponseCode() != 404) {
                return true;
            } else {
                return false;
            }
        } catch (IOException e) {
            return false;
        } finally {
            if (null != connection) {
                connection.disconnect();
            }
        }
    }


    //测试方法
    public static void main(String[] args) {
        String host = "127.0.0.1";
        int port = 8080;
        int timeOut = 3000;
        testIpAndPort(host, port, timeOut);
    }


}
