package com.njcb.game.util

import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Component
import java.time.Duration
import java.util.concurrent.CompletableFuture
import java.util.concurrent.Future
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException

fun <T> Future<T>.executeWithTimeout(timeout: Duration): T {
    try {
        return get(timeout.toMillis(), TimeUnit.MILLISECONDS)
    } catch (e: TimeoutException) {
        cancel(true)
        throw e
    }
}

@Component
class AsyncWrapper {
    @Async
    fun <T> callAsync(func: () -> T): CompletableFuture<T> {
        return CompletableFuture.completedFuture(func())
    }
}
