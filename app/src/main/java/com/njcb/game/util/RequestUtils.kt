package com.njcb.game.util

import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.*

object RequestUtil {
    fun checkSubmitFreq(lastSubmitTime: Date) {
        if (ChronoUnit.SECONDS.between(lastSubmitTime.toInstant(), Instant.now()) < 60) {
            fail("提交过于频繁")
        }
    }
}
