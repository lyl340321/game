package com.njcb.game.portal;

import com.njcb.ams.support.autoconfigure.annotation.AmsApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.WebApplicationInitializer;

/**
 * @author LOONG
 */
@AmsApplication(scanBasePackages = {"com.njcb"})
@EnableFeignClients(basePackages = {"com.njcb"})
@EnableAsync
public class GameApplication extends SpringBootServletInitializer implements WebApplicationInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(GameApplication.class);
    }

    public static void main(String[] args) {
    	SpringApplication.run(GameApplication.class, args);
    }

}
