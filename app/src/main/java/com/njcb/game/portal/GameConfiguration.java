package com.njcb.game.portal;

import com.njcb.ams.portal.SysBaseDefine;
import com.njcb.ams.support.annotation.AmsConfig;
import com.njcb.ams.support.config.AppConfiguration;
import org.springframework.stereotype.Component;

@AmsConfig
public class GameConfiguration implements AppConfiguration {

	@Override
	public String getAppVersion() {
		return SysBaseDefine.PLATFORM_VERSION;
	}

	@Override
	public void init() throws Exception {
		System.out.println("系统初始化");
	}

	@Override
	public void destroy() {
		System.out.println("系统销毁");
	}

}
