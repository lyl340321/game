package com.njcb.game.pojo

import com.njcb.ams.support.annotation.DataDic
import com.njcb.ams.support.codevalue.EnumCode

@DataDic(dataType = "CheckStatus", dataTypeName = "验证状态")
enum class CheckStatus(
    private val desc: String,
) : EnumCode {
    PENDING("等待验证"),
    CHECKING("验证中"),
    COMPLETE("验证完成"),
    TIMEOUT("验证超时"),
    ERROR("验证出错"),
    ;

    override fun getCode(): String = name

    override fun getDesc(): String = desc
}