package com.njcb.game.pojo;

import com.njcb.ams.support.annotation.DataDic;
import com.njcb.ams.support.codevalue.EnumCode;

@DataDic(dataType = "DepartmentNameCode", dataTypeName = "验证状态")
public enum DepartmentNameCode implements EnumCode {
    GONGSI("gongsi", "公司"),
    LINGSHOU("lingshou", "零售"),
    YUNYING("yunying", "运营"),
    CHUANGXIN("chuangxin", "创新"),
    FENGXIAN("fengxian", "风险"),
    FENHANG("fenhang", "分行"),
    OTHER("other", "其他");

    private String code;
    private String desc;

    DepartmentNameCode(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getDesc() {
        return this.desc;
    }
}
