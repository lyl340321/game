package com.njcb.game.pojo;

import java.math.BigDecimal;

/**
 * @author LOONG
 */
public class RwaType {

    /**
     * 资产类型名称
     */
    private String assetTypeName;

    /**
     * 资产类型代码
     */
    private String assetTypeNo;

    /**
     * 资产期限
     */
    private String assetLimit;

    /**
     * 风险权重
     */
    private BigDecimal riskWeighting;

    public RwaType(String assetTypeName, String assetTypeNo, String assetLimit, BigDecimal riskWeighting){
        this.assetTypeName = assetTypeName;
        this.assetTypeNo = assetTypeNo;
        this.assetLimit = assetLimit;
        this.riskWeighting = riskWeighting;
    }

    public String getAssetTypeName() {
        return assetTypeName;
    }

    public void setAssetTypeName(String assetTypeName) {
        this.assetTypeName = assetTypeName;
    }

    public String getAssetTypeNo() {
        return assetTypeNo;
    }

    public void setAssetTypeNo(String assetTypeNo) {
        this.assetTypeNo = assetTypeNo;
    }

    public String getAssetLimit() {
        return assetLimit;
    }

    public void setAssetLimit(String assetLimit) {
        this.assetLimit = assetLimit;
    }

    public BigDecimal getRiskWeighting() {
        return riskWeighting;
    }

    public void setRiskWeighting(BigDecimal riskWeighting) {
        this.riskWeighting = riskWeighting;
    }
}