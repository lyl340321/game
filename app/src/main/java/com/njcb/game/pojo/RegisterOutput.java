package com.njcb.game.pojo;

/**
 * @author LOONG
 */
public class RegisterOutput {
    private String token;

    private Integer contestId;
    public Integer getContestId() {
        return contestId;
    }

    public void setContestId(Integer contestId) {
        this.contestId = contestId;
    }


    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
