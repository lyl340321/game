package com.njcb.game.pojo

/** 单次测试样例可能的结果 */
enum class TestCaseResult {
    ACCEPT, NOT_ACCEPT, ERROR, TIMEOUT
}

data class TestCaseResultDetail(
    val result: TestCaseResult,
    val message: String?,
) {
    companion object {
        fun accept(message: String? = null) = TestCaseResultDetail(
            result = TestCaseResult.ACCEPT,
            message = message,
        )

        fun error(message: String? = null) = TestCaseResultDetail(
            result = TestCaseResult.ERROR,
            message = message,
        )

        fun notAccept(message: String? = null) = TestCaseResultDetail(
            result = TestCaseResult.NOT_ACCEPT,
            message = message,
        )

        fun timeout(message: String? = null) = TestCaseResultDetail(
            result = TestCaseResult.TIMEOUT,
            message = message,
        )
    }
}
