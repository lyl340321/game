package com.njcb.game.pojo;

import com.njcb.ams.support.annotation.DataDic;
import com.njcb.ams.support.codevalue.EnumCode;

/**
 * @author LOONG
 */

@DataDic(dataType = "CheckType", dataTypeName = "验证类型")
public enum CheckType implements EnumCode {

    CLASS("CLASS", "调用指定class判分"),
    SQL("SQL", "执行指定SQL判分");

    private String code;
    private String desc;

    CheckType(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getDesc() {
        return this.desc;
    }
}
