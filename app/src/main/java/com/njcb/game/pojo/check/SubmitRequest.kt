package com.njcb.game.pojo.check

import com.fasterxml.jackson.annotation.JsonFormat
import java.math.BigDecimal
import java.time.LocalDateTime

data class SubmitRequest(val businessNo: String, val operator: String, val businessAmount: BigDecimal)
data class ApproveRequest(val businessNo: String, val operator: String, val approveResult: ApproveResult)
data class QueryApproveRecordRequest(val businessNo: String = "")
data class CommonResponse(val success: Boolean = false, val code: String = "")

data class PagedResponse(
    val success: Boolean = false,
    val code: String = "",
    val rows: List<QueryRecord>? = null,
    val total: Int = 0
)

enum class ApproveResult { agree, disagree }
data class QueryRecord(
    val nodeName: String = "",
    val operator: String = "",
    val approveResult: ApproveResult? = null,
    //val startTime: String = "",
    //val endTime: String = ""
) {
    override fun equals(other: Any?): Boolean {
        return super.equals(other)
    }

    override fun hashCode(): Int {
        return javaClass.hashCode()
    }
}