package com.njcb.game.pojo

import com.njcb.ams.support.annotation.DataDic
import com.njcb.ams.support.codevalue.EnumCode

@DataDic(dataType = "FileUploadStatus", dataTypeName = "文件上传状态")
enum class FileUploadStatus(
    private val desc: String,
) : EnumCode {
    PENDING("等待上传"),
    UPLOADING("上传中"),
    UPLOADED("上传完成"),
    CANCELLED("上传取消"),
    ;

    override fun getCode(): String = name

    override fun getDesc(): String = desc
}