package com.njcb.game.pojo;

/**
 * @author LOONG
 */
public class GameResultDetail {
    /**
     * 参赛人工号
     */
    private String runnersNo;
    /**
     * 参赛人姓名
     */
    private String runnersName;
    /**
     * 题目编号
     */
    private String quesNo;
    /**
     * 题目标题
     */
    private String quesTitle;
    /**
     * 验证结果
     */
    private String checkResult;
    /**
     * 失败原因
     */
    private String failReason;
    /**
     * 得分
     */
    private Integer score;

    public String getRunnersNo() {
        return runnersNo;
    }

    public void setRunnersNo(String runnersNo) {
        this.runnersNo = runnersNo;
    }

    public String getRunnersName() {
        return runnersName;
    }

    public void setRunnersName(String runnersName) {
        this.runnersName = runnersName;
    }

    public String getQuesNo() {
        return quesNo;
    }

    public void setQuesNo(String quesNo) {
        this.quesNo = quesNo;
    }

    public String getQuesTitle() {
        return quesTitle;
    }

    public void setQuesTitle(String quesTitle) {
        this.quesTitle = quesTitle;
    }


    public String getCheckResult() {
        return checkResult;
    }

    public void setCheckResult(String checkResult) {
        this.checkResult = checkResult;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public String getFailReason() {
        return failReason;
    }

    public void setFailReason(String failReason) {
        this.failReason = failReason;
    }
}