package com.njcb.game.pojo


data class SimpleTestCase(
    val input: Map<String, Any>,
    val expect: Map<String, Any>,
    val score: Int = 0,
)

data class SimpleTestResultDetail(
    val testCase: SimpleTestCase,
    val answer: Map<String, Any>?,
    val result: TestCaseResult,
    val message: String?,
    val value: Float
)