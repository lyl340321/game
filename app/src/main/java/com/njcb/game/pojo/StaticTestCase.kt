package com.njcb.game.pojo


data class StaticTestSet(
    val dir: String,
    val score: Int,
    val count: Int,
)

data class StaticTestMetadata(
    val testSets: List<StaticTestSet>
)
