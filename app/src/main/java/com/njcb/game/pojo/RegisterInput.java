package com.njcb.game.pojo;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * @author LOONG
 */
public class RegisterInput {
    @Pattern(regexp = "^\\d+$", message = "非法的工号，工号必须是数字")
    @NotBlank(message = "注册信息不能为空")
    @NotNull
    private String runnersNo;

    @NotBlank(message = "注册信息不能为空")
    @NotNull
    private String runnersName;

    @NotBlank(message = "注册信息不能为空")
    @NotNull
    private String serverAddr;

    @NotBlank(message = "注册部门不能为空")
    private String department;

    @Nullable
    private String token;

    @NotNull
    public String getRunnersNo() {
        return runnersNo;
    }

    public void setRunnersNo(@NotNull String runnersNo) {
        this.runnersNo = runnersNo;
    }

    @NotNull
    public String getRunnersName() {
        return runnersName;
    }

    public void setRunnersName(@NotNull String runnersName) {
        this.runnersName = runnersName;
    }

    @NotNull
    public String getDepartment() {
        return department;
    }

    public void setDepartment(@NotNull String department) {
        this.department = department;
    }

    @NotNull
    public String getServerAddr() {
        return serverAddr;
    }

    public void setServerAddr(@NotNull String serverAddr) {
        this.serverAddr = serverAddr;
    }

    @Nullable
    public String getToken() {
        return token;
    }

    public void setToken(@Nullable String token) {
        this.token = token;
    }
}
