package com.njcb.game.pojo.check

data class OpticalCharacterRecognition(
    val title: String,
    val date: String,
    val serial:String,
    val payerName: String,
    val payerAccount:String,
    val payerBank: String,
    val payeeName:String,
    val payeeAccount:String,
    val payeeBank: String,
    val amount:String,
    val capital:String,
) {
    override fun toString(): String {
        return "$title$date$serial$payerName$payerAccount$payerBank$payeeName$payeeAccount$payeeBank$amount$capital"
    }
}