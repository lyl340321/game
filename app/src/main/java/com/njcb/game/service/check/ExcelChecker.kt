package com.njcb.game.service.check

import com.njcb.ams.util.AmsDateUtils
import com.njcb.game.pojo.CheckStatus
import com.njcb.game.repository.dao.GameAttemptRepository
import com.njcb.game.repository.entity.GameAttemptEntity
import com.njcb.game.repository.entity.GameQuestionEntity
import com.njcb.game.repository.entity.GameRegisterEntity
import com.njcb.game.util.ExceptionUtil
import com.njcb.game.util.assertEqual
import org.apache.poi.ss.usermodel.CellType
import org.apache.poi.ss.usermodel.DateUtil
import org.apache.poi.xssf.usermodel.XSSFCell
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import org.springframework.beans.factory.annotation.Value
import java.io.File
import java.io.FileInputStream
import java.math.BigDecimal
import java.nio.file.Paths
import java.util.*
import kotlin.io.path.Path
import kotlin.io.path.div
import kotlin.io.path.name
import kotlin.io.path.pathString

class ExcelChecker(
    private val gameAttemptRepository: GameAttemptRepository,
    @Value("\${game.working-dir}") baseWorkingDir: String,
) : QuestionChecker {
    private val workingDirBase = Paths.get(baseWorkingDir, "cases", "005", "target")
    override fun check(
        parti: GameRegisterEntity,
        question: GameQuestionEntity,
        attempt: GameAttemptEntity
    ): CheckResult {//硬编码: 本次question的专题是005
        val gameAttempt = gameAttemptRepository.findByRunnersNoAndQuesNo(parti.runnersNo, "005")
            ?: ExceptionUtil.throwAppException("${parti.runnersNo}/005 无法找到尝试记录")
        //如何控制重入?
        assertEqual(CheckStatus.CHECKING.code, gameAttempt.checkStatus) {
            "提交 ${attempt.id} 状态有误：需要 ${CheckStatus.CHECKING}，实际 ${gameAttempt.checkStatus}"
        }
        //Read Template Excel
        val wb = XSSFWorkbook(FileInputStream(File(attempt.checkFile)))
        val templateBonds = readTemplateData(wb)
        val groups = templateBonds.groupBy { q -> BondGroup4(q.book, q.trader, q.securityId, q.securitySubType) }
        //DATA
        val templateData: List<BondData> = templateBonds.map { q ->
            BondData(
                q.trade.toString(), q.desk, q.book, q.trader, q.securitySubType,
                q.securityId, q.notional.toString(), if (q.purchaseOrSell == "P") {
                    "-${q.bondTradePrice}"
                } else {
                    "${q.bondTradePrice}"
                }, q.purchaseOrSell,
                (q.bondTradePrice * q.notional).toString(), q.customer, q.currency, q.status
            )
        }
        //SUM
        val templateSum: List<BondSum> = listOf()
        for (group in groups) {
            val purchase =
                group.value.filter { q -> q.purchaseOrSell == "P" }.sumOf { q -> -q.notional * q.bondTradePrice }
            val sell = group.value.filter { q -> q.purchaseOrSell == "S" }.sumOf { q -> q.notional * q.bondTradePrice }
            val bonus = sell - purchase
            val notional = group.value.sumOf { q -> q.notional } //Laughing
            templateSum.plus(
                BondSum(
                    group.value.map { q -> q.trade }.first().toString(),
                    group.key.book,
                    group.key.trader,
                    group.key.securityId,
                    group.key.securitySubType,
                    notional.toString(),
                    bonus.toString()
                )
            )
        }
        //监测数据汇总
        val templateTotalSum = templateSum.map { q ->
            BondTotalSum(
                q.trade,
                q.book,
                q.trader,
                q.securitySubType,
                q.securityId,
                q.notional
            )
        }

        val path = workingDirBase / Path(attempt.checkFile).name
        val user = XSSFWorkbook(FileInputStream(File(path.pathString)))
        val userBonds = readTemplateData(user)

        val userData = readUserData(user)
        val userSum = readUserSum(user)
        val userTotalSum = readUserTotalSum(user)

        var acceptCount = 0
        var notAcceptCount = 0
        var errCount = 0
        try {
            checkAnswerEqual(templateBonds, userBonds)
            acceptCount++
            checkAnswerEqual(templateData, userData)
            acceptCount++
            checkAnswerEqual(templateSum, userSum)
            acceptCount++
            checkAnswerEqual(templateTotalSum, userTotalSum)
            acceptCount++
        } catch (t: AnswerNotAcceptException) {
            notAcceptCount++
        } catch (ex: Exception) {
            errCount++
        }


        return CheckResult(question.score * acceptCount / 4, 1, acceptCount, notAcceptCount, errCount, false)
    }

    private fun readTemplateData(workbook: XSSFWorkbook): List<BondSecurity> {
        val templateSheet = workbook.getSheetAt(0)
        val templateBonds: List<BondSecurity> = listOf()
        for (i in 1..templateSheet.lastRowNum) {
            val row = templateSheet.getRow(i)
            val book = readCell2String(row.getCell(6))
            val customer = readCell2String(row.getCell(12))
            val desk = readCell2String(row.getCell(14))
            val notionalCell = row.getCell(18)
            var notion = BigDecimal(0)
            if (notionalCell.cellTypeEnum.equals(CellType.STRING)) notion =
                BigDecimal(notionalCell.stringCellValue) else
                if (notionalCell.cellTypeEnum.equals(CellType.NUMERIC)) notion =
                    BigDecimal(notionalCell.numericCellValue)
            val securityId = readCell2String(row.getCell(20))
            val securitySubType = readCell2String(row.getCell(21))
            val tradeCell = row.getCell((24))
            var trade: Date = AmsDateUtils.getCurrentDate()
            if (tradeCell.cellTypeEnum == CellType.NUMERIC && DateUtil.isCellDateFormatted((tradeCell)))
                trade = tradeCell.dateCellValue
            val trader = readCell2String(row.getCell(26))
            val bondTradePriceCell = row.getCell(27)
            var price = BigDecimal(0)
            if (bondTradePriceCell.cellTypeEnum.equals(CellType.STRING)) price =
                BigDecimal(bondTradePriceCell.stringCellValue) else
                if (bondTradePriceCell.cellTypeEnum.equals(CellType.NUMERIC)) price =
                    BigDecimal(bondTradePriceCell.numericCellValue)
            val purchaseOrSell = readCell2String(row.getCell(28))
            val currency = readCell2String(row.getCell(8))
            val status = readCell2String(row.getCell(5))
            templateBonds.plus(
                BondSecurity(
                    book,
                    customer,
                    desk,
                    notion,
                    securityId,
                    securitySubType,
                    trade,
                    trader,
                    price,
                    purchaseOrSell,
                    currency,
                    status
                )
            )
        }
        return templateBonds
    }

    private fun readUserData(workbook: XSSFWorkbook): List<BondData> {
        val sheet = workbook.getSheet("DATA")
        val bondData: List<BondData> = listOf()
        for (i in 1..sheet.lastRowNum) {
            val row = sheet.getRow(i)
            bondData.plus(
                BondData(
                    readCell2String(row.getCell(0)),
                    readCell2String(row.getCell(1)),
                    readCell2String(row.getCell(2)),
                    readCell2String(row.getCell(3)),
                    readCell2String(row.getCell(4)),
                    readCell2String(row.getCell(5)),
                    readCell2String(row.getCell(6)),
                    readCell2String(row.getCell(7)),
                    readCell2String(row.getCell(8)),
                    readCell2String(row.getCell(9)),
                    readCell2String(row.getCell(10)),
                    readCell2String(row.getCell(11)),
                    readCell2String(row.getCell(12)),
                )
            )
        }
        return bondData
    }

    private fun readUserSum(workbook: XSSFWorkbook): List<BondSum> {
        val sheet = workbook.getSheet("SUM")
        val bondSum: List<BondSum> = listOf()
        for (i in 1..sheet.lastRowNum) {
            val row = sheet.getRow(i)
            bondSum.plus(
                BondSum(
                    readCell2String(row.getCell(0)),
                    readCell2String(row.getCell(1)),
                    readCell2String(row.getCell(2)),
                    readCell2String(row.getCell(3)),
                    readCell2String(row.getCell(4)),
                    readCell2String(row.getCell(5)),
                    readCell2String(row.getCell(6)),
                )
            )
        }
        return bondSum
    }

    private fun readUserTotalSum(workbook: XSSFWorkbook): List<BondTotalSum> {
        val sheet = workbook.getSheet("监测数据汇总")
        val bondTotalSum: List<BondTotalSum> = listOf()
        for (i in 1..sheet.lastRowNum) {
            val row = sheet.getRow(i)
            bondTotalSum.plus(
                BondTotalSum(
                    readCell2String(row.getCell(0)),
                    readCell2String(row.getCell(1)),
                    readCell2String(row.getCell(2)),
                    readCell2String(row.getCell(3)),
                    readCell2String(row.getCell(4)),
                    readCell2String(row.getCell(5)),
                )
            )
        }
        return bondTotalSum
    }

    private fun readCell2String(cell: XSSFCell): String {
        return when (cell.cellTypeEnum) {
            CellType.STRING -> cell.stringCellValue
            CellType.NUMERIC -> if (DateUtil.isCellDateFormatted(cell)) {
                cell.dateCellValue.toString()
            } else {
                cell.numericCellValue.toString()
            }

            else -> ""
        }
    }
}

data class BondSecurity(
    val book: String?,
    val customer: String?,
    val desk: String?,
    val notional: BigDecimal,
    val securityId: String?,
    val securitySubType: String?,
    val trade: Date,
    val trader: String?,
    val bondTradePrice: BigDecimal,
    val purchaseOrSell: String?,
    val currency: String?,
    val status: String?
)

data class BondData(
    val trade: String?,
    val desk: String?,
    val book: String?,
    val trader: String?,
    val securitySubType: String?,
    val securityId: String?,
    val notional: String?,
    val price: String?,
    val purchaseOrSell: String?,
    val amount: String?,
    val customer: String?,
    val currency: String?,
    val status: String?
)

data class BondGroup4(
    val book: String?,
    val trader: String?,
    val securityId: String?,
    val securitySubType: String?
)

data class BondSum(
    val trade: String?,
    val book: String?,
    val trader: String?,
    val securityId: String?,
    val securitySubType: String?,
    val notional: String?,
    val bonus: String?
)

data class BondTotalSum(
    val trade: String?,
    val book: String?,
    val trader: String?,
    val securitySubType: String?,
    val securityId: String?,
    val notional: String?
)