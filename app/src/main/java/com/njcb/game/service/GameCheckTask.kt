package com.njcb.game.service

import com.njcb.ams.pojo.enumvalue.JobResult
import com.njcb.ams.scheduler.bean.BaseJobDetail
import org.quartz.JobExecutionContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

/**
 * @author LOONG
 */
@Component
class GameCheckTask : BaseJobDetail() {

    @Autowired
    private lateinit var checkService: QuestionsCheckService

    override fun executeJob(context: JobExecutionContext): JobResult {
        checkService.check()
        return JobResult.RESULT_IGNORE
    }

}