package com.njcb.game.service.check

import com.njcb.ams.factory.domain.AppContext
import com.njcb.game.util.AsyncWrapper
import com.njcb.game.util.executeWithTimeout
import org.springframework.beans.factory.config.ConfigurableBeanFactory
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component
import java.time.Duration
import java.util.concurrent.TimeoutException


@Component
class CheckTimeoutManager(
    private val asyncWrapper: AsyncWrapper,
) {

    fun newSession(timeout: Duration): CheckTimeoutSession {
        return AppContext.getContext().getBean(CheckTimeoutSession::class.java, timeout.toMillis(), asyncWrapper)
    }

}

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
class CheckTimeoutSession(
    private var remainingTime: Long,
    private val asyncWrapper: AsyncWrapper,
) {
    fun <T> runWithTimeout(func: () -> T): T {
        if (remainingTime <= 0) {
            throw TimeoutException()
        }

        val startTime = System.currentTimeMillis()
        val result = try {
            asyncWrapper.callAsync(func).executeWithTimeout(Duration.ofMillis(remainingTime))
        } finally {
            val elapsed = System.currentTimeMillis() - startTime
            remainingTime -= elapsed
        }

        return result
    }
}
