package com.njcb.game.service;

import com.njcb.ams.support.comm.SequenceService;
import com.njcb.ams.support.exception.ExceptionUtil;
import com.njcb.ams.util.*;
import com.njcb.game.pojo.DepartmentNameCode;
import com.njcb.game.pojo.RegisterInput;
import com.njcb.game.pojo.RwaType;
import com.njcb.game.repository.dao.GameRegisterRepository;
import com.njcb.game.repository.dao.RwaInfoRepository;
import com.njcb.game.repository.entity.GameRegisterEntity;
import com.njcb.game.repository.entity.RwaInfoEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.sound.midi.Sequence;
import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @author LOONG
 */
@Service
public class RwaService {

    private static List<RwaType> rwaTypeList = new ArrayList<>();

    static {
        rwaTypeList.add(new RwaType("国债", "10", "不限", new BigDecimal(0)));
        rwaTypeList.add(new RwaType("对公贷款", "20", "不限", new BigDecimal(1)));
        rwaTypeList.add(new RwaType("个人贷款", "30", "不限", new BigDecimal(0.5)));
        rwaTypeList.add(new RwaType("股权", "40", "不限", new BigDecimal(2.5)));
        rwaTypeList.add(new RwaType("信用证", "50", "一年以上", new BigDecimal(1)));
        rwaTypeList.add(new RwaType("信用证", "50", "一年以内(含)", new BigDecimal(0.2)));
        rwaTypeList.add(new RwaType("融资性保函", "60", "不限", new BigDecimal(1)));
        rwaTypeList.add(new RwaType("非融资性保函", "61", "不限", new BigDecimal(0.5)));
        rwaTypeList.add(new RwaType("银行承兑汇票", "70", "不限", new BigDecimal(1)));
        rwaTypeList.add(new RwaType("不可撤销贷款承诺", "80", "一年以内(含)", new BigDecimal(0.2)));
        rwaTypeList.add(new RwaType("不可撤销贷款承诺", "80", "一年以上", new BigDecimal(0.5)));
        rwaTypeList.add(new RwaType("其他资产", "不限", "不限", new BigDecimal(1)));
    }

    @Autowired
    private RwaInfoRepository rwaInfoRepository;

    public List<RwaInfoEntity> getAssetList(String serialNo) {
        String custNo = SequenceService.genSeq("rwaCustNo");
        List<RwaInfoEntity> entityList = new ArrayList<>();
        int count = new Random(10).nextInt();
        for (int i = 0; i < count; i++) {
            RwaType rwaType = rwaTypeList.stream()
                    .skip(new Random().nextInt(rwaTypeList.size()))
                    .findFirst()
                    .orElse(null);
            RwaInfoEntity entity = new RwaInfoEntity();
            entity.setSerialNo(serialNo);
            entity.setCustNo(custNo);
            entity.setVoucherNo(SequenceService.genSeq("VoucherNo"));
            entity.setAssetTypeName(rwaType.getAssetTypeName());
            entity.setAssetTypeNo(rwaType.getAssetTypeNo());
            entity.setAssetLimit("");
            entity.setRiskBalance(new BigDecimal(100));
            entityList.add(entity);
            BigDecimal rwaBal = computeRwa(entityList);
            if (rwaBal.compareTo(new BigDecimal(50000000)) > 0) {
                break;
            }
        }
        rwaInfoRepository.saveAll(entityList);
        return entityList;
    }


    /**
     * 计算风险加权资产
     *
     * @param entityList
     * @return
     */
    public BigDecimal computeRwa(List<RwaInfoEntity> entityList) {
        return new BigDecimal(0);
    }
}
