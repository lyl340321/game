package com.njcb.game.service;

import com.njcb.ams.support.exception.ExceptionUtil;
import com.njcb.ams.util.*;
import com.njcb.game.pojo.DepartmentNameCode;
import com.njcb.game.pojo.RegisterInput;
import com.njcb.game.repository.dao.GameRegisterRepository;
import com.njcb.game.repository.entity.GameRegisterEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.UUID;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @author LOONG
 */
@Service
public class RegisterService {
    @Autowired
    private GameRegisterRepository gameRegisterRepository;

    public String register(@Valid RegisterInput input) {
        GameRegisterEntity gameRegister = gameRegisterRepository.findByRunnersNo(input.getRunnersNo());
        if (null == gameRegister) {
            gameRegister = new GameRegisterEntity();
            AmsBeanUtils.copyProperties(gameRegister, input);
            String token = AmsCryptUtils.encryptForMD5(input.getRunnersNo() + UUID.randomUUID());
            gameRegister.setToken(token);
        } else {
            // 注册接口被再次调用，验证token避免其他人恶意修改
            AmsAssert.notNull(input.getToken(), "参赛编号[runnersNo]已存在时token不能为空");
            AmsAssert.isEqual(gameRegister.getToken(), input.getToken(), "token不匹配");
        }

        DepartmentNameCode department = AmsEnumUtils.valueOfDesc(DepartmentNameCode.class, input.getDepartment());
        if (AmsUtils.isNull(gameRegister.getDepartment()) || null == department) {
            ExceptionUtil.throwAppException("注册部门值无效,有效的枚举值为:" + Arrays.stream(DepartmentNameCode.values()).map(dep -> dep.getDesc()).collect(Collectors.toList()));
        }

        Pattern pattern = Pattern.compile("^(https?)://[-A-Za-z0-9+&@#/%?=_|!:,.;]+[-A-Za-z0-9+&@#/%=_|]");
        if (!pattern.matcher(input.getServerAddr()).matches()) {
            ExceptionUtil.throwAppException("服务地址[" + input.getServerAddr() + "]不正确");
        }
        if (input.getServerAddr().contains("localhost")) {
            ExceptionUtil.throwAppException("服务地址[" + input.getServerAddr() + "]不能包含localhost地址");
        }
        gameRegister.setDepartment(department.getDesc());
        gameRegister.setRunnersNo(input.getRunnersNo());
        gameRegister.setRunnersName(input.getRunnersName());
        gameRegister.setServerAddr(input.getServerAddr());
        gameRegisterRepository.save(gameRegister);
        Integer id = gameRegisterRepository.findByRunnersNo(input.getRunnersNo()).getId();
        return gameRegister.getToken();
    }
}
