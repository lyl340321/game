package com.njcb.game.service.check

import com.njcb.game.pojo.TestCaseResult
import com.njcb.game.repository.entity.GameAttemptEntity
import com.njcb.game.repository.entity.GameQuestionEntity
import com.njcb.game.repository.entity.GameRegisterEntity


class CheckSession(private val questionScore: Int, private val totalCaseCount: Int) {
    var caseNo: Int = 0
        private set
    private var acCount: Int = 0
    private var naCount: Int = 0
    private var errorCount: Int = 0
    private var totalScore: Int = 0
    private var timeout: Boolean = false

    fun parseCaseResult(score: Int, result: TestCaseResult): Boolean {
        when (result) {
            TestCaseResult.ACCEPT -> {
                acCount++
                totalScore += score
            }

            TestCaseResult.NOT_ACCEPT -> naCount++
            TestCaseResult.ERROR -> errorCount++
            TestCaseResult.TIMEOUT -> {
                errorCount++
                timeout = true
            }
        }

        caseNo++

        return timeout
    }

    fun getResult(): CheckResult {
        return CheckResult(
            finalScore = questionScore * totalScore / 100,
            totalCaseCount = totalCaseCount,
            acceptCount = acCount,
            notAcceptCount = naCount,
            errorCount = errorCount,
            timeout = timeout,
        )
    }
}

data class CheckResult(
    val finalScore: Int,
    val totalCaseCount: Int,
    val acceptCount: Int,
    val notAcceptCount: Int,
    val errorCount: Int,
    val timeout: Boolean,
) {
    fun toResultMessage(): String {
        var result = "测试样例通过 $acceptCount/$totalCaseCount"
        if (notAcceptCount != 0) {
            result += "，结果错误 $notAcceptCount"
        }
        if (errorCount != 0) {
            result += "，评分失败 $errorCount"
        }
        if (timeout) {
            result += "，评分超时"
        }

        return result
    }
}

interface QuestionChecker {
    fun check(parti: GameRegisterEntity, question: GameQuestionEntity, attempt: GameAttemptEntity): CheckResult
}