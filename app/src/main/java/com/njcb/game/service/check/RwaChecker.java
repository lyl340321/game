package com.njcb.game.service.check;

import com.google.common.collect.Maps;
import com.njcb.ams.pojo.dto.standard.EntityResponse;
import com.njcb.ams.support.comm.SequenceService;
import com.njcb.ams.support.exception.ExceptionUtil;
import com.njcb.ams.util.AmsAssert;
import com.njcb.ams.util.AmsJsonUtils;
import com.njcb.game.repository.dao.RwaInfoRepository;
import com.njcb.game.repository.entity.GameAttemptEntity;
import com.njcb.game.repository.entity.GameQuestionEntity;
import com.njcb.game.repository.entity.GameRegisterEntity;
import com.njcb.game.repository.entity.RwaInfoEntity;
import com.njcb.game.service.RwaService;
import com.njcb.game.util.AmsNetworkUtils;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * 风险加权资产计算
 */
@Component("rwaChecker")
public class RwaChecker implements QuestionChecker {
    private static final Logger logger = LoggerFactory.getLogger(QuestionChecker.class);

    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private RwaInfoRepository rwaInfoRepository;
    @Autowired
    private RwaService rwaService;

    @NotNull
    @Override
    public CheckResult check(@NotNull GameRegisterEntity parti, @NotNull GameQuestionEntity question, @NotNull GameAttemptEntity attempt) {
        logger.info("开始验证用户[{}]的题目[{}]",parti.getRunnersNo(),question.getQuesNo());
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        String serialNo = SequenceService.genSeq("serialNo");
        Map param = Maps.newHashMap();
        param.put("serialNo",serialNo);

        HttpEntity httpEntity = new HttpEntity(param, headers);
        String url = parti.getServerAddr() + "/rwa/compute";
        if(!AmsNetworkUtils.testUrlIsExist(url)){
            ExceptionUtil.throwAppException("验证地址["+url+"]不存在");
        }
        ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST, httpEntity, String.class);
        logger.debug("\ntoken刷新令牌:" + responseEntity.getBody());
        if (HttpStatus.OK == responseEntity.getStatusCode()) {
            String body = responseEntity.getBody();
            EntityResponse entityResponse = AmsJsonUtils.jsonToObject(body, EntityResponse.class);
            BigDecimal rtBal = new BigDecimal(entityResponse.getEntity().toString());
            List<RwaInfoEntity> entityList = rwaInfoRepository.findBySerialNo(serialNo);
            BigDecimal rwaBal = rwaService.computeRwa(entityList);
            if(rtBal == rwaBal){
                CheckResult checkResult = new CheckResult(10,1,1,0,0,Boolean.FALSE);
                return checkResult;
            }
        } else {
            AmsAssert.fail(responseEntity.getBody());
        }
        CheckResult checkResult = new CheckResult(10,1,1,0,0,Boolean.FALSE);
        return checkResult;
    }
}
