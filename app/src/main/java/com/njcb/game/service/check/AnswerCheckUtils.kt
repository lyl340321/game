package com.njcb.game.service.check

import com.njcb.ams.util.AmsBeanUtils
import org.springframework.beans.BeanUtils

private fun throwAnswerNotAcceptException(propName: String?): Nothing {
    if (propName == null) {
        throw AnswerNotAcceptException("答案不符")
    } else {
        throw AnswerNotAcceptException("$propName 不符")
    }
}

private val BUILT_IN_PROPS = setOf("class")

/** 过滤 props */
fun Map<String, Any>.filterProps(
    includeProps: Set<String>?,
    excludeProps: Set<String>?,
): Map<String, Any> {
    return if (includeProps != null) {
        this.filterKeys { it in includeProps && it !in BUILT_IN_PROPS }
    } else if (excludeProps != null) {
        this.filterKeys { it !in excludeProps }
    } else {
        this.filterKeys { it !in BUILT_IN_PROPS }
    }
}

/**
 * 判断两个对象是否相等。对于复合对象，递归的判断子属性是否相同，而不在意具体的类型。
 * 对于简单对象则调用.equals()判断是否相等。
 * 近似可以理解为将两个对象视为JSON之类的形式处理，而忽略复合对象的类型。
 */
fun propertiesEqual(
    expected: Any?,
    actual: Any?,
    includeProps: Set<String>? = null,
    excludeProps: Set<String>? = null,
): Boolean {
    if (expected == null || actual == null || BeanUtils.isSimpleValueType(expected.javaClass)) {
        return expected == actual
    }

    val expectedProps = AmsBeanUtils.describe(expected).filterProps(includeProps, excludeProps)
    val actualProps = AmsBeanUtils.describe(actual).filterProps(includeProps, excludeProps)
    if (expectedProps.keys != actualProps.keys) {
        return false
    }

    // 处理属性
    return expectedProps.keys.all { prop ->
        propertiesEqual(expectedProps[prop], actualProps[prop])
    }
}

fun checkAnswerEqual(
    expected: Any?,
    actual: Any?,
    propName: String? = null,
    includeProps: Set<String>? = null,
    excludeProps: Set<String>? = null,
) {
    if (expected == null) {
        if (actual == null) {
            return
        }
        throwAnswerNotAcceptException(propName)
    }

    if (BeanUtils.isSimpleValueType(expected.javaClass)) {
        if (expected != actual) {
            throwAnswerNotAcceptException(propName)
        }
    } else if (actual == null) {
        throwAnswerNotAcceptException(propName)
    } else {
        val expectedProps = AmsBeanUtils.describe(expected).filterProps(includeProps, excludeProps)
        val actualProps = AmsBeanUtils.describe(actual).filterProps(includeProps, excludeProps)

        val propNamePrefix = if (propName == null) "" else "$propName."
        val commonProps = expectedProps.keys intersect actualProps.keys
        // 先处理共同属性
        commonProps.forEach { prop ->
            checkAnswerEqual(expectedProps[prop], actualProps[prop], "$propNamePrefix$prop")
        }
        // 处理缺失属性
        (expectedProps.keys - commonProps).let { missingProps ->
            if (missingProps.isNotEmpty()) {
                throw AnswerNotAcceptException("缺失以下属性: $propNamePrefix[${missingProps.joinToString(",")}]")
            }
        }
        // 处理多余属性
        (actualProps.keys - commonProps).let { extraProps ->
            if (extraProps.isNotEmpty()) {
                throw AnswerNotAcceptException("多出以下属性: $propNamePrefix[${extraProps.joinToString(",")}]")
            }
        }
    }
}

/**
 * 验证两个给定的无序答案序列是否相同。
 */
fun <E, A> checkAnswerUnordered(expected: List<E>, actual: List<A>, comparator: (E, A) -> Boolean) {
    val exp = expected.toMutableList()
    val act = actual.toMutableList()

    val expI = exp.listIterator()
    while (expI.hasNext()) {
        val e = expI.next()
        val actI = act.listIterator()
        act@ while (actI.hasNext()) {
            val a = actI.next()

            if (comparator(e, a)) {
                expI.remove()
                actI.remove()
                break@act
            }
        }
    }

    if (exp.isNotEmpty() || act.isNotEmpty()) {
        throwAnswerNotAcceptException(null)
    }
}
