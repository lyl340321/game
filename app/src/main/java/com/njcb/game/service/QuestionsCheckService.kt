package com.njcb.game.service

import com.njcb.ams.factory.comm.DataBus
import com.njcb.ams.util.AmsJsonUtils
import com.njcb.ams.util.AmsUtils
import com.njcb.game.pojo.CheckStatus
import com.njcb.game.pojo.CheckType
import com.njcb.game.repository.dao.*
import com.njcb.game.repository.entity.*
import com.njcb.game.service.check.QuestionChecker
import com.njcb.game.util.*
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.ApplicationContext
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Service
import org.springframework.transaction.PlatformTransactionManager
import org.springframework.transaction.annotation.Transactional
import java.nio.file.Path
import java.nio.file.Paths
import java.util.*
import kotlin.io.path.createDirectories
import kotlin.io.path.div

/**
 * @author LOONG
 */
@Service
class QuestionsCheckService(
    @Value("\${game.working-dir}")
    baseWorkingDir: String,
    private val context: ApplicationContext,
    private val registerDAO: GameRegisterRepository,
    private val questionDAO: GameQuestionRepository,
    private val checkDAO: GameCheckRepository,
    private val scoreDAO: GameScoreRepository,
    private val attemptDAO: GameAttemptRepository,
    private val transactionManager: PlatformTransactionManager,
) {
    private val workingDirBase = Paths.get(baseWorkingDir, "attempt")

    fun check() {
        // 首先进行SQL形式的检测（即不需要用户主动提交结果的题目的自动判分）
        doSQLBasedCheck()

        // 然后验证用户的主动提交
        attemptDAO.findAll().forEach { attempt ->
            // 先将 attempt 更新为 CHECKING 状态
            transactionManager.executeInNewTransaction {
                attemptDAO.findById(attempt.id).get().let {
                    if (it.checkStatus != CheckStatus.PENDING.code) {
                        logger.error("Unexpected check status {} for attempt {}", it.checkStatus, attempt.id)
                        null
                    } else {
                        it.checkStatus = CheckStatus.CHECKING.code
                        attemptDAO.save(it)
                    }
                }
            }?.let {
                // 成功的话就继续
                checkAttempt(it)
            }
        }
    }

    /** 如果用户允许提交指定的题目，则执行 [action] */
    fun <T> guardUserSubmission(
        token: String,
        questNo: String,
        userSubmit: Boolean,
        action: (GameRegisterEntity, GameQuestionEntity) -> T
    ): T {
        // 获取并锁定用户
        val parti = assertNotNull(registerDAO.findByToken(token)) { "未知的 token $token" }

        // 限制提交频率
        val attempt = attemptDAO.findByRunnersNo(parti.runnersNo, Sort.by(Sort.Direction.DESC,"submitTime") )?.firstOrNull()?.let { latestAttempt ->
            RequestUtil.checkSubmitFreq(latestAttempt.submitTime)
        }

        // 获取题目
        val question = getQuestionForAttempt(questNo, userSubmit)

        // 禁止在判分完成前重复提交同一道题
        attemptDAO.selectLatestAttempt(parti.runnersNo, question.quesNo)?.let { latestAttempt ->
            if (latestAttempt.checkStatus == CheckStatus.PENDING.code
                || latestAttempt.checkStatus == CheckStatus.CHECKING.code) {
                fail("禁止在判分完成前重复提交同一题")
            }
        }

        return action(parti, question)
    }

    /** 无条件创建 attempt */
    fun forceAttempt(runnersNo: String, questNo: String): GameAttemptEntity {
        return GameAttemptEntity().apply {
            this.runnersNo = runnersNo
            quesNo = questNo
            submitTime = Date()
            checkStatus = CheckStatus.PENDING.code
        }.also(attemptDAO::save)
    }

    @Transactional
    fun submitAttempt(token: String, questNo: String, userSubmit: Boolean) {
        guardUserSubmission(token, questNo, userSubmit) { parti, question ->
            forceAttempt(parti.runnersNo, question.quesNo)
        }
    }

    private fun getCurrentScore(parti: GameRegisterEntity, question: GameQuestionEntity): GameScoreEntity = scoreDAO
        .findByRunnersNoAndQuesNo(parti.runnersNo, question.quesNo)
        ?: GameScoreEntity().apply {
            runnersNo = parti.runnersNo
            quesNo = question.quesNo
            runnersName = parti.runnersName
            quesTitle = question.quesTitle
        }.also(scoreDAO::save)

    private fun doSQLBasedCheck() {
        val allParticipants = registerDAO.findAll()
        val sort = Sort.by(Sort.Direction.ASC,"quesNo")
        questionDAO.findByEnabledAndCheckType(1,CheckType.SQL.code,sort).forEach { question ->
            logger.debug("验证题目 {}:{}...", question.quesNo, question.quesTitle)

            allParticipants.forEach { parti ->
                val score = getCurrentScore(parti, question)

                if (AmsUtils.isNotNull(score.score)) {
                    // 如果已经pass了那么跳过
                    logger.debug("选手 {} 已通过题目 {}，不再重新评分", parti.runnersNo, question.quesNo)
                } else {
                    // 否则执行评分SQL
                    // 构造请求字典
                    val paramMap = mutableMapOf<String, String>()
                    paramMap.putAll(AmsJsonUtils.objectToMap(parti) as Map<String, String>)
                    paramMap.putAll(AmsJsonUtils.objectToMap(question) as Map<String, String>)
                    DataBus.setAttribute("paramMap", paramMap)

                    // 执行SQL
                    val rt = checkDAO.executeSql(question.checkSql, paramMap)

                    // 判断结果
                    if (rt.isNotEmpty()) {
                        // 如果结果非空则认为答题成功
                        score.score = question.score  // 直接给满分
                        score.scoreTime = Date() // 设置评分时间

                        scoreDAO.save(score)
                        scoreDAO.flush()
                        logger.info("选手 {} 通过题目 {}", parti.runnersNo, question.quesNo)
                    } else {
                        // 对于自动判分题，如果判分没有通过那么不登记为失败
                        logger.debug("选手 {} 没有通过题目 {}", parti.runnersNo, question.quesNo)
                    }
                }
            }
        }
    }

    private fun getQuestionForAttempt(quesNo: String, userSubmit: Boolean): GameQuestionEntity {
        val question = assertNotNull(questionDAO.findByQuesNo(quesNo)) { "编号 $quesNo 对应的题目不存在" }
        assertNotEqual(0, question.enabled) { "题目 ${question.quesNo} 已禁用" }
        assertNotEqual(CheckType.SQL.code, question.checkType) {
            "题目 ${question.quesNo} 验证方式为 ${CheckType.SQL.code}, 禁止创建提交"
        }
        if (userSubmit) {
            assertNotEqual(0, question.allowSubmit) {
                "题目 ${question.quesNo} 禁止主动提交"
            }
        }
        return question
    }

    private fun checkAttempt(attempt: GameAttemptEntity) {
        try {
            // 读取题目
            val question = getQuestionForAttempt(attempt.quesNo, false)

            // 读取用户
            val parti = assertNotNull(registerDAO.findByRunnersNo(attempt.runnersNo)) { "用户 ${attempt.runnersNo} 不存在" }

            when (question.checkType) {
                CheckType.CLASS.code -> {
                    val checkerClassName = assertNotNull(question.checkClass) { "题目 ${question.quesNo} 没有设置 checkClass" }

                    // 获取对应的验证器
                    val checker = if (checkerClassName.startsWith("@")) {
                        // 特殊情况，表示这里是bean名称而不是类名
                        context.getBean(checkerClassName.substring(1), QuestionChecker::class.java)
                    } else {
                        context.getBean(Class.forName(checkerClassName)) as QuestionChecker
                    }

                    // 执行验证
                    val result = checker.check(parti, question, attempt)

                    // 汇总最后结果
                    attempt.score = result.finalScore
                    attempt.checkResult = result.toResultMessage()
                    attempt.checkStatus = if (result.timeout) {
                        CheckStatus.TIMEOUT.code
                    } else {
                        CheckStatus.COMPLETE.code
                    }
                }

                else -> fail("题目 ${question.quesNo} 设置了不支持的验证方式 ${question.checkType}")
            }

            // 保存结果
            attempt.scoreTime = Date()
            attemptDAO.save(attempt)
            attemptDAO.flush()
            // 更新分数
            val score = getCurrentScore(parti, question)
            if (score.score == null || score.score < attempt.score) {
                score.score = attempt.score
                score.scoreAttempt = attempt.id
                score.scoreTime = attempt.scoreTime
                scoreDAO.save(score)
                scoreDAO.flush()
            }
        } catch (ex: Throwable) {
            logger.error("打分失败", ex)
            attempt.checkStatus = CheckStatus.ERROR.code
            attempt.checkResult = ex.message
            attempt.scoreTime = Date()
            attemptDAO.save(attempt)
            attemptDAO.flush()
        }
    }

    /**
     * 为校验给定的请求准备工作目录，
     * 该目录通常用来储存该次验证所需的数据（如随机生成的测试样例等）及结果数据等以备事后查阅
     *
     * 路径为 ~/njcb_games/attempt/<选手工号>/<题目编号>/<提交编号>/
     *
     * @param attempt 单次用户的答案提交
     * @return 用于该次验证的工作目录路径
     */
    fun ensureWorkingDir(attempt: GameAttemptEntity): Path = ensureWorkingDir(attempt.runnersNo, attempt.quesNo, attempt.id)

    fun ensureWorkingDir(runnersNo: String, questNo: String, attemptId: Int): Path {
        val p = workingDirBase / runnersNo / questNo / attemptId.toString()
        return p.createDirectories()
    }

    companion object {
        private val logger = LoggerFactory.getLogger(QuestionsCheckService::class.java)
    }
}