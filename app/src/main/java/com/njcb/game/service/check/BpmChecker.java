package com.njcb.game.service.check;

import com.google.common.collect.Maps;
import com.njcb.ams.support.exception.ExceptionUtil;
import com.njcb.ams.util.AmsAssert;
import com.njcb.game.repository.entity.GameAttemptEntity;
import com.njcb.game.repository.entity.GameQuestionEntity;
import com.njcb.game.repository.entity.GameRegisterEntity;
import com.njcb.game.util.AmsNetworkUtils;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@Component("bpmChecker")
public class BpmChecker implements QuestionChecker {
    private static final Logger logger = LoggerFactory.getLogger(QuestionChecker.class);

    @Autowired
    private RestTemplate restTemplate;

    @NotNull
    @Override
    public CheckResult check(@NotNull GameRegisterEntity parti, @NotNull GameQuestionEntity question, @NotNull GameAttemptEntity attempt) {
        logger.info("开始验证用户[{}]的题目[{}]", parti.getRunnersNo(), question.getQuesNo());
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        Map<Object, Object> param = Maps.newHashMap();
        param.put("businessNo", "0001");
        param.put("operator", "0001");
        param.put("businessAmount", 100);
        HttpEntity httpEntity = new HttpEntity(param, headers);
        String url = parti.getServerAddr() + "/contest/businessSubmit";
        if (!AmsNetworkUtils.testUrlIsExist(url)) {
            ExceptionUtil.throwAppException("验证地址[" + url + "]不存在");
        }
        ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST, httpEntity, String.class);
        logger.debug("\ntoken刷新令牌:" + responseEntity.getBody());
        if (HttpStatus.OK == responseEntity.getStatusCode()) {
            String body = responseEntity.getBody();
        } else {
            AmsAssert.fail(responseEntity.getBody());
        }
        CheckResult checkResult = new CheckResult(10, 1, 1, 0, 0, Boolean.FALSE);
        return checkResult;
    }
}
