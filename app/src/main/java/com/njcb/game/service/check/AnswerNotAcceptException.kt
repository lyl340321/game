package com.njcb.game.service.check

class AnswerNotAcceptException(message: String? = null) : RuntimeException(message)
