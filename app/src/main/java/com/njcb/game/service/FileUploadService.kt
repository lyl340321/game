package com.njcb.game.service

import com.njcb.game.pojo.CheckStatus
import com.njcb.game.pojo.FileUploadStatus
import com.njcb.game.repository.dao.GameAttemptRepository
import com.njcb.game.repository.dao.GameRegisterRepository
import com.njcb.game.repository.dao.GameUploadRepository
import com.njcb.game.repository.entity.GameAttemptEntity
import com.njcb.game.repository.entity.GameRegisterEntity
import com.njcb.game.repository.entity.GameUploadEntity
import com.njcb.game.util.*
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import org.springframework.transaction.PlatformTransactionManager
import java.util.*

@Service
class FileUploadService(
    private val gameUploadRepository: GameUploadRepository,
    private val gameRegisterRepository: GameRegisterRepository,
    private val gameAttemptRepository: GameAttemptRepository,
    private val transactionManager: PlatformTransactionManager,
) {
    fun registerUploadForAttempt(attemptId: Int, caseNo: Int): GameUploadEntity {
        return GameUploadEntity().apply {
            checkAttempt = attemptId
            this.caseNo = caseNo
            status = FileUploadStatus.PENDING.code
        }.also(gameUploadRepository::save)
    }

    fun upload(token: String, action: (parti: GameRegisterEntity, attempt: GameAttemptEntity, upload: GameUploadEntity) -> Unit) {
        val parti = assertNotNull(gameRegisterRepository.findByToken(token)) { "未知的 token $token" }

        // 获取用户在 Checking 状态的status
        // 由于我们是单线程判分，应该最多只有一个
        val attempts = gameAttemptRepository.findByRunnersNoAndCheckStatus(parti.runnersNo, CheckStatus.CHECKING.code)
        assertEqual(1, attempts.size) { "当前状态不允许上传文件" }
        val attempt = attempts[0]

        val uploads = gameUploadRepository.findByIdAndStatus(attempt.id, FileUploadStatus.PENDING.code)
        assertEqual(1, uploads.size) { "当前状态不允许上传文件" }
        val upload = uploads[0]

        // 更新为 uploading 状态
        transactionManager.executeInNewTransaction {
            gameUploadRepository.findById(upload.id).get().also {
                assertEqual(FileUploadStatus.PENDING.code, it.status) {"当前状态不允许上传文件"}

                it.status = FileUploadStatus.UPLOADING.code
                gameUploadRepository.save(it)
            }
        }

        try {
            //执行文件上传
            action(parti, attempt, upload)

            // 更新为 uploaded
            transactionManager.executeInNewTransaction {
                gameUploadRepository.findById(upload.id).get().also {
                    assertEqual(FileUploadStatus.UPLOADING.code, it.status) {"当前状态不允许上传文件"}

                    it.status = FileUploadStatus.UPLOADED.code
                    it.uploadTime = Date()
                    gameUploadRepository.save(it)
                }
            }
        } catch (e: Exception) {
            logger.error("Failed to upload file: {}", upload.id, e)
            // 更新为失败状态
            transactionManager.executeInNewTransaction {
                gameUploadRepository.findById(upload.id).get().also {
                    assertEqual(FileUploadStatus.UPLOADING.code, it.status) {"当前状态不允许上传文件"}

                    it.status = FileUploadStatus.CANCELLED.code
                    gameUploadRepository.save(it)
                }
            }
            fail("文件上传失败")
        }
    }

    companion object {
        private val logger = LoggerFactory.getLogger(FileUploadService::class.java)
    }
}
