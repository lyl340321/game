package com.njcb.game.service.check

import com.njcb.ams.util.AmsJsonUtils
import com.njcb.ams.util.AmsUtils
import com.njcb.game.application.SimpleTestCaseManager
import com.njcb.game.pojo.CheckStatus
import com.njcb.game.pojo.SimpleTestCase
import com.njcb.game.pojo.SimpleTestResultDetail
import com.njcb.game.pojo.TestCaseResult
import com.njcb.game.pojo.check.OpticalCharacterRecognition
import com.njcb.game.repository.entity.GameAttemptEntity
import com.njcb.game.repository.entity.GameQuestionEntity
import com.njcb.game.repository.entity.GameRegisterEntity
import com.njcb.game.service.FileIOService
import com.njcb.game.service.QuestionsCheckService
import com.njcb.game.util.LevenShtein
import com.njcb.game.util.assertEqual
import com.njcb.game.util.assertNotNull
import feign.Request
import feign.Retryer
import org.slf4j.LoggerFactory
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.context.annotation.Bean
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import java.net.URI
import java.time.Duration
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException
import kotlin.io.path.div

/**
 * 一个通用的基于RESTful接口的评分类
 */
class RestChecker(
    private val checkService: QuestionsCheckService,
    private val testCaseManager: SimpleTestCaseManager,
    private val checkClient: RestCheckClient,
    private val fileIO: FileIOService,
    private val timeoutManager: CheckTimeoutManager,
) : QuestionChecker {

    override fun check(
        parti: GameRegisterEntity,
        question: GameQuestionEntity,
        attempt: GameAttemptEntity
    ): CheckResult {
        assertEqual(CheckStatus.CHECKING.code, attempt.checkStatus) {
            "提交 ${attempt.id} 状态有误：需要 ${CheckStatus.CHECKING}，实际 ${attempt.checkStatus}"
        }

        val checkAddr = assertNotNull(parti.serverAddr)

        // 准备工作目录
        val workingDir = checkService.ensureWorkingDir(attempt)

        // 准备本次评分使用的测试样例
        val testCases = testCaseManager.pickTestCases(questNo = question.quesNo)

        // 开始验证结果
        val timeout = timeoutManager.newSession(Duration.ofSeconds(question.checkTimeout.toLong()))
        val session = CheckSession(questionScore = question.score, totalCaseCount = testCases.size)
        for (testCase in testCases) {
            val result = verify(timeout, checkAddr, question, testCase)

            // 储存结果
            fileIO.writeJson(workingDir / "${session.caseNo}.json", result)

            // 累计分数
            if (session.parseCaseResult(
                    if ("009" == question.quesNo) { //Ugly
                        testCase.score
                    } else {
                        (testCase.score * result.value).toInt()
                    }, result.result
                )
            ) {
                break
            }
        }

        return session.getResult()
    }

    private fun verify(
        timeout: CheckTimeoutSession,
        addr: String,
        question: GameQuestionEntity,
        testCase: SimpleTestCase,
    ): SimpleTestResultDetail {
        try {
            val result = timeout.runWithTimeout {
                checkClient.test(
                    URI(addr),
                    Request.Options(
                        question.checkTimeout.toLong(),
                        TimeUnit.SECONDS,
                        question.checkTimeout.toLong(),
                        TimeUnit.SECONDS,
                        true
                    ),
                    question.quesNo,
                    testCase.input
                )
            }
            //ocr hardcore
            val cr: TestCaseResult
            var number: Float = 0F
            if ("009" == question.quesNo) { //未来需要一个额外的数据库字段来指代使用列文斯坦的相似度算法, 今天先硬编码
                val expJson = AmsJsonUtils.mapToJson(testCase.expect)
                val resJson = AmsJsonUtils.mapToJson(result)
                val expected: OpticalCharacterRecognition =
                    AmsJsonUtils.jsonToObject(expJson, OpticalCharacterRecognition::class.java)
                val resulted: OpticalCharacterRecognition =
                    AmsJsonUtils.jsonToObject(resJson, OpticalCharacterRecognition::class.java)
                val expStr = expected.toString()
                val resStr = resulted.toString()
                val steps: Int = LevenShtein.levenshteinDistance(expStr, resStr)
                if (steps == expStr.length) {
                    cr = TestCaseResult.NOT_ACCEPT
                } else {
                    cr = TestCaseResult.ACCEPT
                    number = 1 - steps.toFloat() / expStr.length
                }
            } else {
                cr = if (result == testCase.expect) {
                    TestCaseResult.ACCEPT
                } else {
                    TestCaseResult.NOT_ACCEPT
                }
            }

            return SimpleTestResultDetail(testCase, result, cr, null, number)
        } catch (ex: TimeoutException) {
            logger.error("验证超时", ex)
            return SimpleTestResultDetail(
                testCase, null, TestCaseResult.TIMEOUT, ex.stackTraceToString(), 0F
            )
        } catch (ex: Throwable) {
            logger.error("验证失败", ex)
            return SimpleTestResultDetail(
                testCase, null, TestCaseResult.ERROR, ex.stackTraceToString(), 0F
            )
        }
    }

    companion object {
        private val logger = LoggerFactory.getLogger(RestChecker::class.java)
    }
}

class RestCheckClientConfig {

    /** 不重试对选手接口的调用 */
    @Bean
    fun retryer(): Retryer = Retryer.NEVER_RETRY
}

@FeignClient("RestCheckClient", url = "dummy", configuration = [RestCheckClientConfig::class])
interface RestCheckClient {

    @PostMapping("/question/{questNo}")
    fun test(
        baseUrl: URI,
        options: Request.Options,
        @PathVariable questNo: String,
        @RequestBody input: Map<String, Any>
    ): Map<String, Any>
}
