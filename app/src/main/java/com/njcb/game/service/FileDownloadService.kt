package com.njcb.game.service

import com.njcb.ams.util.AmsUtils
import com.njcb.game.repository.dao.GameDownloadRepository
import com.njcb.game.repository.dao.GameRegisterRepository
import com.njcb.game.repository.entity.GameDownloadEntity
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.nio.file.Path
import kotlin.io.path.absolutePathString
import kotlin.jvm.optionals.getOrNull

@Service
class FileDownloadService(
    private val gameDownloadRepository: GameDownloadRepository,
    private val gameRegisterDAO: GameRegisterRepository,
) {

    fun registerFileForDownload(fileName: String, path: Path, userNo: String? = null): GameDownloadEntity {
        // TODO: 校验输入合法性
        return GameDownloadEntity().apply {
            runnersNo = userNo
            this.fileName = fileName
            this.filePath = path.absolutePathString()
        }.also(gameDownloadRepository::save)
    }

    fun downloadFile(id: Int, token: String): GameDownloadEntity? {
        val user = gameRegisterDAO.findByToken(token) ?: run {
            logger.warn("未知的token {}", token)
            return null
        }
        val file = gameDownloadRepository.findById(id).getOrNull() ?: run {
            logger.warn("下载id {} 不存在", id)
            return null
        }
        if (AmsUtils.isNotNull(file.runnersNo) && user.runnersNo != file.runnersNo) {
            logger.warn("用户 {} 无权下载文件 {}", user.runnersNo, id)
            return null
        }

        return file
    }
    fun downloadFile(id: Int): GameDownloadEntity? {
        val file = gameDownloadRepository.findById(5).getOrNull() ?: run {
            logger.warn("Download Id {} not exist.", id)
            return null
        }

        return file
    }

    companion object {
        private val logger = LoggerFactory.getLogger(FileDownloadService::class.java)
    }
}
