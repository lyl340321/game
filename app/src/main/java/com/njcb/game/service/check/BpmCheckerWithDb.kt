package com.njcb.game.service.check

import com.njcb.ams.util.AmsJsonUtils
import com.njcb.game.repository.dao.GameBpmTestcaseRepository
import com.njcb.game.repository.entity.GameAttemptEntity
import com.njcb.game.repository.entity.GameQuestionEntity
import com.njcb.game.repository.entity.GameRegisterEntity
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import org.springframework.stereotype.Component
import org.springframework.web.client.RestTemplate
import kotlin.math.roundToInt

@Component("bpmCheckerWithDb")
data class BpmCheckerWithDb(
    @Autowired val restTemplate: RestTemplate,
    @Autowired val bpm: GameBpmTestcaseRepository,

    ) : QuestionChecker {
    private val logger: Logger = LoggerFactory.getLogger(BpmCheckerWithDb::class.java)

    init {
    }

    override fun check(
        parti: GameRegisterEntity,
        question: GameQuestionEntity,
        attempt: GameAttemptEntity
    ): CheckResult {
        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_JSON
        //val mapper = com.fasterxml.jackson.module.kotlin.jacksonObjectMapper()
        val cases = bpm.findAll().sortedBy { q -> q.id }
        var value: Double = 0.0
        var accept: Int = 0
        var notAccept: Int = 0
        var errorCount: Int = 0
        val total: Int = cases.size
        logger.info("parti={},question={},attempt={}", parti.runnersNo, question.quesNo, attempt.id)
        for (case in cases) {
            val url = "${parti.serverAddr}${case.url}"
            when (case.exchangeMethod?.uppercase()) {
                "POST" -> {
                    val request = HttpEntity(case.exchangeJson, headers)
                    val response = restTemplate.exchange(url, HttpMethod.POST, request, String::class.java)
                    if (response.hasBody()) {
                        val body = response.body
                        if (response.statusCode.is2xxSuccessful) {
                            //把选手返回的body转换成ExpectedClass
                            //其中集合类需要重写Hash, 因为ExpectedClass是作为POJO类, 参与比对即可按照需要的字段进行判断
                            val expected = AmsJsonUtils.jsonToObject(
                                case.expectedJson,
                                Class.forName(case.expectedClass)
                            ) // mapper.readValue(case.expectedJson, Class.forName(case.expectedClass))
                            val userResp = AmsJsonUtils.jsonToObject(
                                body,
                                Class.forName(case.expectedClass)
                            ) //mapper.readValue(body, Class.forName(case.expectedClass))
                            //比对两个类，是否返回的类里面包含需要检测的
                            try {
                                checkAnswerEqual(
                                    expected,
                                    userResp,
                                    "",
                                    case.compareProperties?.split(";")?.toSet(),
                                    null
                                )
                                value += case.value
                                accept++
                            } catch (t: AnswerNotAcceptException) {
                                notAccept++
                            } catch (ex: Exception) {
                                errorCount++
                            }
                        } else errorCount++
                    } else errorCount++
                    logger.info(
                        "run test => {}, value={}, accept={}, notAcc={}, error={}",
                        url,
                        value,
                        accept,
                        notAccept,
                        errorCount
                    )
                }

                "GET" -> {
                    //目前没有处理类似url?param=..形式，理论上应该用request里面配置
                    //val response = restTemplate.exchange(url, HttpMethod.GET, null, String::class.java)


                }

                else -> {

                }
            }
            try {
                Thread.sleep(case.sleepSeconds * 1000L);
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }
        }
        return CheckResult((question.score * value).roundToInt(), total, accept, notAccept, errorCount, false)
    }
}