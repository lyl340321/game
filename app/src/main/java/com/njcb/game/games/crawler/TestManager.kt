package com.njcb.game.games.crawler

import com.njcb.game.repository.dao.GameCrawlerAttemptRepository
import com.njcb.game.repository.dao.GameRegisterRepository
import com.njcb.game.repository.entity.*
import com.njcb.game.service.QuestionsCheckService
import com.njcb.game.service.check.CheckResult
import com.njcb.game.util.*
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import org.springframework.transaction.PlatformTransactionManager
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.multipart.MultipartFile
import java.nio.file.Path
import java.nio.file.Paths
import java.time.temporal.ChronoUnit
import java.util.*
import kotlin.io.path.createDirectories
import kotlin.io.path.div

@Component("crawlerTestManager")
class TestManager(
    @Value("\${game.crawler.question-no}") private val questNo: String,
    @Value("\${game.working-dir}")
    baseWorkingDir: String,
    private val checkService: QuestionsCheckService,
    private val gameRegisterDAO: GameRegisterRepository,
    private val transactionManager: PlatformTransactionManager,
    private val crawlerAttemptDAO: GameCrawlerAttemptRepository,
) {
    private val testBaseDir = Paths.get(baseWorkingDir, "crawler")

    fun prepareTest(token: String) {
        // 创建 attempt
        val attempt = transactionManager.executeInNewTransaction {
            checkService.guardUserSubmission(token, questNo, false) { parti, _ ->
                // 限制提交频率
                crawlerAttemptDAO.selectLatestAttempt(parti.runnersNo)?.let { latestAttempt ->
                    RequestUtil.checkSubmitFreq(latestAttempt.submitTime)
                }

                // 检查是否有还在初始化的提交
//                crawlerAttemptDAO.findAll().apply {
//                    createCriteria().apply {
//                        andRunnersNoEqualTo(parti.runnersNo).andStatusEqualTo(CrawlerAttemptStatus.INIT.code)
//                    }
//                })?.let { fail("提交过于频繁") }

                // 放弃用户当前尚未完成的所有提交
                crawlerAttemptDAO.findByRunnersNoAndStatusNotIn(
                    parti.runnersNo, listOf(
                        CrawlerAttemptStatus.CANCELLED.code,
                        CrawlerAttemptStatus.COMPLETED.code,
                    )
                ).forEach { attempt ->
                    run {
                        attempt.status = CrawlerAttemptStatus.CANCELLED.code
                        crawlerAttemptDAO.save(attempt)
                    }
                };

                // 创建 attempt
                GameCrawlerAttemptEntity().apply {
                    runnersNo = parti.runnersNo
                    submitTime = Date()
                    status = CrawlerAttemptStatus.INIT.code
                }.also(crawlerAttemptDAO::save)
            }
        }

        try {
            // 准备测试样例
            val workingDir = attempt.workingDir.createDirectories()
            // TODO: 生成测试样例

            // 准备完成，等待用户上传
            transactionManager.executeInNewTransaction {
                attempt.status = CrawlerAttemptStatus.WAITING.code
                crawlerAttemptDAO.save(attempt)
            }
        } catch (e: Throwable) {
            // 取消attempt
            logger.error("初始化测试样例失败，取消本次评分", e)
            transactionManager.executeInNewTransaction {
                attempt.status = CrawlerAttemptStatus.CANCELLED.code
                crawlerAttemptDAO.save(attempt)
            }
            throw e
        }
    }

    @Transactional
    fun uploadResult(token: String, file: MultipartFile) {
        val parti = assertNotNull(gameRegisterDAO.findByToken(token)) { "未知的 token $token" }
        // 获取最新的用户提交
        val attempt = assertNotNull(crawlerAttemptDAO.selectLatestAttempt(parti.runnersNo)) {
            "抓取尚未开始，无法上传结果"
        }
        // 校验提交状态
        assertEqual(
            CrawlerAttemptStatus.WAITING.code,
            attempt.status
        ) {
            when (attempt.status) {
                CrawlerAttemptStatus.UPLOADED.code -> "禁止重复上传"
                else -> "抓取尚未开始，无法上传结果"
            }
        }

        // 保存文件
        val workingDir = attempt.workingDir
        file.transferTo(workingDir / "upload.zip")

        // 创建提交
        val checkAttempt = checkService.forceAttempt(parti.runnersNo, questNo)

        // 更新状态
        attempt.status = CrawlerAttemptStatus.UPLOADED.code
        attempt.checkAttempt = checkAttempt.id
        crawlerAttemptDAO.save(attempt)
    }

    fun check(parti: GameRegisterEntity, question: GameQuestionEntity, attempt: GameAttemptEntity): CheckResult {
        // 读取对应的提交
        val crawlerAttempt = transactionManager.executeInNewTransaction {
            assertNotNull(
                crawlerAttemptDAO.findByRunnersNoAndCheckAttempt(
                    parti.runnersNo,
                    attempt.id,
                )
            ) {
                "找不到 ${parti.runnersNo}:${attempt.id} 的提交"
            }.let {
                assertEqual(CrawlerAttemptStatus.UPLOADED.code, it.status) {
                    "提交状态为 ${it.status}, 需要 ${CrawlerAttemptStatus.UPLOADED.code}"
                }
                // 更新状态
                it.status = CrawlerAttemptStatus.COMPLETED.code
                crawlerAttemptDAO.save(it)
            }
        }

        // 检查是否超时
        if (ChronoUnit.SECONDS.between(crawlerAttempt.submitTime.toInstant(), attempt.submitTime.toInstant())
            > question.checkTimeout
        ) {
            return CheckResult(
                finalScore = 0,
                totalCaseCount = 1,
                acceptCount = 0,
                notAcceptCount = 0,
                errorCount = 1,
                timeout = true
            )
        }

        val workingDir = crawlerAttempt.workingDir
        // TODO: 解析结果

        TODO("Not yet implemented")
    }

    private val GameCrawlerAttemptEntity.workingDir: Path
        get() = testBaseDir / runnersNo / id.toString()

    companion object {
        private val logger = LoggerFactory.getLogger(TestManager::class.java)
    }
}
