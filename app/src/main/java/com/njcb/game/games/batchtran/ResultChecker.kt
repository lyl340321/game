package com.njcb.game.games.batchtran

import com.dcitsbiz.esb.metadata.ReqSysHeadType
import com.dcitsbiz.esb.services._10001000001.Req1000100000101Type
import com.dcitsbiz.esb.services._10001000001.Req1000100000102Type
import com.dcitsbiz.esb.services._10001000001.ReqAppHeadType
import com.dcitsbiz.esb.services._10001000001.Rsp1000100000102Type
import com.dcitsbiz.esb.services._10001000001.wsdl.ESBServerPortType
import com.njcb.ams.service.ParamManageService
import com.njcb.ams.store.esbmodule.EsbHeadUtil
import com.njcb.ams.support.comm.SequenceService
import com.njcb.ams.support.trade.TradeUtil
import com.njcb.game.esb.EsbUtils
import com.njcb.game.esb.EsbUtils.esbClient
import com.njcb.game.pojo.CheckStatus
import com.njcb.game.pojo.TestCaseResultDetail
import com.njcb.game.repository.dao.GameTransAccountRepository
import com.njcb.game.repository.entity.*
import com.njcb.game.service.FileDownloadService
import com.njcb.game.service.FileIOService
import com.njcb.game.service.QuestionsCheckService
import com.njcb.game.service.check.*
import com.njcb.game.service.readJson
import com.njcb.game.util.*
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import org.springframework.transaction.PlatformTransactionManager
import java.io.Writer
import java.nio.file.Path
import java.time.Duration
import java.util.concurrent.ThreadLocalRandom
import java.util.concurrent.TimeoutException
import kotlin.io.path.bufferedWriter
import kotlin.io.path.createDirectories
import kotlin.io.path.div

@Component("batchTranResultChecker")
class ResultChecker(
    private val checkService: QuestionsCheckService,
    private val downloadService: FileDownloadService,
    private val transAccountDAO: GameTransAccountRepository,
    private val transactionManager: PlatformTransactionManager,
    private val fileIO: FileIOService,
    private val paramManageService: ParamManageService,
    private val timeoutManager: CheckTimeoutManager,
) : QuestionChecker {
    override fun check(parti: GameRegisterEntity, question: GameQuestionEntity, attempt: GameAttemptEntity): CheckResult {
        assertEqual(CheckStatus.CHECKING.code, attempt.checkStatus) {
            "提交 ${attempt.id} 状态有误：需要 ${CheckStatus.CHECKING}，实际 ${attempt.checkStatus}"
        }

        val checkAddr = assertNotNull(parti.serverAddr)

        // 准备工作目录
        val workingDir = checkService.ensureWorkingDir(attempt)

        val timeout = timeoutManager.newSession(Duration.ofSeconds(question.checkTimeout.toLong()))

        // 准备本次评分使用的测试样例
        val testCases: List<Pair<Int, (Int) -> TestCaseResultDetail>> = listOf<Pair<Int, (Int) -> TestCaseResultDetail>>(
            // 正常样例
            10 to { caseNo -> runTestCaseAllGood(timeout, checkAddr, parti, question, attempt, workingDir, caseNo, 20, 3) },
            10 to { caseNo -> runTestCaseAllGood(timeout, checkAddr, parti, question, attempt, workingDir, caseNo, 20, 3) },
            10 to { caseNo -> runTestCaseAllGood(timeout, checkAddr, parti, question, attempt, workingDir, caseNo, 20, 3) },
            15 to { caseNo -> runTestCaseAllGood(timeout, checkAddr, parti, question, attempt, workingDir, caseNo, 50, 8) },
            25 to { caseNo -> runTestCaseAllGood(timeout, checkAddr, parti, question, attempt, workingDir, caseNo, 50, 15) },

            // 文件格式错误
            10 to { caseNo -> runTestCaseInvalidFile(timeout, checkAddr, parti, question, attempt, workingDir, caseNo, InvalidFileType.NEGATIVE_AMOUNT) },
            10 to { caseNo -> runTestCaseInvalidFile(timeout, checkAddr, parti, question, attempt, workingDir, caseNo, InvalidFileType.TOTAL_MESS) },

            // 通用记账接口调用失败
            10 to { caseNo -> runTestCaseTransAccountFailure(timeout, checkAddr, parti, question, attempt, workingDir, caseNo) },
        ).shuffled()

        // 开始验证结果
        val session = CheckSession(questionScore = question.score, totalCaseCount = testCases.size)
        for ((score, action) in testCases) {
            val result = try {
                action(session.caseNo)
            } catch (ex: AnswerNotAcceptException) {
                TestCaseResultDetail.notAccept(ex.message)
            } catch (ex: TimeoutException) {
                logger.error("验证超时", ex)
                TestCaseResultDetail.timeout(ex.stackTraceToString())
            } catch (ex: Throwable) {
                logger.error("验证失败", ex)
                TestCaseResultDetail.error(ex.stackTraceToString())
            }

            // 储存结果
            fileIO.writeJson(
                workingDir / session.caseNo.toString() / "result.json",
                result
            )

            // 累计分数
            if (session.parseCaseResult(score, result.result)) {
                break
            }
        }

        return session.getResult()
    }

    private fun prepareWorkingDir(
        baseWorkDir: Path,
        caseNo: Int,
    ): Path {
        val workingDir = baseWorkDir / caseNo.toString()
        workingDir.createDirectories()
        return workingDir
    }

    /** 生成新的批量记账文件并注册下载链接 */
    private fun prepareInputFile(
        parti: GameRegisterEntity,
        workingDir: Path,
        writer: (Writer) -> Unit,
    ): GameDownloadEntity {
        // 生成输入文件
        val transFile = workingDir / "trans.csv"
        transFile.bufferedWriter(charset = Charsets.UTF_8).use(writer)
        // 登记下载
        return transactionManager.executeInNewTransaction {
            downloadService.registerFileForDownload("trans.csv", transFile, parti.runnersNo)
        }
    }

    private fun List<TransAccount>.writeTo(writer: Writer) {
        forEachIndexed { index, tx ->
            writer.write(
                "${index + 1001}|${tx.payer.name}|${tx.payer.number}|${tx.payee.name}|${tx.payee.number}|${tx.amount.toPlainString()}\n"
            )
        }
    }

    /** 设置新的全局流水号 */
    private fun setNextGlobalSeq(): String {
        val globalSeq = SequenceService.getGlobalSeq()
        TradeUtil.setGlobalSeq(globalSeq)
        logger.debug("全局流水号 {}", globalSeq)
        return globalSeq
    }

    /** 插入一条通用记账记录，等待选手的调用 */
    private fun insertTransAccount(
        parti: GameRegisterEntity,
        question: GameQuestionEntity,
        attempt: GameAttemptEntity,
        caseNo: Int,
        globalSeq: String,
    ): Int = transactionManager.executeInNewTransaction {
        GameTransAccountEntity().apply {
            this.runnersNo = parti.runnersNo
            this.quesNo = question.quesNo
            this.attempt = attempt.id
            this.caseNo = caseNo
            this.globalSeq = globalSeq
        }.also(transAccountDAO::save)
    }.id

    /** 调用选手接口 */
    private fun callService(
        timeout: CheckTimeoutSession,
        addr: String,
        workingDir: Path,
        gameDownloadEntity: GameDownloadEntity,
        caseNo: Int,
        globalSeq: String,
    ): Pair<Req1000100000102Type, Rsp1000100000102Type> {
        val esb = esbClient<ESBServerPortType>("${addr}/services/S10001000001")
        EsbHeadUtil.setIsHeadProxy(false) // 本次禁用自带的ESB错误码解析
        EsbUtils.dumpMessagesTo(esb, workingDir, "BatchAccount") // 增加interceptor以记录具体请求和应答内容
        val req = Req1000100000102Type().apply {
            reqSysHead = ReqSysHeadType().apply {
                this.globalSeq = globalSeq
            }
            reqAppHead = ReqAppHeadType().apply {
                tlrNo = randomString(5)
                brId = randomString(5)
            }
            reqAppBody = Req1000100000102Type.ReqAppBody().apply {
                applicantCustNo = randomString(20)
                applicantName = randomString(15)
                batchSerialNo = caseNo.toString()
                fileUrl = "${paramManageService.getValueByTypeAndName("GAME", "context-url")}/download/${gameDownloadEntity.id}"
            }
        }
        return req to timeout.runWithTimeout { esb.batchaccount(req) }
    }

    /** 校验通用记账接口是否被调用了指定次数 */
    private fun assertTransAccountInvoke(
        transAccountId: Int,
        expectedInvokeCount: Int
    ) {
        // 读取 TransAccount 调用记录
        val transAccount = assertNotNull(transactionManager.executeInNewTransaction {
            transAccountDAO.findById(transAccountId).get()
        }) { "ID [$transAccountId] 对应的 TransAccount 调用记录不存在" }

        assertEqual(expectedInvokeCount, transAccount.invokeCount) {
            throw AnswerNotAcceptException("TransAccount 接口调用次数为 ${transAccount.invokeCount}, 应为 1")
        }
    }

    private fun runTestCaseAllGood(
        timeout: CheckTimeoutSession,
        addr: String,
        parti: GameRegisterEntity,
        question: GameQuestionEntity,
        attempt: GameAttemptEntity,
        baseWorkDir: Path,
        caseNo: Int,
        txCount: Int,
        accCount: Int,
    ): TestCaseResultDetail {
        // 准备工作目录
        val workingDir = prepareWorkingDir(baseWorkDir, caseNo)

        // 生成测试数据
        val testTxs = generateTransactions(txCount, accCount)
        // 计算答案
        val expectedTxs = testTxs
            .sumTransactions()
            .map {
                // 转换成目标格式
                Req1000100000101Type.ReqAppBody.Details().apply {
                    draweeAcctName = it.payer.name
                    draweeAccount = it.payer.number
                    payeeAcctName = it.payee.name
                    payeeAccount = it.payee.number
                    amount = it.amount.toPlainString()
                }
            }

        // 保存测试样例配置
        fileIO.writeJson(
            workingDir / "case.json",
            TestCase(
                TestType.ALL_GOOD,
                expectedTransAccounts = expectedTxs,
            )
        )

        // 生成输入文件
        val gameDownload = prepareInputFile(parti, workingDir) { writer -> testTxs.writeTo(writer) }

        // 生成全局流水号
        val globalSeq = setNextGlobalSeq()

        // 写入 TransAccount 记录
        val transAccountId = insertTransAccount(parti, question, attempt, caseNo, globalSeq)

        // 准备完成，调用选手接口
        val (req, resp) = callService(timeout, addr, workingDir, gameDownload, caseNo, globalSeq)

        // 校验结果
        // 校验 TransAccount 调用请求
        assertTransAccountInvoke(transAccountId, 1)
        val transAccountReq = fileIO.readJson<Req1000100000101Type>(
            workingDir / "TransAccount.req.0.json"
        )
        // TODO: 更完善的请求头校验
        checkAnswerEqual(globalSeq, transAccountReq.reqSysHead.globalSeq, "TransAccount 请求的 reqSysHead")
        checkAnswerEqual(req.reqAppHead, transAccountReq.reqAppHead, "TransAccount 请求的 reqAppHead")

        checkAnswerEqual(req.reqAppBody.applicantName, transAccountReq.reqAppBody.applicantName, "TransAccount 请求的 reqAppBody.applicantName")
        checkAnswerEqual(req.reqAppBody.applicantCustNo, transAccountReq.reqAppBody.applicantCustNo, "TransAccount 请求的 reqAppBody.applicantCustNo")
        // 校验答案
        val submittedTransAccounts = transAccountReq.reqAppBody.details
        // 先判断一下数量
        checkAnswerEqual(expectedTxs.size, submittedTransAccounts.size, "TransAccount 请求的 reqAppBody.details")
        // 然后检查能不能对的上
        checkAnswerUnordered(expectedTxs, submittedTransAccounts, ::propertiesEqual)

        // 校验接口返回值
        // TODO: 更完善的返回头校验
        checkAnswerEqual(globalSeq, resp.rspSysHead.globalSeq, "resp.rspSysHead.globalSeq")
        checkAnswerEqual("S", resp.rspSysHead.transStatus, "resp.rspSysHead.transStatus")
        checkAnswerEqual(req.reqAppHead, resp.rspAppHead, "resp.rspAppHead")
        checkAnswerEqual(txCount.toBigInteger(), resp.rspAppBody.totalCount, "resp.rspAppBody.totalCount")

        return TestCaseResultDetail.accept()
    }

    /** 非法文件的类型 */
    private enum class InvalidFileType {
        /** 有负值 */
        NEGATIVE_AMOUNT,

        /** 格式完全错误 */
        TOTAL_MESS,
    }

    private fun runTestCaseInvalidFile(
        timeout: CheckTimeoutSession,
        addr: String,
        parti: GameRegisterEntity,
        question: GameQuestionEntity,
        attempt: GameAttemptEntity,
        baseWorkDir: Path,
        caseNo: Int,
        type: InvalidFileType,
    ): TestCaseResultDetail {
        // 准备工作目录
        val workingDir = prepareWorkingDir(baseWorkDir, caseNo)

        // 保存测试样例配置
        fileIO.writeJson(
            workingDir / "case.json",
            TestCase(
                TestType.INVALID_FILE,
            )
        )

        // 生成输入文件
        val gameDownload = prepareInputFile(parti, workingDir) { writer ->
            when (type) {
                InvalidFileType.NEGATIVE_AMOUNT -> {
                    // 先生成一些合法数据
                    val testTxs = generateTransactions(20, 5).toMutableList()
                    // 然后将其中一个金额改为负
                    testTxs[0] = testTxs[0].let { it.copy(amount = it.amount.negate()) }
                    // 然后打乱顺序
                    testTxs.shuffle()
                    // 写入文件
                    testTxs.writeTo(writer)
                }

                InvalidFileType.TOTAL_MESS -> {
                    writer.write(
                        // 写入随机数据
                        randomString(1000)
                    )
                }
            }
        }

        // 生成全局流水号
        val globalSeq = setNextGlobalSeq()

        // 写入 TransAccount 记录
        val transAccountId = insertTransAccount(parti, question, attempt, caseNo, globalSeq)

        // 准备完成，调用选手接口
        val (req, resp) = callService(timeout, addr, workingDir, gameDownload, caseNo, globalSeq)

        // 校验结果
        // 校验 TransAccount 调用请求
        assertTransAccountInvoke(transAccountId, 0)

        // 校验接口返回值
        // TODO: 更完善的返回头校验
        checkAnswerEqual(globalSeq, resp.rspSysHead.globalSeq, "resp.rspSysHead.globalSeq")
        checkAnswerEqual("F", resp.rspSysHead.transStatus, "resp.rspSysHead.transStatus")

        return TestCaseResultDetail.accept()
    }


    private fun runTestCaseTransAccountFailure(
        timeout: CheckTimeoutSession,
        addr: String,
        parti: GameRegisterEntity,
        question: GameQuestionEntity,
        attempt: GameAttemptEntity,
        baseWorkDir: Path,
        caseNo: Int,
    ): TestCaseResultDetail {
        // 准备工作目录
        val workingDir = prepareWorkingDir(baseWorkDir, caseNo)

        // 生成测试数据
        val testTxs = generateTransactions(10, 3)
        val expectedErrorCode = "${randomString(5)}E${randomString(4)}"
        val expectedErrorMessage = "Error message ${randomString(10)}"

        // 保存测试样例配置
        fileIO.writeJson(
            workingDir / "case.json",
            TestCase(
                TestType.TRANS_ACCOUNT_ERROR,
                expectedErrorCode = expectedErrorCode,
                expectedErrorMessage = expectedErrorMessage,
            )
        )

        // 生成输入文件
        val gameDownload = prepareInputFile(parti, workingDir) { writer -> testTxs.writeTo(writer) }

        // 生成全局流水号
        val globalSeq = setNextGlobalSeq()

        // 写入 TransAccount 记录
        val transAccountId = insertTransAccount(parti, question, attempt, caseNo, globalSeq)

        // 准备完成，调用选手接口
        val (req, resp) = callService(timeout, addr, workingDir, gameDownload, caseNo, globalSeq)

        // 校验结果
        // 校验 TransAccount 调用请求
        assertTransAccountInvoke(transAccountId, 1)

        // 校验接口返回值
        // TODO: 更完善的返回头校验
        checkAnswerEqual(globalSeq, resp.rspSysHead.globalSeq, "resp.rspSysHead.globalSeq")
        checkAnswerEqual("F", resp.rspSysHead.transStatus, "resp.rspSysHead.transStatus")
        checkAnswerEqual(expectedErrorCode, resp.rspSysHead.retCode, "resp.rspSysHead.retCode")
        checkAnswerEqual(expectedErrorMessage, resp.rspSysHead.retMsg, "resp.rspSysHead.retMsg")

        return TestCaseResultDetail.accept()
    }

    companion object {
        private val logger = LoggerFactory.getLogger(ResultChecker::class.java)

        fun generateTestAccounts(n: Int): List<TestAccount> = (1..n).map {
            val no = (1_000_000 + it).toString()
            val name = "客户" + (((it - 1) % 8) + 'A'.code).toChar()

            TestAccount(name, no)
        }

        fun generateTransactions(txCount: Int, accountCount: Int): List<TransAccount> {
            val accounts = generateTestAccounts(accountCount)
            return (1..txCount).map {
                val (i, j) = ThreadLocalRandom.current().randomlyPickTwo(accountCount) // 随机选择两个不同的账号

                // 然后生成带两位小数的随机正数作为金额
                val amount = (ThreadLocalRandom.current()
                    .nextLong(1_000_000_00L) + 1) // 生成一个很大的正整数
                    .toBigDecimal()
                    .scaleByPowerOfTen(-2) // 然后除以100以产生两位小数

                TransAccount(
                    payer = accounts[i],
                    payee = accounts[j],
                    amount = amount
                )
            }
        }
    }

}