package com.njcb.game.games.ftp

import com.njcb.game.service.FileIOService
import com.njcb.game.service.QuestionsCheckService
import com.njcb.game.service.check.CheckTimeoutManager
import com.njcb.game.service.check.QuestionChecker
import com.njcb.game.service.check.RestCheckClient
import com.njcb.game.service.check.RestChecker
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration("ftpResultCheckerConfig")
class ResultCheckerConfig {

    @Bean("ftpResultChecker")
    fun staticChecker(
        checkService: QuestionsCheckService,
        testCaseManager: FtpTestCaseManager,
        checkClient: RestCheckClient,
        fileIO: FileIOService,
        timeoutManager: CheckTimeoutManager,
    ): QuestionChecker = RestChecker(
        checkService, testCaseManager, checkClient, fileIO, timeoutManager
    )
}
