package com.njcb.game.games.networth

import com.njcb.game.application.SimpleTestCaseManager
import com.njcb.game.pojo.SimpleTestCase
import com.njcb.game.util.BeanUtil
import com.njcb.game.util.randomlyPickTwo
import org.springframework.stereotype.Component
import java.time.LocalDate
import java.util.concurrent.ThreadLocalRandom


@Component
class NetWorthTestCaseManager(
    private val navManager: NAVManager,
) : SimpleTestCaseManager {
    override fun pickTestCases(questNo: String): List<SimpleTestCase> {
        // 生成10个测试样例
        return (1..10).map {
            val testCase = generateTestCase() // 生成输入
            val answer = testCase.calculateGain(navManager) // 计算答案

            // 转换为通用的输入输出格式
            SimpleTestCase(
                input = BeanUtil.toJSONMap(testCase),
                expect = BeanUtil.toJSONMap(mapOf("gain" to answer.toPlainString())),
                score = 10, // 每题10分
            )
        }
    }

    private fun generateTestCase(): TestCase {
        val rand = ThreadLocalRandom.current()
        val allProducts = navManager.allProducts

        // 生成15笔交易
        val purchases = (1..15).map {
            // 随机取一个产品代码
            val prod = allProducts.random()

            // 随机取两个日期
            val allDates = navManager.getAvailableDates(prod).toList()
            val (d1, d2) = rand.randomlyPickTwo(allDates.size)
            val dat1 = LocalDate.parse(allDates[d1], NAVManager.dateFormat)
            val dat2 = LocalDate.parse(allDates[d2], NAVManager.dateFormat)

            // 随机生成一笔 1w - 1000w 之间的金额
            val amount = rand.nextInt(1, 1000)
                .toBigDecimal()
                .scaleByPowerOfTen(4)

            Purchase(
                productCode = prod,
                amount = amount,
                buyDate = minOf(dat1, dat2),
                sellDate = maxOf(dat1, dat2),
            )
        }

        return TestCase(
            purchases = purchases,
        )
    }
}
