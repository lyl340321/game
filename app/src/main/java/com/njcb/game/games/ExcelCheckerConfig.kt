package com.njcb.game.games

import com.njcb.game.repository.dao.GameAttemptRepository
import com.njcb.game.service.check.ExcelChecker
import com.njcb.game.service.check.QuestionChecker
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class ExcelCheckerConfig {
    @Bean("excelChecker")
    fun excelChecker(
        gameAttemptRepository: GameAttemptRepository,
        @Value("\${game.working-dir}") baseWorkingDir: String,
    ): QuestionChecker =
        ExcelChecker(gameAttemptRepository, baseWorkingDir)
}