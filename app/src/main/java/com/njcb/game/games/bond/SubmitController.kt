package com.njcb.game.games.bond

import com.njcb.ams.pojo.dto.standard.EntityResponse
import com.njcb.game.service.FileUploadService
import com.njcb.game.service.QuestionsCheckService
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.multipart.MultipartFile
import kotlin.io.path.createDirectories
import kotlin.io.path.div

@RestController("BondSubmitController")
@RequestMapping("/bond")
class SubmitController(
    private val questionsCheckService: QuestionsCheckService,
    private val fileUploadService: FileUploadService,
) {

    @PostMapping(value = ["/", ""])
    fun uploadResult(
        @RequestParam("intermediate") intermediateFile: MultipartFile,
        @RequestParam("final") finalFile: MultipartFile,
        @RequestParam token: String
    ): EntityResponse<Any?> {
        fileUploadService.upload(token) { _, attempt, upload ->
            val workingDir = questionsCheckService.ensureWorkingDir(attempt) / upload.caseNo.toString()
            workingDir.createDirectories()

            intermediateFile.transferTo(workingDir / "intermediate.xlsx")
            finalFile.transferTo(workingDir / "final.xlsx")
        }
        return EntityResponse.buildSucc()
    }
}
