package com.njcb.game.games

import com.njcb.game.application.StaticTestCaseManager
import com.njcb.game.service.FileIOService
import com.njcb.game.service.QuestionsCheckService
import com.njcb.game.service.check.CheckTimeoutManager
import com.njcb.game.service.check.QuestionChecker
import com.njcb.game.service.check.RestCheckClient
import com.njcb.game.service.check.RestChecker
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class CommonCheckerConfig {

    /**
     * 静态测试样例检查器
     */
    @Bean("staticChecker")
    fun staticChecker(
        checkService: QuestionsCheckService,
        testCaseManager: StaticTestCaseManager,
        checkClient: RestCheckClient,
        fileIO: FileIOService,
        timeoutManager: CheckTimeoutManager,
    ): QuestionChecker = RestChecker(
        checkService, testCaseManager, checkClient, fileIO, timeoutManager
    )
}
