package com.njcb.game.games.batchtran

import com.dcitsbiz.esb.services._10001000001.*
import com.dcitsbiz.esb.services._10001000001.wsdl.ESBServerPortType
import com.njcb.ams.store.esbmodule.annotation.EsbService
import com.njcb.ams.support.exception.AppException
import com.njcb.ams.util.AmsBeanUtils
import com.njcb.game.repository.dao.GameTransAccountRepository
import com.njcb.game.service.FileIOService
import com.njcb.game.service.QuestionsCheckService
import com.njcb.game.service.readJson
import com.njcb.game.util.*
import org.apache.cxf.bus.spring.SpringBus
import org.apache.cxf.jaxws.EndpointImpl
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.transaction.PlatformTransactionManager
import javax.xml.ws.Endpoint
import kotlin.io.path.createDirectories
import kotlin.io.path.div

@EsbService
class S10001000001(
    private val transactionManager: PlatformTransactionManager,
    private val gameTransAccountRepository: GameTransAccountRepository,
    private val fileIO: FileIOService,
    private val questionsCheckService: QuestionsCheckService,
) : ESBServerPortType {

    override fun batchaccount(req1000100000102: Req1000100000102Type): Rsp1000100000102Type {
        fail("禁止访问")
    }

    override fun transaccount(req1000100000101: Req1000100000101Type): Rsp1000100000101Type {
        val globalSeq = assertNotNull(req1000100000101.reqSysHead.globalSeq, "reqSysHead.globalSeq 不能为空")
        // TODO: 校验globalSeq

        val ta = transactionManager.executeInNewTransaction {
            assertNotNull(gameTransAccountRepository.findByGlobalSeq(globalSeq)) { "错误的全局流水号 $globalSeq" }.also {
                // 更新访问次数
                it.invokeCount++
                gameTransAccountRepository.save(it)
            }
        }

        assertEqual(1, ta.invokeCount) { "记账接口只允许调用一次" }

        // 保存用户提交内容以供最后验证
        val workingDir = questionsCheckService.ensureWorkingDir(ta.runnersNo, ta.quesNo, ta.attempt) / ta.caseNo.toString()
        workingDir.createDirectories()
        fileIO.writeJson(workingDir / "TransAccount.req.${ta.invokeCount - 1}.json", req1000100000101)

        // 读取配置文件以生成返回结果
        val testCase: TestCase = fileIO.readJson(workingDir / "case.json")
        return when (testCase.type) {
            TestType.ALL_GOOD,
                // 不应该调用的，我们就当正常，最后判分的时候再处理
            TestType.INVALID_FILE -> Rsp1000100000101Type().apply {
                rspAppHead = RspAppHeadType().apply {
                    AmsBeanUtils.copyProperties(this, req1000100000101.reqAppHead)
                }
            }

            TestType.TRANS_ACCOUNT_ERROR ->
                throw object : AppException(testCase.expectedErrorMessage!!, testCase.expectedErrorCode!!) {}
        }
    }
}

@Configuration("GameServiceConfigBatchTran")
class GameServiceConfig(
    private val springBus: SpringBus,
    private val s10001000001: S10001000001,
) {
    @Bean
    fun s10001000001Endpoint(): Endpoint = EndpointImpl(springBus, s10001000001).apply {
        publish("/S10001000001")
    }
}
