package com.njcb.game.games.ftp

import com.njcb.game.application.SimpleTestCaseManager
import com.njcb.game.pojo.SimpleTestCase
import com.njcb.game.util.BeanUtil
import org.springframework.stereotype.Component
import java.util.concurrent.ThreadLocalRandom

@Component
class FtpTestCaseManager : SimpleTestCaseManager {
    override fun pickTestCases(questNo: String): List<SimpleTestCase> {
        // 生成10个测试样例
        return (1..10).map {
            val testCase = generateTestCase() // 生成输入
            val answer = testCase.calculateCustomerSpread() // 计算答案

            // 转换为通用的输入输出格式
            SimpleTestCase(
                input = BeanUtil.toJSONMap(testCase),
                expect = BeanUtil.toJSONMap(mapOf("spreads" to answer)),
                score = 10, // 每题10分
            )
        }
    }

    private fun generateTestCase(): TestCase {
        val rand = ThreadLocalRandom.current()
        // 所有的利率都保留一位小数的百分比，所以为了便于计算
        // 我们先直接生成10倍的正整数，最后再除以10
        // 生成FTP利率
        val ftpRate = rand.nextInt(25, 40) // 利率在 2.5 ~ 3.9% 之间

        // 生成4~8个存款产品
        val deposits = (1..rand.nextInt(4, 9)).map {
            // 存款利率比FTP利率低0.5~2个点
            val r = ftpRate - rand.nextInt(5, 21)
            Product(
                code = "CK00$it",
                yearlyRate = r.toBigDecimal().scaleByPowerOfTen(-3),
            )
        }
        // 生成4~8个贷款产品
        val loans = (1..rand.nextInt(4, 9)).map {
            // 贷款利率比FTP利率高0.5~2个点
            val r = ftpRate + rand.nextInt(5, 21)
            Product(
                code = "DK00$it",
                yearlyRate = r.toBigDecimal().scaleByPowerOfTen(-3),
            )
        }

        // 然后生成3-10个客户
        val purchases = (1..rand.nextInt(3, 11)).flatMap { custNo ->
            // 对每个客户，随机生成1-10笔购买
            (1..rand.nextInt(1, 11)).map {
                // 50%几率贷款或存款
                val prods = if (rand.nextBoolean()) {
                    deposits
                } else {
                    loans
                }

                // 随机选择一个产品
                val prod = prods.random().code

                // 随机产生一笔 10~9999990 之间的金额
                // 由于利率都是一位小数的百分比，而最终金额结果保留两位小数，
                // 为了避免舍入的误差，我们产品购买的金额都取10的倍数
                val amount = rand
                    .nextInt(1, 100_000)
                    .toBigDecimal()
                    .scaleByPowerOfTen(1)

                Purchase(
                    customerNo = (10000 + custNo).toString(),
                    productCode = prod,
                    amount = amount,
                )
            }
        }.shuffled() // 打乱顺序

        return TestCase(
            ftpRate = ftpRate.toBigDecimal().scaleByPowerOfTen(-3),
            depositProducts = deposits,
            loanProducts = loans,
            purchases = purchases,
        )
    }
}