package com.njcb.game.games.crawler

import com.njcb.game.repository.entity.GameAttemptEntity
import com.njcb.game.repository.entity.GameQuestionEntity
import com.njcb.game.repository.entity.GameRegisterEntity
import com.njcb.game.service.check.CheckResult
import com.njcb.game.service.check.QuestionChecker
import org.springframework.stereotype.Component

@Component("crawlerResultChecker")
class ResultChecker(
    private val testManager: TestManager,
) : QuestionChecker {
    override fun check(parti: GameRegisterEntity, question: GameQuestionEntity, attempt: GameAttemptEntity): CheckResult = testManager.check(parti, question, attempt)
}
