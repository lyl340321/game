package com.njcb.game.games.batchtran

import com.dcitsbiz.esb.services._10001000001.Req1000100000101Type


/**
 * 考点类型
 */
enum class TestType {
    /** 一切正常 */
    ALL_GOOD,

    /**
     * 批量并账文件错误，包括但不限于：
     * - 格式不正确
     * - 自己给自己转账
     * - 转账金额为负
     */
    INVALID_FILE,

    /** 选手调用普通记账接口失败 */
    TRANS_ACCOUNT_ERROR,
}

data class TestCase(
    val type: TestType,

    /**
     * 当 type = TRANS_ACCOUNT_ERROR 时，这里是当时选手调用记账接口时我们返回的错误码，
     * 最后我们要验证这个错误码有没有透传
     */
    val expectedErrorCode: String? = null,
    /**
     * 当 type = TRANS_ACCOUNT_ERROR 时，这里是当时选手调用记账接口时我们返回的错误信息，
     * 最后我们要验证这个错误信息有没有透传
     */
    val expectedErrorMessage: String? = null,

    val expectedTransAccounts: List<Req1000100000101Type.ReqAppBody.Details>? = null,
)
