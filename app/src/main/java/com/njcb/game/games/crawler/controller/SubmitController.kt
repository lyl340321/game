package com.njcb.game.games.crawler.controller

import com.njcb.ams.pojo.dto.standard.EntityResponse
import com.njcb.game.games.crawler.TestManager
import io.swagger.annotations.ApiOperation
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.multipart.MultipartFile

@RestController("CrawlerSubmitController")
@RequestMapping("/crawler")
class SubmitController(
    private val testManager: TestManager,
) {

    @ApiOperation(value = "开始抓取", notes = "开始抓取数据")
    @PostMapping(value = ["/start"])
    fun startCrawling(@RequestParam token: String): EntityResponse<Any?> {
        testManager.prepareTest(token)
        return EntityResponse.buildSucc()
    }

    @ApiOperation(value = "提交抓取的数据", notes = "提交抓取的数据")
    @PostMapping(value = ["/upload"])
    fun uploadResult(@RequestParam("file") file: MultipartFile, @RequestParam token: String): EntityResponse<Any?> {
        testManager.uploadResult(token, file)
        return EntityResponse.buildSucc()
    }

}