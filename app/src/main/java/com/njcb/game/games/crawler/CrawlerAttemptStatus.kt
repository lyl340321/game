package com.njcb.game.games.crawler

import com.njcb.ams.support.annotation.DataDic
import com.njcb.ams.support.codevalue.EnumCode

@DataDic(dataType = "CrawlerAttemptStatus", dataTypeName = "爬虫提交状态")
enum class CrawlerAttemptStatus(
    private val desc: String,
) : EnumCode {
    INIT("初始化测试样例中"),
    WAITING("初始化完成，等待用户提交答案"),
    UPLOADED("答案已提交，等待评分"),
    CANCELLED("已放弃"),
    COMPLETED("评分完成"),
    ;

    override fun getCode(): String = name

    override fun getDesc(): String = desc
}
