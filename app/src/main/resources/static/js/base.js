var pathName = window.document.location.pathname;
var projectName = pathName.substring(0, pathName.substr(1).indexOf("/") + 1);

function fnW(str) {
    var num;
    str >= 10 ? num = str : num = "0" + str;
    return num;
}

//实时得分榜单
var game_billboard =echarts.init(document.getElementById("game_billboard"),'infographic');

var billboardOption = {
    title: {
        text: '实时榜单 前20名'
    },
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'shadow'
        }
    },
    legend: {},
    grid: {
        left: '0%',
        right: '0%',
        bottom: '5%',
        containLabel: true
    },
    xAxis: {
        type: 'value',
        boundaryGap: [0, 0.01],
        axisLabel: {
            show: true,
            textStyle: {
                color: '#ffd639',
                fontSize: 12
            }
        },
    },
    yAxis: {
        type: 'category',
        inverse: true,
        data: ['未开始'],
        axisLabel: {
            interval: 0,
            color: '#D8D9D9',
            margin: 90,
            formatter: (params, index) => {
                if (index < 9) {
                    return [`{a${index + 1}|  ${index + 1}}    ${params}`].join('\n')
                }else{
                    return [`{a${index + 1}|${index + 1}}    ${params}`].join('\n')
                }
            },
            align: 'left', // 文字左排序
            rich: {
                a1: {
                    color: '#fff',
                    backgroundColor: '#ff4a45',
                    align: 'center',
                    borderRadius: 4,
                },
                a2: {
                    color: '#fff',
                    backgroundColor: '#ffa139',
                    align: 'center',
                    borderRadius: 4,
                },
                a3: {
                    color: '#fff',
                    backgroundColor: '#deff23',
                    align: 'center',
                    borderRadius: 4,
                },
                a4: {
                    color: '#fff',
                    backgroundColor: '#ffd639',
                    align: 'center',
                    borderRadius: 4,
                },
                a5: {
                    color: '#fff',
                    backgroundColor: '#ffd639',
                    align: 'center',
                    borderRadius: 4,
                },
                a6: {
                    color: '#fff',
                    backgroundColor: '#a5a5a5',
                    align: 'center',
                    borderRadius: 4,
                },
                a7: {
                    color: '#fff',
                    backgroundColor: '#a5a5a5',
                    align: 'center',
                    borderRadius: 4,
                },
                a8: {
                    color: '#fff',
                    backgroundColor: '#a5a5a5',
                    align: 'center',
                    borderRadius: 4,
                },
                a9: {
                    color: '#fff',
                    backgroundColor: '#a5a5a5',
                    align: 'center',
                    borderRadius: 4,
                },
                a10: {
                    color: '#fff',
                    backgroundColor: '#a5a5a5',
                    align: 'center',
                    borderRadius: 4,
                },
                title: {
                    color: "#fff",
                    width: 15,
                    align: "left"
                }
            }
        }
    },
    series: [
        {
            name: '得分',
            type: 'bar',
            data: [100, 100, 100, 100, 100, 100, 100, 100]
        }
    ]
};
game_billboard.setOption(billboardOption);

//参赛人员分布
var runnerDistribution =echarts.init(document.getElementById("runnerDistribution"),'infographic');
var runnerOption = {
    title : {
        x:'center'
    },
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    legend: {
        orient: 'vertical',
        left: 'left',
        data: ['未开始'],
        textStyle: {color: '#fff'}
    },
    
	label: {
	     normal: {
	          textStyle: {
	                color: 'red'  // 改变标示文字的颜色
	          }
	     }
	},
    series : [
        {
            name: '参赛人员分布',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:300, name:'未开始'}
            ],
          
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            }
        }
    ]
};
runnerDistribution.setOption(runnerOption);


//===================完成率=======================
var completionRate =echarts.init(document.getElementById("completionRate"),'macarons');
var completionRateOption = {
    series: [
        {
            type: 'gauge',
            progress: {
                show: true,
                width: 10
            },
            axisLine: {
                lineStyle: {
                    width: 10
                }
            },
            axisTick: {
                show: false
            },
            splitLine: {
                length: 10,
                lineStyle: {
                    width: 2,
                    color: '#999'
                }
            },
            axisLabel: {
                distance: 15,
                color: '#999',
                fontSize: 10
            },
            anchor: {
                show: true,
                showAbove: true,
                size: 25,
                itemStyle: {
                    borderWidth: 10
                }
            },
            title: {
                show: false
            },
            detail: {
                valueAnimation: true,
                fontSize: 30,
                offsetCenter: [0, '80%']
            },
            data: [
                {
                    value: 80
                }
            ]
        }
    ]
};

completionRate.setOption(completionRateOption);


//=========得分率=======================
var questionScoreRate =echarts.init(document.getElementById("questionScoreRate"),'shine');
var questionScoreRateOption = {
    color: ['#FADB71'],
    tooltip : {
        trigger: 'axis',
        axisPointer : {            // 坐标轴指示器，坐标轴触发有效
            type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
        }
    },
    grid: {
        x:30,
        y:10,
        x2:15,
        y2:20
    },
    xAxis : [
        {
            type : 'category',
            data : ['未开始'],
            axisTick: {
                alignWithLabel: true
            },
            axisLabel: {
			    color: "#FADB71" //刻度线标签颜色
			}
        }
    ],
    yAxis : [
        {
            type : 'value',
            axisLabel: {
			    color: "#FADB71" //刻度线标签颜色
			}
        }
    ],
    series : [
        {
            name:'得分率',
            type:'bar',
            barWidth: '60%',
            data:[100, 100, 100]
        }
    ]
};
questionScoreRate.setOption(questionScoreRateOption);


//获取当前时间
var timer = setInterval(function () {
    var date = new Date();
    var year = date.getFullYear(); //当前年份
    var month = date.getMonth(); //当前月份
    var data = date.getDate(); //天
    var hours = date.getHours(); //小时
    var minute = date.getMinutes(); //分
    var second = date.getSeconds(); //秒
    var day = date.getDay(); //获取当前星期几
    var amPm = hours < 12 ? 'am' : 'pm';
    $('#time').html(fnW(hours) + ":" + fnW(minute) + ":" + fnW(second));
    $('#date').html('<span>' + year + '/' + (month + 1) + '/' + data + '</span><span>' + amPm + '</span><span>周' + day + '</span>')
}, 1000)

//3s请求刷新一次数据
window.setInterval(function () {
    refreshValueData();
    refreshChartsData();
},3000);


function refreshChartsData(){
    $.ajax({
        url:projectName+"/billboard/charts",
        data:{},
        type:"get",
        dataType: 'json',
        crossDomain: true,
        success:function (data) {
            if (!data.success) {
                alert(data.message);
                return;
            }
            //实时榜单
            var realListOption = game_billboard.getOption();
            var yAxisTmp = billboardOption.yAxis;
            yAxisTmp.data = data.entity.realRankingList.name;
            realListOption.yAxis = yAxisTmp;
            realListOption.series[0].data = data.entity.realRankingList.value;
            game_billboard.setOption(realListOption,true);

            //参赛人员分布
            var runnerDistributionOption = runnerDistribution.getOption();
            var legend = runnerOption.legend;
            legend.data = data.entity.runnerDistribution.title;
            runnerDistributionOption.legend = legend;
            runnerDistributionOption.series[0].data = data.entity.runnerDistribution.data;
            runnerDistribution.setOption(runnerDistributionOption,true);

            //题目得分率
            var questionScoreOption = questionScoreRate.getOption();
            questionScoreOption.xAxis[0].data = data.entity.questionScoreRate.title;
            questionScoreOption.series[0].data = data.entity.questionScoreRate.data;
            questionScoreRate.setOption(questionScoreOption,true);

        },
        error:function () {
        }
    });
}


function refreshValueData(){
    $.ajax({
        url:projectName+"/billboard/value",
        data:{},
        type:"get",
        dataType: 'json',
        crossDomain: true,
        success:function (data) {
            if (!data.success) {
                alert(data.message);
                return;
            }
            var ranking_one = document.getElementById("ranking_one");
            var ranking_two = document.getElementById("ranking_two");
            var ranking_three = document.getElementById("ranking_three");
            var judgeList = document.getElementById("judge");
            ranking_one.innerHTML = data.entity.one;
            ranking_two.innerHTML = data.entity.two;
            ranking_three.innerHTML = data.entity.three;
            judgeList.innerHTML = data.entity.judgeList;

            var teamNumber = document.getElementById("teamNumber");
            var remainingTime = document.getElementById("remainingTime");
            var ranking_three = document.getElementById("ranking_three");
            teamNumber.innerText = data.entity.teamNumber;
            remainingTime.innerText = data.entity.remainingTime;

            var completionRateOption = completionRate.getOption();
            completionRateOption.series[0].data[0].value = data.entity.completionRate;
            completionRate.setOption(completionRateOption,true);
        },
        error:function () {
        }
    });
}