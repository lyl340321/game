
drop table if exists GAME_REGISTER;
drop table if exists GAME_QUESTION;
drop table if exists GAME_ATTEMPT;
drop table if exists GAME_SCORE;
drop table if exists GAME_TRANS_ACCOUNT;
drop table if exists GAME_DOWNLOAD;

-- Create table
create table GAME_REGISTER
(
    ID            INTEGER primary key auto_increment,
    RUNNERS_NO    VARCHAR(20) not null,
    RUNNERS_NAME  VARCHAR(200) not null,
    SERVER_ADDR   VARCHAR(200) not null,
    DEPARTMENT    VARCHAR(200),
    REGISTER_TIME CHAR(14),
    TOKEN         VARCHAR(200),
    CONDUCT_USER  INTEGER,
    CONDUCT_TIME  CHAR(14)
);
alter table GAME_REGISTER
    add unique (RUNNERS_NO);
alter table GAME_REGISTER
    add unique (TOKEN);


-- CrEATE TABLE
CREATE TABLE GAME_QUESTION
(
    ID            INTEGER PRIMARY key auto_increment comment '主键',
    QUES_NO       VARCHAR(20) NOT NULL comment '题目编号',
    QUES_TITLE    VARCHAR(200) NOT NULL comment '标题',
    QUES_CONTEXT  VARCHAR(200) NOT NULL comment '简介',
    ENABLED       INTEGER DEFAULT 1 NOT NULL comment '是否启用',
    CHECK_TYPE    VARCHAR(20) NOT NULL comment '评分模式 SQL/CLASS',
    CHECK_SQL     VARCHAR(200) comment '评分用SQL',
    CHECK_CLASS   VARCHAR(200) comment '评分用class的FQ名，或者@bean名称',
    CHECK_TIMEOUT INTEGER NOT NULL comment '评分超时秒数',
    SCORE         INTEGER NOT NULL comment '分数',
    ALLOW_SUBMIT  INTEGER NOT NULL comment '允许用户直接提交',
    CONDUCT_USER  INTEGER,
    CONDUCT_TIME  CHAR(14)
);
alter table GAME_QUESTION
    add unique (QUES_NO);

INSERT INTO GAME_QUESTION
(QUES_NO, QUES_TITLE, QUES_CONTEXT, CHECK_TYPE, CHECK_SQL, CHECK_CLASS, CHECK_TIMEOUT, SCORE, ALLOW_SUBMIT)
VALUES('001', '注册TOKEN', '用户调用赛事服务器的用户报道API获取TOKEN', 'SQL', 'select  id
  from game_register r
 where r.token is not null
   and r.runners_no = ${runnersNo}
', NULL, 5, 20, 0);

INSERT INTO GAME_QUESTION
(QUES_NO, QUES_TITLE, QUES_CONTEXT, CHECK_TYPE, CHECK_SQL, CHECK_CLASS, CHECK_TIMEOUT, SCORE, ALLOW_SUBMIT)
VALUES('002', '大数阶乘', '计算大数的阶乘', 'CLASS', NULL, '@staticChecker', 5, 10, 1);

INSERT INTO GAME_QUESTION
(QUES_NO, QUES_TITLE, QUES_CONTEXT, CHECK_TYPE, CHECK_SQL, CHECK_CLASS, CHECK_TIMEOUT, SCORE, ALLOW_SUBMIT)
VALUES('003', '批量并账处理', '批量并账处理', 'CLASS', NULL, '@batchTranResultChecker', 20, 80, 1);

INSERT INTO GAME_QUESTION
(QUES_NO, QUES_TITLE, QUES_CONTEXT, CHECK_TYPE, CHECK_SQL, CHECK_CLASS, CHECK_TIMEOUT, SCORE, ALLOW_SUBMIT)
VALUES('004', 'FTP利差', 'FTP利差', 'CLASS', NULL, '@ftpResultChecker', 10, 10, 1);

INSERT INTO GAME_QUESTION
(QUES_NO, QUES_TITLE, QUES_CONTEXT, CHECK_TYPE, CHECK_SQL, CHECK_CLASS, CHECK_TIMEOUT, SCORE, ALLOW_SUBMIT)
VALUES('005', '投资收益', '投资收益', 'CLASS', NULL, '@navResultChecker', 10, 10, 1);

INSERT INTO GAME_QUESTION
(QUES_NO, QUES_TITLE, QUES_CONTEXT, CHECK_TYPE, CHECK_SQL, CHECK_CLASS, CHECK_TIMEOUT, SCORE, ALLOW_SUBMIT)
VALUES('006', '爬虫', '爬虫', 'CLASS', NULL, '@crawlerResultChecker', 300, 20, 0);

INSERT INTO GAME_QUESTION
(QUES_NO, QUES_TITLE, QUES_CONTEXT, CHECK_TYPE, CHECK_SQL, CHECK_CLASS, CHECK_TIMEOUT, SCORE, ALLOW_SUBMIT)
VALUES('007', '交易汇总', '交易汇总', 'CLASS', NULL, '@bondResultChecker', 30, 20, 1);

-- CrEATE TABLE
CREATE TABLE GAME_ATTEMPT
(
    ID           INTEGER PRIMARY key auto_increment comment '主键',
    RUNNERS_NO   VARCHAR(20) NOT NULL comment '提交者工号',
    QUES_NO      VARCHAR(20) NOT NULL comment '题目编号',
    SUBMIT_TIME  TIMESTAMP NOT NULL comment '提交时间戳',
    CHECK_STATUS VARCHAR(20) NOT NULL comment '评分状态',
    CHECK_RESULT VARCHAR(200) comment '评分结果信息',
    SCORE        INTEGER comment '分数',
    SCORE_TIME   TIMESTAMP comment '评分时间',
    CONDUCT_USER INTEGER,
    CONDUCT_TIME CHAR(14)
);


-- Create table
create table GAME_SCORE
(
    ID            INTEGER PRIMARY key auto_increment comment '主键',
    RUNNERS_NO    VARCHAR(20) NOT NULL comment '选手工号',
    QUES_NO       VARCHAR(20) NOT NULL comment '题目编号',
    RUNNERS_NAME  VARCHAR(200) NOT NULL comment '选手名字',
    QUES_TITLE    VARCHAR(200) NOT NULL comment '题目标题',
    SCORE         INTEGER comment '分数',
    SCORE_ATTEMPT INTEGER comment '当前分数的提交编号',
    SCORE_TIME    TIMESTAMP comment '评分时间',
    CONDUCT_USER  INTEGER,
    CONDUCT_TIME  CHAR(14)
);

alter table GAME_SCORE
    add unique (RUNNERS_NO, QUES_NO);


create table GAME_TRANS_ACCOUNT
(
    ID            INTEGER PRIMARY key auto_increment comment '主键',
    RUNNERS_NO    VARCHAR(20) NOT NULL comment '选手工号',
    QUES_NO       VARCHAR(20) NOT NULL comment '题目编号',
    ATTEMPT       INTEGER NOT NULL comment '提交编号',
    CASE_NO       INTEGER NOT NULL comment '测试样例编号',
    GLOBAL_SEQ    VARCHAR(24) NOT NULL comment '全局流水号',
    INVOKE_COUNT  INTEGER default 0 NOT NULL comment '接口调用次数',
    CONDUCT_USER  INTEGER,
    CONDUCT_TIME  CHAR(14)
) COMMENT = '通用记账接口调用记录';

alter table GAME_TRANS_ACCOUNT
    add unique (ATTEMPT, CASE_NO);

alter table GAME_TRANS_ACCOUNT
    add unique (GLOBAL_SEQ);

create table GAME_DOWNLOAD
(
    ID            INTEGER PRIMARY key auto_increment comment '主键',
    RUNNERS_NO    VARCHAR(20) comment '选手工号, NULL的话所有人都能下载',
    FILE_NAME     VARCHAR(100) NOT NULL comment '文件名',
    FILE_PATH     VARCHAR(500) NOT NULL comment '文件路径',
    CONDUCT_USER  INTEGER,
    CONDUCT_TIME  CHAR(14)
) COMMENT = '文件下载记录';

insert into sys_param (NAME, VALUE, TYPE, SUBTYPE, STATUS, REMARK, CONDUCT_USER, CONDUCT_TIME)
values ('context-url', 'http://localhost:8888/contest', 'GAME', '1', '1', '比赛服务器基准URL', 1, '20230306000000');


create table GAME_UPLOAD
(
    ID            INTEGER PRIMARY key auto_increment comment '主键',
    CHECK_ATTEMPT INTEGER NOT NULL comment '评分编号',
    CASE_NO       INTEGER NOT NULL comment '测试样例编号',
    STATUS        VARCHAR(20) NOT NULL comment '上传状态',
    UPLOAD_TIME   TIMESTAMP comment '上传时间戳',
    CONDUCT_USER  INTEGER,
    CONDUCT_TIME  CHAR(14)
) COMMENT = '文件上传记录';


-- 爬虫题目用表
create table game_crawler_attempt
(
    ID            INTEGER PRIMARY key auto_increment comment '主键',
    RUNNERS_NO    VARCHAR(20) NOT NULL comment '选手工号',
    SUBMIT_TIME   TIMESTAMP NOT NULL comment '提交时间戳',
    STATUS        VARCHAR(20) NOT NULL comment '提交状态',
    CHECK_ATTEMPT INTEGER comment '评分编号',
    CONDUCT_USER  INTEGER,
    CONDUCT_TIME  CHAR(14)
) comment = '爬虫提交表';
