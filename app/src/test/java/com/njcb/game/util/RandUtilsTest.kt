package com.njcb.game.util

import java.util.concurrent.ThreadLocalRandom
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals

class RandUtilsTest {

    @Test
    fun `test randomlyPickTwo`() {
        val results = mutableSetOf<Int>()
        repeat(500) {
            val (i, j) = ThreadLocalRandom.current().randomlyPickTwo(5)
            assertNotEquals(i, j)
            results.add(i)
            results.add(j)
        }

        assertEquals(setOf(0, 1, 2, 3, 4), results)
    }
}