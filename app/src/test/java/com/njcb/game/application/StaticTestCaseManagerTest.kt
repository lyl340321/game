package com.njcb.game.application

import com.njcb.game.service.FileIOService
import kotlin.test.Test

class StaticTestCaseManagerTest {
    private val manager = StaticTestCaseManager(
        "${System.getProperty("user.home")}/njcb_games",
        FileIOService()
    )

    @Test
    fun test() {
        manager.pickTestCases("002")
    }
}