package com.njcb.game.service.games.batchtran

import com.njcb.game.games.batchtran.ResultChecker
import com.njcb.game.games.batchtran.TestAccount
import kotlin.test.Test
import kotlin.test.assertContentEquals
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals

class ResultCheckerTest {
    @Test
    fun `test generateTestAccounts`() {
        assertContentEquals(
            listOf(
                TestAccount("客户A", "1000001"),
                TestAccount("客户B", "1000002"),
                TestAccount("客户C", "1000003"),
                TestAccount("客户D", "1000004"),
                TestAccount("客户E", "1000005"),
                TestAccount("客户F", "1000006"),
                TestAccount("客户G", "1000007"),
                TestAccount("客户H", "1000008"),
                TestAccount("客户A", "1000009"),
                TestAccount("客户B", "1000010"),
                TestAccount("客户C", "1000011"),
                TestAccount("客户D", "1000012"),
                TestAccount("客户E", "1000013"),
                TestAccount("客户F", "1000014"),
                TestAccount("客户G", "1000015"),
                TestAccount("客户H", "1000016"),
                TestAccount("客户A", "1000017"),
                TestAccount("客户B", "1000018"),
                TestAccount("客户C", "1000019"),
                TestAccount("客户D", "1000020"),
                TestAccount("客户E", "1000021"),
                TestAccount("客户F", "1000022"),
                TestAccount("客户G", "1000023"),
                TestAccount("客户H", "1000024"),
                TestAccount("客户A", "1000025"),
                TestAccount("客户B", "1000026"),
                TestAccount("客户C", "1000027"),
            ),
            ResultChecker.generateTestAccounts(27)
        )
    }

    @Test
    fun `test generateTransactions`() {
        val txs = ResultChecker.generateTransactions(50, 10)
        txs.forEach {
            assertNotEquals(it.payee, it.payer)
            assertEquals(2, it.amount.scale())
            assertEquals(1, it.amount.signum())
        }
    }
}
