package com.njcb.main;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Main {
    public static void main(String[] args) throws Exception {
        Cat cat1 = new Cat("zhangsan", 10);
        Cat cat2 = new Cat("lisi", 20);

        List one = new ArrayList<>();
        one.add(cat1);
        one.add(cat2);

        Cat cat3 = new Cat("lisi", 20);
        Cat cat4 = new Cat("zhangsan", 10);
        List two = new ArrayList<>();
        two.add(cat3);
        two.add(cat4);

        System.out.println(one.containsAll(two) && two.containsAll(one));

    }


    public static class Cat {
        private String name;
        private Integer age;

        public Cat(String name, Integer age) {
            this.name = name;
            this.age = age;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getAge() {
            return age;
        }

        public void setAge(Integer age) {
            this.age = age;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Cat cat = (Cat) o;
            return Objects.equals(name, cat.name) && Objects.equals(age, cat.age);
        }

        @Override
        public int hashCode() {
            return Objects.hash(name, age);
        }

        @Override
        public String toString() {
            return "Cat{" +
                    "name='" + name + '\'' +
                    ", age=" + age +
                    '}';
        }
    }
}

