#基础镜像，如果本地没有，会从远程仓库拉取。
FROM openjdk:8
#镜像的制作人
MAINTAINER liuyanlong/23562656@qq.com

ENV appname ams-demo.jar
ENV keyword DEMO
ENV active dev

RUN mkdir -p /home/app/
#拷贝本地文件到镜像中
COPY ams-sso.jar /home/app/ams-demo.jar
#工作目录
WORKDIR /home/app/
#声明了容器应该打开的端口并没有实际上将它打开
EXPOSE 8123
#指定容器启动时要执行的命令，但如果存在CMD指令，CMD中的参数会被附加到ENTRYPOINT指令的后面
ENTRYPOINT ["nohup","java","-jar","ams-demo.jar","&"]