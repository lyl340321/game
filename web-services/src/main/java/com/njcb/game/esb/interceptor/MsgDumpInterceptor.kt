package com.njcb.game.esb.interceptor

import com.njcb.game.esb.EsbUtils.getSinglePayload
import com.njcb.game.service.FileIOService
import org.apache.cxf.binding.soap.SoapMessage
import org.apache.cxf.phase.AbstractPhaseInterceptor
import org.slf4j.LoggerFactory
import java.nio.file.Path
import java.util.concurrent.atomic.AtomicInteger
import kotlin.io.path.div

/**
 * 将SOAP输入输出消息导出保存到指定JSON文件
 */
class MsgDumpInterceptor(
    private val path: Path,
    private val prefix: String,
    private val fileIO: FileIOService,
    phase: String,
) : AbstractPhaseInterceptor<SoapMessage>(phase) {

    private val seq = AtomicInteger(0)

    override fun handleMessage(message: SoapMessage) {
        val payload = message.getSinglePayload<Any>()
        if (payload == null) {
            logger.error("Unable to get the payload for message {}", message)
        } else {
            val fileName = "$prefix.${seq.getAndIncrement()}.json"
            fileIO.writeJson(
                path / fileName,
                payload
            )
        }
    }

    companion object {
        private val logger = LoggerFactory.getLogger(MsgDumpInterceptor::class.java)
    }
}
