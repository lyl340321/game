package com.njcb.game.esb

import com.njcb.ams.factory.domain.AppContext
import com.njcb.ams.store.esbmodule.interceptor.ReqInterceptor
import com.njcb.ams.store.esbmodule.interceptor.RspInterceptor
import com.njcb.game.esb.interceptor.MsgDumpInterceptor
import com.njcb.game.service.FileIOService
import org.apache.cxf.binding.soap.SoapMessage
import org.apache.cxf.configuration.jsse.TLSClientParameters
import org.apache.cxf.frontend.ClientProxy
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean
import org.apache.cxf.phase.Phase
import org.apache.cxf.service.Service
import org.apache.cxf.service.invoker.MethodDispatcher
import org.apache.cxf.service.model.BindingOperationInfo
import org.apache.cxf.transport.http.HTTPConduit
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy
import java.nio.file.Path

object EsbUtils {

    /**
     * 没有使用 com.njcb.ams.store.esbmodule.WebServiceClient，因为对同一个url我们创建的client会带上不同
     * 的interceptor，因此不能复用。而WebServiceClient会用缓存。
     */
    inline fun <reified T> esbClient(endpoint: String): T {
        val pf = JaxWsProxyFactoryBean().apply {
            serviceClass = T::class.java
            address = endpoint

            outInterceptors.add(AppContext.getBean(ReqInterceptor::class.java))
            inInterceptors.add(AppContext.getBean(RspInterceptor::class.java))
        }

        val port = pf.create(T::class.java)


        val httpConduit = ClientProxy.getClient(port).conduit as HTTPConduit
        // 设置超时
        httpConduit.client = HTTPClientPolicy().apply {
            connectionTimeout = 60_000L
            receiveTimeout = 60_000L
        }

        // 禁用 HTTPS CN 检查
        httpConduit.tlsClientParameters = TLSClientParameters().apply {
            isDisableCNCheck = true
        }

        return port
    }

    /**
     * 将SOAP输入输出消息导出保存到指定JSON文件
     */
    fun dumpMessagesTo(client: Any, path: Path, prefix: String) {
        val fileIO = AppContext.getBean(FileIOService::class.java)
        ClientProxy.getClient(client).apply {
            outInterceptors.add(MsgDumpInterceptor(path, "$prefix.req", fileIO, Phase.PRE_STREAM))
            inInterceptors.add(MsgDumpInterceptor(path, "$prefix.resp", fileIO, Phase.POST_LOGICAL))
        }
    }

    inline fun <reified T> SoapMessage.getSinglePayload(): T? {
        val bp = exchange.get(BindingOperationInfo::class.java)
        val service = exchange.get(Service::class.java)
        val md = service[MethodDispatcher::class.java.name] as MethodDispatcher
        val method = md.getMethod(bp)
        val payloads = this.getContent(List::class.java)
        val parameterTypes = method.parameterTypes
        if (parameterTypes.size == 1) {
            return payloads[0] as T
        }

        return null
    }
}
