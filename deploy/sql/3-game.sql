
drop table if exists GAME_REGISTER;
drop table if exists GAME_QUESTION;
drop table if exists GAME_ATTEMPT;
drop table if exists GAME_SCORE;
drop table if exists GAME_TRANS_ACCOUNT;
drop table if exists GAME_DOWNLOAD;

-- Create table
create table GAME_REGISTER
(
    ID            INTEGER primary key auto_increment,
    RUNNERS_NO    VARCHAR(20) not null,
    RUNNERS_NAME  VARCHAR(200) not null,
    SERVER_ADDR   VARCHAR(200) not null,
    DEPARTMENT    VARCHAR(200),
    REGISTER_TIME CHAR(14),
    TOKEN         VARCHAR(200),
    CONDUCT_USER  INTEGER,
    CONDUCT_TIME  CHAR(14)
);
alter table GAME_REGISTER
    add unique (RUNNERS_NO);
alter table GAME_REGISTER
    add unique (TOKEN);


-- CrEATE TABLE
CREATE TABLE GAME_QUESTION
(
    ID            INTEGER PRIMARY key auto_increment comment '主键',
    QUES_NO       VARCHAR(20) NOT NULL comment '题目编号',
    QUES_TITLE    VARCHAR(200) NOT NULL comment '标题',
    QUES_CONTEXT  VARCHAR(200) NOT NULL comment '简介',
    ENABLED       INTEGER DEFAULT 1 NOT NULL comment '是否启用',
    CHECK_TYPE    VARCHAR(20) NOT NULL comment '评分模式 SQL/CLASS',
    CHECK_SQL     VARCHAR(200) comment '评分用SQL',
    CHECK_CLASS   VARCHAR(200) comment '评分用class的FQ名，或者@bean名称',
    CHECK_TIMEOUT INTEGER NOT NULL comment '评分超时秒数',
    SCORE         INTEGER NOT NULL comment '分数',
    ALLOW_SUBMIT  INTEGER NOT NULL comment '允许用户直接提交',
    CONDUCT_USER  INTEGER,
    CONDUCT_TIME  CHAR(14)
);
alter table GAME_QUESTION
    add unique (QUES_NO);

INSERT INTO GAME_QUESTION
(QUES_NO, QUES_TITLE, QUES_CONTEXT, ENABLED, CHECK_TYPE, CHECK_SQL, CHECK_CLASS, CHECK_TIMEOUT, SCORE, ALLOW_SUBMIT)
VALUES('001', '注册TOKEN', '用户调用赛事服务器的用户报道API获取TOKEN', 1,'SQL', 'select  id
  from game_register r
 where r.token is not null
   and r.runners_no = ${runnersNo}
', NULL, 5, 20, 0);

INSERT INTO GAME_QUESTION
(QUES_NO, QUES_TITLE, QUES_CONTEXT, CHECK_TYPE, CHECK_SQL, CHECK_CLASS, CHECK_TIMEOUT, SCORE, ALLOW_SUBMIT)
VALUES('002', '大数阶乘', '计算大数的阶乘', 'CLASS', NULL, '@staticChecker', 5, 10, 1);

INSERT INTO GAME_QUESTION
(QUES_NO, QUES_TITLE, QUES_CONTEXT, CHECK_TYPE, CHECK_SQL, CHECK_CLASS, CHECK_TIMEOUT, SCORE, ALLOW_SUBMIT)
VALUES('003', '批量并账处理', '批量并账处理', 'CLASS', NULL, '@batchTranResultChecker', 20, 80, 1);

INSERT INTO GAME_QUESTION
(QUES_NO, QUES_TITLE, QUES_CONTEXT, CHECK_TYPE, CHECK_SQL, CHECK_CLASS, CHECK_TIMEOUT, SCORE, ALLOW_SUBMIT)
VALUES('004', 'FTP利差', 'FTP利差', 'CLASS', NULL, '@ftpResultChecker', 10, 10, 1);

INSERT INTO GAME_QUESTION
(QUES_NO, QUES_TITLE, QUES_CONTEXT, CHECK_TYPE, CHECK_SQL, CHECK_CLASS, CHECK_TIMEOUT, SCORE, ALLOW_SUBMIT)
VALUES('005', '投资收益', '投资收益', 'CLASS', NULL, '@navResultChecker', 10, 10, 1);

INSERT INTO GAME_QUESTION
(QUES_NO, QUES_TITLE, QUES_CONTEXT, CHECK_TYPE, CHECK_SQL, CHECK_CLASS, CHECK_TIMEOUT, SCORE, ALLOW_SUBMIT)
VALUES('006', '爬虫', '爬虫', 'CLASS', NULL, '@crawlerResultChecker', 300, 20, 0);


INSERT INTO GAME_QUESTION
(QUES_NO, QUES_TITLE, QUES_CONTEXT, CHECK_TYPE, CHECK_SQL, CHECK_CLASS, CHECK_TIMEOUT, SCORE, ALLOW_SUBMIT)
VALUES('010', '流程引擎', '使用流程引擎以完成指定流程', 'CLASS', NULL, '@crawlerResultChecker', 300, 20, 1);

-- CrEATE TABLE
CREATE TABLE GAME_ATTEMPT
(
    ID           INTEGER PRIMARY key auto_increment comment '主键',
    RUNNERS_NO   VARCHAR(20) NOT NULL comment '提交者工号',
    QUES_NO      VARCHAR(20) NOT NULL comment '题目编号',
    SUBMIT_TIME  TIMESTAMP NOT NULL comment '提交时间戳',
    CHECK_STATUS VARCHAR(20) NOT NULL comment '评分状态',
    CHECK_RESULT VARCHAR(200) comment '评分结果信息',
    SCORE        INTEGER comment '分数',
    SCORE_TIME   TIMESTAMP comment '评分时间',
    CONDUCT_USER INTEGER,
    CONDUCT_TIME CHAR(14)
);


-- Create table
create table GAME_SCORE
(
    ID            INTEGER PRIMARY key auto_increment comment '主键',
    RUNNERS_NO    VARCHAR(20) NOT NULL comment '选手工号',
    QUES_NO       VARCHAR(20) NOT NULL comment '题目编号',
    RUNNERS_NAME  VARCHAR(200) NOT NULL comment '选手名字',
    QUES_TITLE    VARCHAR(200) NOT NULL comment '题目标题',
    SCORE         INTEGER comment '分数',
    SCORE_ATTEMPT INTEGER comment '当前分数的提交编号',
    SCORE_TIME    TIMESTAMP comment '评分时间',
    CONDUCT_USER  INTEGER,
    CONDUCT_TIME  CHAR(14)
);

alter table GAME_SCORE
    add unique (RUNNERS_NO, QUES_NO);


create table GAME_TRANS_ACCOUNT
(
    ID            INTEGER PRIMARY key auto_increment comment '主键',
    RUNNERS_NO    VARCHAR(20) NOT NULL comment '选手工号',
    QUES_NO       VARCHAR(20) NOT NULL comment '题目编号',
    ATTEMPT       INTEGER NOT NULL comment '提交编号',
    CASE_NO       INTEGER NOT NULL comment '测试样例编号',
    GLOBAL_SEQ    VARCHAR(24) NOT NULL comment '全局流水号',
    INVOKE_COUNT  INTEGER default 0 NOT NULL comment '接口调用次数',
    CONDUCT_USER  INTEGER,
    CONDUCT_TIME  CHAR(14)
) COMMENT = '通用记账接口调用记录';

alter table GAME_TRANS_ACCOUNT
    add unique (ATTEMPT, CASE_NO);

alter table GAME_TRANS_ACCOUNT
    add unique (GLOBAL_SEQ);

create table GAME_DOWNLOAD
(
    ID            INTEGER PRIMARY key auto_increment comment '主键',
    RUNNERS_NO    VARCHAR(20) comment '选手工号, NULL的话所有人都能下载',
    FILE_NAME     VARCHAR(100) NOT NULL comment '文件名',
    FILE_PATH     VARCHAR(500) NOT NULL comment '文件路径',
    CONDUCT_USER  INTEGER,
    CONDUCT_TIME  CHAR(14)
) COMMENT = '文件下载记录';

insert into sys_param (NAME, VALUE, TYPE, SUBTYPE, STATUS, REMARK, CONDUCT_USER, CONDUCT_TIME)
values ('context-url', 'http://localhost:8888/contest', 'GAME', '1', '1', '比赛服务器基准URL', 1, '20230306000000');


-- 爬虫题目用表
create table game_crawler_attempt
(
    ID            INTEGER PRIMARY key auto_increment comment '主键',
    RUNNERS_NO    VARCHAR(20) NOT NULL comment '选手工号',
    SUBMIT_TIME   TIMESTAMP NOT NULL comment '提交时间戳',
    STATUS        VARCHAR(20) NOT NULL comment '提交状态',
    CHECK_ATTEMPT INTEGER comment '评分编号',
    CONDUCT_USER  INTEGER,
    CONDUCT_TIME  CHAR(14)
) comment = '爬虫提交表';

-- BPM类通用Case表
CREATE TABLE GAME_BPM_TESTCASE (
    ID              INTEGER PRIMARY KEY AUTO_INCREMENT,
    QUES_NO         VARCHAR(20) NOT NULL COMMENT '题目编号',
    SEQ_NO          INTEGER NOT NULL COMMENT '顺序号',
    EXCHANGE_URL    VARCHAR(200) NOT NULL COMMENT '指定的URL, 无前面的/号',
    EXCHANGE_METHOD VARCHAR(10) NOT NULL DEFAULT 'POST' COMMENT 'GET/POST/...',
    EXCHANGE_JSON   VARCHAR(1024) NOT NULL COMMENT '发送的JSON',
    EXCHANGE_CLASS  VARCHAR(200) NOT NULL COMMENT '发送验证类',
    EXCHANGE_HEADER VARCHAR(200) NOT NULL DEFAULT 'Content-Type: text/json' COMMENT '发送的http头设置',
    EXPECTED_JSON   VARCHAR(1024) NOT NULL COMMENT '预期的返回JSON',
    EXPECTED_CLASS  VARCHAR(200) NOT NULL COMMENT '预期验证类, 使用这个类反序列号再用通用比较方法比对是否符合',
    COMPARE_PROPERTIES VARCHAR(1024) NOT NULL COMMENT '需要比对的Property, 使用;号隔开',
    SLEEP_SECONDS   INTEGER NOT NULL DEFAULT 5 COMMENT '等待几秒让选手服务端',
    ENABLED         INTEGER DEFAULT 1 COMMENT '是否启用1-是 0-否'
) comment = 'BPM类通用Case表';

-- 假设题目中所用到的类, 作为POJO的命名空间是  com.njcb.game.pojo.check
-- SubmitRequest, ApproveRequest, QueryApproveRecordRequest - 作为发送的类
-- CommonResponse, PagedResponse<T>, ApproveResult, QueryRecord - 作为返回的检查类
-- Request Json可以从C# TestCaseGenerator 10生成
INSERT INTO GAME_BPM_TESTCASE(QUES_NO, SEQ_NO, EXCHANGE_JSON, EXCHANGE_CLASS, EXPECTED_JSON, EXPECTED_CLASS) VALUES('01', 1, '{"businessNo":"0001","operator":"10000","businessAmount":-10}', 'com.njcb.game.pojo.check.SubmitRequest', '{"success":false,"status":200,"code":"9001"}', 'com.njcb.game.pojo.check.CommonResponse');
INSERT INTO GAME_BPM_TESTCASE(QUES_NO, SEQ_NO, EXCHANGE_JSON, EXCHANGE_CLASS, EXPECTED_JSON, EXPECTED_CLASS) VALUES('02', 1, '{"businessNo":"0001","operator":"10000","businessAmount":10000000}', 'com.njcb.game.pojo.check.SubmitRequest', '{"success":false,"status":200,"code":"9001"}', 'com.njcb.game.pojo.check.CommonResponse');
INSERT INTO GAME_BPM_TESTCASE(QUES_NO, SEQ_NO, EXCHANGE_JSON, EXCHANGE_CLASS, EXPECTED_JSON, EXPECTED_CLASS) VALUES('02', 2, '{"businessNo":"1111","operator":"10002","approveResult":"agree"}', 'com.njcb.game.pojo.check.ApproveRequest', '{"success":false,"status":200,"code":"AAA00T9999"}', 'com.njcb.game.pojo.check.CommonResponse');
INSERT INTO GAME_BPM_TESTCASE(QUES_NO, SEQ_NO, EXCHANGE_JSON, EXCHANGE_CLASS, EXPECTED_JSON, EXPECTED_CLASS) VALUES('02', 3, '{"businessNo":"1111","operator":"3","approveResult":"agree"}', 'com.njcb.game.pojo.check.ApproveRequest', '{"success":false,"status":200,"code":"AAA00T9999"}', 'com.njcb.game.pojo.check.CommonResponse');
INSERT INTO GAME_BPM_TESTCASE(QUES_NO, SEQ_NO, EXCHANGE_JSON, EXCHANGE_CLASS, EXPECTED_JSON, EXPECTED_CLASS) VALUES('03', 1, '{"businessNo":"0002","operator":"10000","businessAmount":10000000}', 'com.njcb.game.pojo.check.SubmitRequest', '{"success":false,"status":200,"code":"9001"}', 'com.njcb.game.pojo.check.CommonResponse');
INSERT INTO GAME_BPM_TESTCASE(QUES_NO, SEQ_NO, EXCHANGE_JSON, EXCHANGE_CLASS, EXPECTED_JSON, EXPECTED_CLASS) VALUES('03', 2, '{"businessNo":"0002","operator":"10001","approveResult":"agree"}', 'com.njcb.game.pojo.check.ApproveRequest', '{"success":false,"status":200,"code":"AAA00T9999"}', 'com.njcb.game.pojo.check.CommonResponse');
INSERT INTO GAME_BPM_TESTCASE(QUES_NO, SEQ_NO, EXCHANGE_JSON, EXCHANGE_CLASS, EXPECTED_JSON, EXPECTED_CLASS) VALUES('03', 3, '{"businessNo":"0002","operator":"10002","approveResult":"agree"}', 'com.njcb.game.pojo.check.ApproveRequest', '{"success":false,"status":200,"code":"AAA00T9999"}', 'com.njcb.game.pojo.check.CommonResponse');
INSERT INTO GAME_BPM_TESTCASE(QUES_NO, SEQ_NO, EXCHANGE_JSON, EXCHANGE_CLASS, EXPECTED_JSON, EXPECTED_CLASS) VALUES('03', 4, '{"businessNo":"0002"}', 'com.njcb.game.pojo.check.QueryRecord', '{"total":0,"rows":null,"success":false,"status":200,"code":"AAA00T9999"}', 'com.njcb.game.pojo.check.PageResponse');
INSERT INTO GAME_BPM_TESTCASE(QUES_NO, SEQ_NO, EXCHANGE_JSON, EXCHANGE_CLASS, EXPECTED_JSON, EXPECTED_CLASS) VALUES('04', 1, '{"businessNo":"0003","operator":"10000","businessAmount":20000000}', 'com.njcb.game.pojo.check.SubmitRequest', '{"success":false,"status":200,"code":"9001"}', 'com.njcb.game.pojo.check.CommonResponse');
INSERT INTO GAME_BPM_TESTCASE(QUES_NO, SEQ_NO, EXCHANGE_JSON, EXCHANGE_CLASS, EXPECTED_JSON, EXPECTED_CLASS) VALUES('04', 2, '{"businessNo":"0003","operator":"10001","approveResult":"agree"}', 'com.njcb.game.pojo.check.ApproveRequest', '{"success":false,"status":200,"code":"AAA00T9999"}', 'com.njcb.game.pojo.check.CommonResponse');
INSERT INTO GAME_BPM_TESTCASE(QUES_NO, SEQ_NO, EXCHANGE_JSON, EXCHANGE_CLASS, EXPECTED_JSON, EXPECTED_CLASS) VALUES('04', 3, '{"businessNo":"0003","operator":"10002","approveResult":"agree"}', 'com.njcb.game.pojo.check.ApproveRequest', '{"success":false,"status":200,"code":"AAA00T9999"}', 'com.njcb.game.pojo.check.CommonResponse');
INSERT INTO GAME_BPM_TESTCASE(QUES_NO, SEQ_NO, EXCHANGE_JSON, EXCHANGE_CLASS, EXPECTED_JSON, EXPECTED_CLASS) VALUES('04', 4, '{"businessNo":"0003"}', 'com.njcb.game.pojo.check.QueryRecord', '{"total":0,"rows":null,"success":false,"status":200,"code":"AAA00T9999"}', 'com.njcb.game.pojo.check.PageResponse');
INSERT INTO GAME_BPM_TESTCASE(QUES_NO, SEQ_NO, EXCHANGE_JSON, EXCHANGE_CLASS, EXPECTED_JSON, EXPECTED_CLASS) VALUES('05', 1, '{"businessNo":"0004","operator":"10000","businessAmount":20000000}', 'com.njcb.game.pojo.check.SubmitRequest', '{"success":false,"status":200,"code":"9001"}', 'com.njcb.game.pojo.check.CommonResponse');
INSERT INTO GAME_BPM_TESTCASE(QUES_NO, SEQ_NO, EXCHANGE_JSON, EXCHANGE_CLASS, EXPECTED_JSON, EXPECTED_CLASS) VALUES('05', 2, '{"businessNo":"0004","operator":"10001","approveResult":"disagree"}', 'com.njcb.game.pojo.check.ApproveRequest', '{"success":false,"status":200,"code":"AAA00T9999"}', 'com.njcb.game.pojo.check.CommonResponse');
INSERT INTO GAME_BPM_TESTCASE(QUES_NO, SEQ_NO, EXCHANGE_JSON, EXCHANGE_CLASS, EXPECTED_JSON, EXPECTED_CLASS) VALUES('05', 3, '{"businessNo":"0004","operator":"10002","approveResult":"agree"}', 'com.njcb.game.pojo.check.ApproveRequest', '{"success":false,"status":200,"code":"AAA00T9999"}', 'com.njcb.game.pojo.check.CommonResponse');
INSERT INTO GAME_BPM_TESTCASE(QUES_NO, SEQ_NO, EXCHANGE_JSON, EXCHANGE_CLASS, EXPECTED_JSON, EXPECTED_CLASS) VALUES('05', 4, '{"businessNo":"0004"}', 'com.njcb.game.pojo.check.QueryRecord', '{"total":3,"rows":[{"nodeName":"\u5BA2\u6237\u7ECF\u7406\u7533\u8BF7","operator":"10000","startTime":"2024-04-04 22:05:06","endTime":"2024-04-04 22:05:06"},{"nodeName":"\u5206\u884C\u4E1A\u52A1\u8D1F\u8D23\u4EBA","operator":"10001","startTime":"2024-04-04 22:05:06","endTime":"2024-04-04 22:05:07"},{"nodeName":"\u5BA2\u6237\u7ECF\u7406\u7533\u8BF7","operator":"10000","startTime":"2024-04-04 22:05:07"}],"success":true,"status":200,"code":"0000"}', 'com.njcb.game.pojo.check.PageResponse');
INSERT INTO GAME_BPM_TESTCASE(QUES_NO, SEQ_NO, EXCHANGE_JSON, EXCHANGE_CLASS, EXPECTED_JSON, EXPECTED_CLASS) VALUES('06', 1, '{"businessNo":"0005","operator":"10000","businessAmount":20000000}', 'com.njcb.game.pojo.check.SubmitRequest', '{"success":false,"status":200,"code":"9001"}', 'com.njcb.game.pojo.check.CommonResponse');
INSERT INTO GAME_BPM_TESTCASE(QUES_NO, SEQ_NO, EXCHANGE_JSON, EXCHANGE_CLASS, EXPECTED_JSON, EXPECTED_CLASS) VALUES('06', 2, '{"businessNo":"0005","operator":"10001","approveResult":"agree"}', 'com.njcb.game.pojo.check.ApproveRequest', '{"success":false,"status":200,"code":"AAA00T9999"}', 'com.njcb.game.pojo.check.CommonResponse');
INSERT INTO GAME_BPM_TESTCASE(QUES_NO, SEQ_NO, EXCHANGE_JSON, EXCHANGE_CLASS, EXPECTED_JSON, EXPECTED_CLASS) VALUES('06', 3, '{"businessNo":"0005","operator":"10002","approveResult":"disagree"}', 'com.njcb.game.pojo.check.ApproveRequest', '{"success":false,"status":200,"code":"AAA00T9999"}', 'com.njcb.game.pojo.check.CommonResponse');
INSERT INTO GAME_BPM_TESTCASE(QUES_NO, SEQ_NO, EXCHANGE_JSON, EXCHANGE_CLASS, EXPECTED_JSON, EXPECTED_CLASS) VALUES('06', 4, '{"businessNo":"0005"}', 'com.njcb.game.pojo.check.QueryRecord', '{"total":4,"rows":[{"nodeName":"\u5BA2\u6237\u7ECF\u7406\u7533\u8BF7","operator":"10000","startTime":"2024-04-04 22:20:31","endTime":"2024-04-04 22:20:32"},{"nodeName":"\u5206\u884C\u4E1A\u52A1\u8D1F\u8D23\u4EBA","operator":"10001","startTime":"2024-04-04 22:20:32","endTime":"2024-04-04 22:20:32"},{"nodeName":"\u603B\u884C\u4E1A\u52A1\u8D1F\u8D23\u4EBA","operator":"10002","startTime":"2024-04-04 22:20:32","endTime":"2024-04-04 22:20:32"},{"nodeName":"\u5BA2\u6237\u7ECF\u7406\u7533\u8BF7","operator":"10000","startTime":"2024-04-04 22:20:32"}],"success":true,"status":200,"code":"0000"}', 'com.njcb.game.pojo.check.PageResponse');
