# 打包过程
1. 依次从以下项目中获取数据库初始化SQL脚本并放置于sql目录下
   1. ams-code
   2. ams-web
   3. ams-scheduler
   4. 本项目

2. 执行 games/大数阶乘/gen_test_cases.py 并将生成的
   - hard
   - mid
   - simple
   - metadata.jsonc

   文件/夹复制到 njcb_games\cases\002 目录中
