# 投资收益

附表是XX银行2021年理财（/基金）的全年净值数据。
下载地址: `<host_addr>/game_files/net_asset_value.csv`

选手提供 RESTful JSON 接口
`POST <server_addr>/question/<ques_no>`
实现以下功能：

- 给定多组产品的代号、购买日期、赎回日期、购买金额
- 如不考虑手续费和所得税，请计算用户在全年可以获得的收益（以人民币元为单位，年化以360天计算）

其中：

- *server_addr* 为选手注册时提供的程序服务地址
- *host_addr* 为比赛组织方提供的比赛平台服务地址
- 日期均为2021年内的合法日期
- 金额在1万至1000万之间
- 最终结果保留2位小数

----

POST body 样例：

```json
 {
  "purchases": [
    {
      "productCode": "Y30003",
      "amount": "2270000",
      "buyDate": "20211013",
      "sellDate": "20211115"
    }
  ]
}
```

| 参数             | 说明     |
|----------------|--------|
| purchases      | 产品购买列表 |
| -- productCode | 产品代码   |
| -- amount      | 购买金额   |
| -- buyDate     | 购买日期   |
| -- sellDate    | 赎回日期   |

响应样例：

```json
 {
  "gain": "118243.83"
}
```

| 参数   | 说明   |
|------|------|
| gain | 年化收益 |
