'''
过滤出2021年的数据
'''

import os

this_dir = os.path.dirname(__file__)

with open(os.path.join(this_dir, '理财净值数据.csv'), 'r') as in_file:
    with open(os.path.join(this_dir, '../../common/src/main/resources/static/game_files/net_asset_value.csv'), 'w') as out_file:
        # 复制 header
        out_file.write(next(in_file))

        # 过滤数据
        for line in in_file:
            date = line.split(',')[1]
            if date.startswith('2021'):
                out_file.write(line)
