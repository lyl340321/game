# 大数阶乘

选手提供 RESTful JSON 接口
`POST <server_addr>/question/<ques_no>`
实现以下功能：

给定正整数*i*，计算*i*的阶乘。

其中：

- *server_addr* 为选手注册时提供的程序服务地址
- *ques_no* 为题目编号
- 1 <= *i* <= 1000

----

POST body 样例：

```json
{
  "i": "4"
}
```

响应样例：

```json
{
  "answer": "24"
}
```
