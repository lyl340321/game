import os
import json
import math

# 20! = 2432902008176640000 刚好在long范围内，所以作为简单题的上限
# 1000! 是上限

def write_single(level, i):
    with open(os.path.join(os.path.dirname(__file__), level, f'{i}.json'), 'w') as f:
        f.write(json.dumps({
            "input": {
                "i": str(i)
            },
            "expect": {
                "answer": str(math.factorial(i))
            }
        }, indent=4))

os.makedirs(os.path.join(os.path.dirname(__file__), 'simple'), exist_ok=True)
os.makedirs(os.path.join(os.path.dirname(__file__), 'mid'), exist_ok=True)
os.makedirs(os.path.join(os.path.dirname(__file__), 'hard'), exist_ok=True)

for i in range(1, 21):
    write_single('simple', i)

for i in range (100, 301):
    write_single('mid', i)

for i in range (800, 1001):
    write_single('hard', i)