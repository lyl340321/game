# FTP 利差

FTP价格指以一定的规则制定的价格，由司库提供给各经营单位的转移资金的价格。商业银行可通过FTP核算资金收益或成本。

例：
存款部门吸收了一笔 1 年定期存款1000万元，到期还本付息，利率 2%；
贷款部门发放了一笔 1 年期固定利率贷款1000万，到期一次性还本付息，利率5%；
其中，FTP价格为3.5%，则：

- 存款FTP利润 = 1000 × 3.5% - 1000 × 2% = 15万
- 贷款FTP利润 = 1000 × 5% - 1000 × 3.5% = 15万
- FTP利差收入=存款FTP利润 + 贷款FTP利润 = 30 万

选手提供 RESTful JSON 接口
`POST <server_addr>/question/<ques_no>`
实现以下功能：

- 银行FTP利率为 *l*
- 现有 *i* 款一年期存款产品，年化利率分别为 m1, m2, ... , mi
- 现有 *j* 款一年期贷款产品，年化利率分别为 n1, n2, ... , ni
- 给定 *k* 组客户编号，持有产品编号，金额等，请计算出FTP利差收入并升序输出客户编号和FTP利差收入

其中：

- *server_addr* 为选手注册时提供的程序服务地址
- 2.5% <= *l* <= 3.9%
- 0.5% <= *m* < *l* < *n* <= 5.9%
- *l*, *m*, *n* 均为3位小数
- 4 <= *i*, *j* <= 8
- 3 <= *k* <= 10
- 10 <= 每组产品金额 <= 9999990 且为10的整数倍
- 最终结果保留2位小数
- 最终结果按FTP利差升序排列，若两个客户利差相同则再按客户编号升序排列

----

POST body 样例：

```json
{
  "ftpRate": "0.039",
  "depositProducts": [
    {
      "code": "CK001",
      "yearlyRate": "0.023"
    }
  ],
  "loanProducts": [
    {
      "code": "DK001",
      "yearlyRate": "0.050"
    }
  ],
  "purchases": [
    {
      "customerNo": "10001",
      "productCode": "CK001",
      "amount": "979290"
    },
    {
      "customerNo": "10002",
      "productCode": "DK001",
      "amount": "240120"
    }
  ]
}
```

| 参数              | 说明      |
|-----------------|---------|
| ftpRate         | 银行FTP利率 |
| depositProducts | 存款产品    |
| -- code         | 产品代码    |
| -- yearlyRate   | 年化利率    |
| loanProducts    | 贷款产品    |
| purchases       | 客户购买产品  |
| -- customerNo   | 客户号     |
| -- productCode  | 产品代码    |
| -- amount       | 金额      |

响应样例：

```json
{
  "spreads": [
    {
      "customerNo": "10002",
      "amount": "2641.32"
    },
    {
      "customerNo": "10001",
      "amount": "15668.64"
    }
  ]
}
```

| 参数            | 说明   |
|---------------|------|
| spreads       | 利差列表 |
| -- customerNo | 客户号  |
| -- amount     | 利差金额 |
